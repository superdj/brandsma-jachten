<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('abbreviation')->nullable();
            $table->decimal('step', '3', '2');
            $table->unsignedBigInteger('user_created');
            $table->foreign('user_created')->references('id')->on('users');
            $table->unsignedBigInteger('user_updated')->nullable();
            $table->foreign('user_updated')->references('id')->on('users');
            $table->timestamps();
        });

        Schema::table('parts', function (Blueprint $table)
        {
            $table->unsignedBigInteger('unit_id')->after('price');
            $table->foreign('unit_id')->references('id')->on('units');
        });

        Schema::table('part_shelf', function (Blueprint $table)
        {
            $table->decimal('stock', 10, 2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parts', function (Blueprint $table)
        {
            $table->dropForeign('unit_id');
            $table->dropColumn('unit_id');
        });

        Schema::table('part_shelf', function (Blueprint $table)
        {
            $table->integer('stock')->change();
        });

        Schema::dropIfExists('units');
    }
}
