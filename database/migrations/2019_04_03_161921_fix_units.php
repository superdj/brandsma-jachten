<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixUnits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('units', function (Blueprint $table)
        {
            $table->dropColumn('step');
            $table->string('abbreviation')->nullable(false)->change();
        });

        Schema::table('parts', function (Blueprint $table)
        {
            $table->decimal('minimal_stock', 10, 2)->change();
        });

        Schema::table('assignment_part', function (Blueprint $table)
        {
            $table->decimal('amount', 10, 2)->change();
        });

        Schema::table('order_part', function (Blueprint $table)
        {
            $table->decimal('amount', 10, 2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('units', function (Blueprint $table)
        {
            $table->decimal('step', 3, 2)->after('abbreviation');
            $table->string('abbreviation')->nullable()->change();
        });

        Schema::table('parts', function (Blueprint $table)
        {
            $table->integer('minimal_stock')->change();
        });

        Schema::table('assignment_part', function (Blueprint $table)
        {
            $table->integer('amount')->change();
        });

        Schema::table('order_part', function (Blueprint $table)
        {
            $table->integer('amount')->change();
        });
    }
}
