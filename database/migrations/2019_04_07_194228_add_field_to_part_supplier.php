<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToPartSupplier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('part_supplier', function (Blueprint $table) {
            $table->decimal('nett_price', 10, 2)->after('id');
            $table->smallInteger('discount')->nullable()->after('nett_price');
            $table->string('number')->unique()->after('nett_price');
        });

        Schema::table('parts', function(Blueprint $table) {
            $table->dropColumn('number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('part_supplier', function (Blueprint $table) {
            $table->dropColumn(['nett_price', 'discount', 'number']);
        });

        Schema::table('parts', function(Blueprint $table) {
            $table->string('number')->after('name');
        });
    }
}
