<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixCascading extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shelves', function (Blueprint $table)
        {
            $table->dropForeign(['rack_id']);
            $table->foreign('rack_id')->references('id')->on('racks')->onDelete('cascade');
        });

        Schema::table('racks', function (Blueprint $table)
        {
            $table->dropForeign(['warehouse_id']);
            $table->foreign('warehouse_id')->references('id')->on('warehouses')->onDelete('cascade');
        });

        Schema::table('part_shelf', function (Blueprint $table)
        {
            $table->dropForeign(['part_id']);
            $table->foreign('part_id')->references('id')->on('parts')->onDelete('cascade');
            $table->dropForeign(['shelf_id']);
            $table->foreign('shelf_id')->references('id')->on('shelves')->onDelete('cascade');
        });

        Schema::table('part_supplier', function (Blueprint $table)
        {
            $table->dropForeign(['part_id']);
            $table->foreign('part_id')->references('id')->on('parts')->onDelete('cascade');
            $table->dropForeign(['supplier_id']);
            $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shelves', function (Blueprint $table)
        {
            $table->dropForeign(['rack_id']);
            $table->foreign('rack_id')->references('id')->on('racks');
        });

        Schema::table('racks', function (Blueprint $table)
        {
            $table->dropForeign(['warehouse_id']);
            $table->foreign('warehouse_id')->references('id')->on('warehouses');
        });

        Schema::table('part_shelf', function (Blueprint $table)
        {
            $table->dropForeign(['part_id']);
            $table->foreign('part_id')->references('id')->on('parts');
            $table->dropForeign(['shelf_id']);
            $table->foreign('shelf_id')->references('id')->on('shelves');
        });

        Schema::table('part_supplier', function (Blueprint $table)
        {
            $table->dropForeign(['part_id']);
            $table->foreign('part_id')->references('id')->on('parts');
            $table->dropForeign(['supplier_id']);
            $table->foreign('supplier_id')->references('id')->on('suppliers');
        });
    }
}
