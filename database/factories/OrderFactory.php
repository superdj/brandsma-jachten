<?php

use Faker\Generator as Faker;

$factory->define(App\Order::class, function (Faker $faker) {
    return [
        'name' => $faker->words(4, true),
        'number' => $faker->ean8,
        'status_id' => App\Status::where('model', '=', 'App\Order')->inRandomOrder()->first()->id,
        'user_created' => App\User::inRandomOrder()->first()->id
    ];
});
