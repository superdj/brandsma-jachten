<?php

use Faker\Generator as Faker;

$factory->define(App\Assignment::class, function (Faker $faker) {
    return [
        'name' => $faker->words(4, true),
        'description' => $faker->text,
        'start_date' => $faker->dateTimeBetween('-1 year', 'now'),
        'end_date' => $faker->dateTimeBetween('now', '+ 1 year'),
        'customer_id' => App\Customer::inRandomOrder()->first()->id,
        'status_id' => App\Status::where('model', '=', 'App\Assignment')->inRandomOrder()->first()->id,
        'user_created' => App\User::inRandomOrder()->first()->id
    ];
});
