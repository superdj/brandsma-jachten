<?php

use Faker\Generator as Faker;


$factory->define(App\Customer::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'boat' => $faker->words(2, true),
        'user_created' => App\User::inRandomOrder()->first()->id
    ];
});
