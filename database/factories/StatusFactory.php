<?php

use Faker\Generator as Faker;

$factory->define(App\Status::class, function (Faker $faker) {
    return [
        'name' => $faker->words(3, true),
        'model' => $faker->randomElement(['App\Order', 'App\Assignment']),
        'lock' => $faker->boolean(50),
        'user_created' => App\User::inRandomOrder()->first()->id
    ];
});
