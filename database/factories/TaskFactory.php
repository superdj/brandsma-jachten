<?php

use Faker\Generator as Faker;

$factory->define(App\Task::class, function (Faker $faker) {
    return [
        'name' => $faker->words(3, true),
        'user_created' => App\User::inRandomOrder()->first()->id
    ];
});
