<?php

use Faker\Generator as Faker;

$factory->define(App\Rack::class, function (Faker $faker) {
    return [
        'name' => $faker->randomDigitNotNull,
        'warehouse_id' => App\Warehouse::inRandomOrder()->first()->id,
        'user_created' => App\User::inRandomOrder()->first()->id
    ];
});
