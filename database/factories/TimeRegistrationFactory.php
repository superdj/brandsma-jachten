<?php

use Faker\Generator as Faker;

$factory->define(App\TimeRegistration::class, function (Faker $faker) {
    return [
        'time' => $faker->time('H:i'),
        'assignment_id' => App\Assignment::inRandomOrder()->first()->id,
        'user_id' => App\User::inRandomOrder()->first()->id,
        'user_created' => App\User::inRandomOrder()->first()->id,
    ];
});
