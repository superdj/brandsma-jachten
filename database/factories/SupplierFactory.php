<?php

use Faker\Generator as Faker;



$factory->define(App\Supplier::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'number' => $faker->ean8,
        'city' => $faker->city,
        'postal_code' => $faker->postcode,
        'street' => $faker->streetName,
        'house_number' => $faker->buildingNumber,
        'email' => $faker->safeEmail,
        'phone_number' => $faker->phoneNumber,
        'mobile_number' => $faker->phoneNumber,
        'website' => $faker->url,
        'user_created' => App\User::inRandomOrder()->first()->id
    ];
});
