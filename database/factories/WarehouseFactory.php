<?php

use Faker\Generator as Faker;

$factory->define(App\Warehouse::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'user_created' => App\User::inRandomOrder()->first()->id
    ];
});
