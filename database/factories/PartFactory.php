<?php

use Faker\Generator as Faker;

$factory->define(App\Part::class, function( Faker $faker ) {
    return [
        'name' => $faker->word,
        'price' => $faker->randomFloat(2, 0, 99999999),
        'unit_id' => App\Unit::inRandomOrder()->first()->id,
        'minimal_stock' => $faker->randomDigit,
        'user_created' => App\User::inRandomOrder()->first()->id
    ];
});
