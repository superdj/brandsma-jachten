<?php

use Faker\Generator as Faker;

$factory->define(App\Shelf::class, function (Faker $faker) {
    return [
        'name' => $faker->randomLetter,
        'rack_id' => App\Rack::inRandomOrder()->first()->id,
        'user_created' => App\User::inRandomOrder()->first()->id
    ];
});
