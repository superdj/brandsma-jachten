<?php

use App\User;
use Illuminate\Database\Seeder;
use App\Unit;

class UnitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Unit::firstOrCreate([
            'name' => 'Liter',
            'abbreviation' => 'L',
            'user_created' => User::inRandomOrder()->first()->id
        ]);

        Unit::firstOrCreate([
            'name' => 'Meter',
            'abbreviation' => 'm',
            'user_created' => User::inRandomOrder()->first()->id
        ]);

        Unit::firstOrCreate([
            'name' => 'Stuks',
            'abbreviation' => 'Stuks',
            'user_created' => User::inRandomOrder()->first()->id
        ]);
    }
}
