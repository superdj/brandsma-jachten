<?php

use Illuminate\Database\Seeder;
use App\Status;
use App\User;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Status::class, 5)->create();

        Status::firstOrCreate([
            'name' => 'Aangemaakt',
            'model' => 'App\Order',
            'lock' => 0,
            'user_created' => User::inRandomOrder()->first()->id
        ]);

        Status::firstOrCreate([
            'name' => 'Besteld',
            'model' => 'App\Order',
            'lock' => 0,
            'user_created' => User::inRandomOrder()->first()->id
        ]);

        Status::firstOrCreate([
            'name' => 'Verzonden',
            'model' => 'App\Order',
            'lock' => 0,
            'user_created' => User::inRandomOrder()->first()->id
        ]);

        Status::firstOrCreate([
            'name' => 'Ontvangen',
            'model' => 'App\Order',
            'lock' => 1,
            'user_created' => User::inRandomOrder()->first()->id
        ]);

        Status::firstOrCreate([
            'name' => 'Aangemaakt',
            'model' => 'App\Assignment',
            'lock' => 0,
            'user_created' => User::inRandomOrder()->first()->id
        ]);

        Status::firstOrCreate([
            'name' => 'Gestart',
            'model' => 'App\Assignment',
            'lock' => 0,
            'user_created' => User::inRandomOrder()->first()->id
        ]);

        Status::firstOrCreate([
            'name' => 'Afgerond',
            'model' => 'App\Assignment',
            'lock' => 1,
            'user_created' => User::inRandomOrder()->first()->id
        ]);
    }
}
