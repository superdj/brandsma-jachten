<?php

use Illuminate\Database\Seeder;

class RackTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Rack::class, 20)->create();
    }
}
