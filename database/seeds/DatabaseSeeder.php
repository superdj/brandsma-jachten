<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PermissionsTableSeeder::class,
            UsersTableSeeder::class,
            SupplierTableSeeder::class,
            CustomerTableSeeder::class,
            WarehouseTableSeeder::class,
            RackTableSeeder::class,
            ShelfTableSeeder::class,
            UnitTableSeeder::class,
            CategoryTableSeeder::class,
            PartTableSeeder::class,
            StatusTableSeeder::class,
            TaskTableSeeder::class,
            OrderTableSeeder::class,
            AssignmentTableSeeder::class,
            TimeRegistrationTableSeeder::class
        ]);
    }
}
