<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\Part;
use App\Shelf;
use App\Supplier;
use Faker\Factory as Faker;

class PartTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory( Part::class, 200 )->create();

        $categories = Category::all();
        $parts = Part::all();
        $shelves = Shelf::all();
        $suppliers = Supplier::all();
        $faker = Faker::create();

        // Adding locations to an part
        $parts->each( function( $part ) use( $shelves ) {

            foreach( $shelves->pluck( 'id' )->random( rand( 1, 3 ) )->toArray() as $shelf )
            {
                $sync[$shelf] = ['stock' => rand(5, 100)];
            }

            $part->shelves()->attach( $sync );
        });

        // Adding suppliers to a part
        $parts->each( function( $part ) use( $suppliers, $faker ) {
            $suppliers = $suppliers->pluck('id' )->random( rand( 1, 3 ) )->toArray();

            foreach( $suppliers as $supplier )
            {
                $sync[$supplier] = [
                    'number' => $faker->ean13,
                    'nett_price' => $faker->randomFloat(2, 0, 99999999),
                    'discount' => rand( 0,100 )
                ];
            }

            $part->suppliers()->attach( $sync );
        });

        // Adding categories to a part
        $parts->each( function( $part ) use( $categories ) {
            $categories = $categories->pluck( 'id' )->random( rand( 1, 3 ) )->toArray();

            $part->categories()->attach( $categories );
        });
    }
}
