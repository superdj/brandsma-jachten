<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory( User::class, 10 )->create();

        $user = User::firstOrCreate(
            ['email' => 'johndoe@brandsma.test'],
            [
                'first_name' => 'John',
                'last_name' => 'Doe',
                'email' => 'johndoe@brandsma.test',
                'password' => Hash::make('Test1234')
            ]
        );

        $user->assignRole( 'Admin' );
    }
}
