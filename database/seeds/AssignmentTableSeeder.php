<?php

use Illuminate\Database\Seeder;
use App\Assignment;

class AssignmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Assignment::class, 150)->create();
        $assignments = Assignment::all();

        /**
         * Adding parts to an assignment
         */
        $parts = App\Part::all();

        $assignments->each( function( $assignment ) use ( $parts ) {
            $parts = $parts->pluck( 'id' )->random( rand( 1, 10 ) )->toArray();

            $month = rand(1, 12);
            $year = rand(1990, 2019);
            $day = rand(1, 28);
            foreach( $parts as $part )
            {
                $sync[$part] = [
                    'amount' => rand(1, 50),
                    'updated_at' => $year.'-'.($month < 10 ? '0'.$month : $month).'-'.($day < 10 ? '0'.$day : $day).' 12:00:00',
                    'created_at' => $year.'-'.($month < 10 ? '0'.$month : $month).'-'.($day < 10 ? '0'.$day : $day).' 12:00:00',
                ];
            }

            $assignment->parts()->attach($sync);
        });

        /**
         * Adding mechanics to an assignment
         */
        $users = App\User::all();

        $assignments->each( function( $assignment ) use ( $users ) {
            $assignment->users()->attach(
                $users->pluck( 'id')->random( rand( 1, 10 ) )->toArray()
            );
        });

        /**
         * Adding mechanics to an assignment
         */
        $tasks = App\Task::all();

        $assignments->each( function( $assignment ) use ( $tasks ) {
            $assignment->tasks()->attach(
                $tasks->pluck( 'id')->random( rand( 1, 10 ) )->toArray()
            );
        });
    }
}
