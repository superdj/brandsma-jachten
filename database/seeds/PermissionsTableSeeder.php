<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // Assignments
        Permission::firstOrCreate(['name' => 'view assignments']);
        Permission::firstOrCreate(['name' => 'create assignments']);
        Permission::firstOrCreate(['name' => 'edit assignments']);
        Permission::firstOrCreate(['name' => 'show assignments']);
        Permission::firstOrCreate(['name' => 'delete assignments']);
        Permission::firstOrCreate(['name' => 'export assignments']);

        // Customers
        Permission::firstOrCreate(['name' => 'view categories']);
        Permission::firstOrCreate(['name' => 'create categories']);
        Permission::firstOrCreate(['name' => 'edit categories']);
        Permission::firstOrCreate(['name' => 'show categories']);
        Permission::firstOrCreate(['name' => 'delete categories']);

        // Customers
        Permission::firstOrCreate(['name' => 'view customers']);
        Permission::firstOrCreate(['name' => 'create customers']);
        Permission::firstOrCreate(['name' => 'edit customers']);
        Permission::firstOrCreate(['name' => 'show customers']);
        Permission::firstOrCreate(['name' => 'delete customers']);

        // Orders
        Permission::firstOrCreate(['name' => 'view orders']);
        Permission::firstOrCreate(['name' => 'create orders']);
        Permission::firstOrCreate(['name' => 'edit orders']);
        Permission::firstOrCreate(['name' => 'show orders']);
        Permission::firstOrCreate(['name' => 'delete orders']);

        // Parts
        Permission::firstOrCreate(['name' => 'view parts']);
        Permission::firstOrCreate(['name' => 'create parts']);
        Permission::firstOrCreate(['name' => 'edit parts']);
        Permission::firstOrCreate(['name' => 'show parts']);
        Permission::firstOrCreate(['name' => 'delete parts']);

        // Racks
        Permission::firstOrCreate(['name' => 'view racks']);
        Permission::firstOrCreate(['name' => 'create racks']);
        Permission::firstOrCreate(['name' => 'edit racks']);
        Permission::firstOrCreate(['name' => 'delete racks']);

        // Roles
        Permission::firstOrCreate(['name' => 'view roles']);
        Permission::firstOrCreate(['name' => 'create roles']);
        Permission::firstOrCreate(['name' => 'edit roles']);
        Permission::firstOrCreate(['name' => 'show roles']);
        Permission::firstOrCreate(['name' => 'delete roles']);

        // Shelves
        Permission::firstOrCreate(['name' => 'view shelves']);
        Permission::firstOrCreate(['name' => 'create shelves']);
        Permission::firstOrCreate(['name' => 'edit shelves']);
        Permission::firstOrCreate(['name' => 'show shelves']);
        Permission::firstOrCreate(['name' => 'delete shelves']);

        // Statuses
        Permission::firstOrCreate(['name' => 'view statuses']);
        Permission::firstOrCreate(['name' => 'create statuses']);
        Permission::firstOrCreate(['name' => 'edit statuses']);
        Permission::firstOrCreate(['name' => 'delete statuses']);

        // Suppliers
        Permission::firstOrCreate(['name' => 'view suppliers']);
        Permission::firstOrCreate(['name' => 'create suppliers']);
        Permission::firstOrCreate(['name' => 'edit suppliers']);
        Permission::firstOrCreate(['name' => 'show suppliers']);
        Permission::firstOrCreate(['name' => 'delete suppliers']);

        // Tasks
        Permission::firstOrCreate(['name' => 'view tasks']);
        Permission::firstOrCreate(['name' => 'create tasks']);
        Permission::firstOrCreate(['name' => 'edit tasks']);
        Permission::firstOrCreate(['name' => 'delete tasks']);

        // Time registrations
        Permission::firstOrCreate(['name' => 'view time-registrations']);
        Permission::firstOrCreate(['name' => 'create time-registrations']);
        Permission::firstOrCreate(['name' => 'edit time-registrations']);
        Permission::firstOrCreate(['name' => 'show time-registrations']);
        Permission::firstOrCreate(['name' => 'delete time-registrations']);

        // Units
        Permission::firstOrCreate(['name' => 'view units']);
        Permission::firstOrCreate(['name' => 'create units']);
        Permission::firstOrCreate(['name' => 'edit units']);
        Permission::firstOrCreate(['name' => 'show units']);
        Permission::firstOrCreate(['name' => 'delete units']);

        // Used parts
        Permission::firstOrCreate(['name' => 'view used-parts']);
        Permission::firstOrCreate(['name' => 'create used-parts']);
        Permission::firstOrCreate(['name' => 'edit used-parts']);
        Permission::firstOrCreate(['name' => 'show used-parts']);
        Permission::firstOrCreate(['name' => 'delete used-parts']);

        // Users
        Permission::firstOrCreate(['name' => 'view users']);
        Permission::firstOrCreate(['name' => 'create users']);
        Permission::firstOrCreate(['name' => 'edit users']);
        Permission::firstOrCreate(['name' => 'show users']);
        Permission::firstOrCreate(['name' => 'delete users']);

        // Warehouses
        Permission::firstOrCreate(['name' => 'view warehouses']);
        Permission::firstOrCreate(['name' => 'create warehouses']);
        Permission::firstOrCreate(['name' => 'edit warehouses']);
        Permission::firstOrCreate(['name' => 'show warehouses']);
        Permission::firstOrCreate(['name' => 'delete warehouses']);

        // Admin
        $role = Role::firstOrCreate(['name' => 'Admin']);
        $role->givePermissionTo(Permission::all());

        // Managing board
        $role = Role::firstOrCreate(['name' => 'Directie']);
        $role->givePermissionTo(Permission::all());

        // Mechanics
        $role = Role::firstOrCreate(['name' => 'Monteur']);
        $role->givePermissionTo([
            'view assignments',
            'show assignments',
            'view customers',
            'show customers',
            'view orders',
            'create orders',
            'edit orders',
            'show orders',
            'delete orders',
            'view parts',
            'create parts',
            'edit parts',
            'show parts',
            'delete parts',
            'view suppliers',
            'show suppliers',
            'view time-registrations',
            'create time-registrations',
            'edit time-registrations',
            'show time-registrations',
            'delete time-registrations',
            'view used-parts',
            'create used-parts',
            'edit used-parts',
            'show used-parts',
            'delete used-parts',
            'view users',
            'show users'
        ]);
    }
}
