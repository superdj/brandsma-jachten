<?php

use Illuminate\Database\Seeder;

class TimeRegistrationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\TimeRegistration::class, 100)->create();

        App\TimeRegistration::all()->each( function( $time_registration ) {
            $tasks = $time_registration->assignment->tasks;
            $time_registration->tasks()->attach(
                $tasks->random()
            );
        });
    }
}
