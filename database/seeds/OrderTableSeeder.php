<?php

use Illuminate\Database\Seeder;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Order::class, 10)->create();

        $parts = App\Part::all();

        App\Order::all()->each( function($order) use ( $parts ) {
           $order->parts()->attach(
               $parts->pluck('id')->random(),
               [
                   'amount' => rand(1, 50)
               ]
           );
        });
    }
}
