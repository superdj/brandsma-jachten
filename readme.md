# Getting started

1. Run the command the following command to install all node packages: `npm install`;

2. After that run the following command to install all composer packages: `composer install`;

3. Duplicate the `.env.example` file and rename 1 to `.env`;

4. Change the settings in the `.env` file to match your settings. 
Most important are the database settings;

5. Run the command: `php artisan key:generate`. To generate a new key;

6. After changing the settings run te following commands: 
`php artisan config:clear` and `php artisan config:cache`.
This will apply the new settings;

7. Run the command: `php artisan migrate`. 
This will update the database and make sure all tables are set;

8. Run the command: `npm run watch` or `npm run dev`.
This will build all the JavaScript and CSS resources

9. Also make sure to run the command: `php artisan storage:link`.
This will make sure all files in `storage/public` are available as a asset
