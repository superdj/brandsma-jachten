<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable = [ 'name', 'abbreviation', 'user_created', 'user_updated' ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function parts()
    {
        return $this->hasMany( Part::class );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo( User::class, 'user_created' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updater()
    {
        return $this->belongsTo( User::class, 'user_updated' );
    }

    /**
     * @param $query
     * @param $q
     *
     * @return mixed
     */
    public function scopeSearch( $query, $q )
    {
        if( $q === null )
        {
            return $query;
        }

        return $query->where( 'id', 'LIKE', "%{$q}%" )
                     ->orWhere( 'name', 'LIKE', "%{$q}%" )
                     ->orWhere( 'abbreviation', 'LIKE', "%{$q}%" );
    }
}
