<?php

namespace App;

class Cart {
    public $items = null;

    public function __construct( $oldCart )
    {
        if( $oldCart )
        {
            $this->items = $oldCart->items;
        }
    }

    /**
     * @param \App\Part $part
     * @param Float     $amount
     */
    public function add( Part $part, Float $amount )
    {
        $storeItem = ['item' => $part, 'amount' => $amount];

        if($this->items && array_key_exists($part->id, $this->items))
        {
            $storeItem = $this->items[$part->id];
        }
        $this->items[$part->id] = $storeItem;
    }

    /**
     * @param \App\Part $part
     * @param Float     $amount
     */
    public function update( Part $part, Float $amount )
    {
        $this->items[$part->id]['amount'] = $amount;
    }

    /**
     * @param \App\Part $part
     */
    public function destroy( Part $part )
    {
        unset($this->items[$part->id]);
    }
}
