<?php

namespace App\Exports;

use App\Assignment;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class AssignmentExport implements FromView
{
    private $assignment = Assignment::class;

    public function __construct( Assignment $assignment )
    {
        $this->assignment = $assignment;
    }

    public function view(): View
    {
        return view('assignments.export', [
            'assignment' => $this->assignment
        ]);
    }
}
