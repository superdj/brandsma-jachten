<?php

namespace App\Http\Controllers;

use App\Warehouse;
use Illuminate\Http\Request;

class WarehouseController extends Controller
{
    function __construct()
    {
        $this->middleware( 'permission:view warehouses', [ 'only' => [ 'index' ] ] );
        $this->middleware( 'permission:create warehouses', [ 'only' => [ 'create', 'store' ] ] );
        $this->middleware( 'permission:edit warehouses', [ 'only' => [ 'edit', 'update' ] ] );
        $this->middleware( 'permission:delete warehouses', [ 'only' => [ 'destroy' ] ] );
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Warehouse[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index( Request $request )
    {
        $sortBy = $request->sort ? $request->sort : 'name';
        $orderBy = $request->order ? $request->order : 'asc';
        $perPage = $request->perPage ? $request->perPage : 20;
        $q = $request->q ? $request->q : null;

        $warehouses = Warehouse::search( $q )->orderBy( $sortBy, $orderBy )->paginate( $perPage );

        return view( 'warehouses.index', compact( 'warehouses', 'sortBy', 'orderBy', 'perPage', 'q' ) );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'warehouses.create' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $request->merge( [ 'user_created' => auth()->user()->id ] );
        $this->validation( $request );

        $stored = Warehouse::create( $request->all() );

        return redirect()->route( 'warehouses.index' )
                         ->with( [
                             'message' => __( $stored ? 'model.stored' : 'model.not_stored', [ 'model' => trans_choice( 'model.Warehouse', 1 ), 'name' => $request->name ] ),
                             'success' => $stored,
                         ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Warehouse $warehouse
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( Warehouse $warehouse )
    {
        return view( 'warehouses.edit', compact( 'warehouse' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Warehouse           $warehouse
     *
     * @return void
     */
    public function update( Request $request, Warehouse $warehouse )
    {
        $request->merge( [ 'user_updated' => auth()->user()->id ] );
        $this->validation( $request, $warehouse->id );

        $updated = $warehouse->update( $request->all() );

        return redirect()->route( 'warehouses.index' )
                         ->with( [
                             'message' => __( $updated ? 'model.updated' : 'model.not_updated', [ 'model' => trans_choice( 'model.Warehouse', 1 ), 'name' => $request->name ] ),
                             'success' => $updated,
                         ] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Warehouse $warehouse
     *
     * @return void
     * @throws \Exception
     */
    public function destroy( Warehouse $warehouse )
    {
        $deleted = $warehouse->delete();

        return redirect()->route( 'warehouses.index' )
                         ->with( [
                             'message' => __( $deleted ? 'model.deleted' : 'model.not_deleted', [ 'model' => trans_choice( 'model.Warehouse', 1 ), 'name' => $warehouse->name ] ),
                             'success' => $deleted,
                         ] );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Int|null                 $id
     *
     * @return array|void
     */
    private function validation( Request $request, Int $id = null )
    {
        $request->validate( [
            'name' => 'required|string|max:255|'.$id ? 'unique:warehouses,name,'.$id : 'unique:warehouses',
        ] );
    }
}
