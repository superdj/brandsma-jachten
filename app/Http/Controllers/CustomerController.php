<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    function __construct()
    {
        $this->middleware( 'permission:view customers', [ 'only' => [ 'index' ] ] );
        $this->middleware( 'permission:create customers', [ 'only' => [ 'create', 'store' ] ] );
        $this->middleware( 'permission:edit customers', [ 'only' => [ 'edit', 'update' ] ] );
        $this->middleware( 'permission:show customers', [ 'only' => [ 'show' ] ] );
        $this->middleware( 'permission:delete customers', [ 'only' => [ 'destroy' ] ] );
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Customer[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index( Request $request )
    {
        $sortBy = $request->sort ? $request->sort : 'name';
        $orderBy = $request->order ? $request->order : 'asc';
        $perPage = $request->perPage ? $request->perPage : 20;
        $q = $request->q ? $request->q : null;

        $customers = Customer::search( $q )->orderBy( $sortBy, $orderBy )->paginate( $perPage );

        return view( 'customers.index', compact( 'customers', 'sortBy', 'orderBy', 'perPage', 'q' ) );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'customers.create' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \App\Customer $customer
     *
     * @return \Illuminate\Http\Response
     */
    public function show( Customer $customer )
    {
        return view( 'customers.show', compact( 'customer' ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $request->merge( [ 'user_created' => auth()->user()->id ] );
        $this->validation( $request );

        $stored = Customer::create( $request->all() );

        return redirect()->route( 'customers.index' )
                         ->with( [
                             'message' => __( $stored ? 'model.stored' : 'model.not_stored', [ 'model' => trans_choice( 'model.Customer', 1 ), 'name' => $request->name ] ),
                             'success' => $stored,
                         ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Customer $customer
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( Customer $customer )
    {
        return view( 'customers.edit', compact( 'customer' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Customer            $customer
     *
     * @return void
     */
    public function update( Request $request, Customer $customer )
    {
        $request->merge( [ 'user_updated' => auth()->user()->id ] );
        $this->validation( $request, $customer->id );

        $updated = $customer->update( $request->all() );

        return redirect()->route( 'customers.index' )
                         ->with( [
                             'message' => __( $updated ? 'model.updated' : 'model.not_updated', [ 'model' => trans_choice( 'model.Customer', 1 ), 'name' => $request->name ] ),
                             'success' => true,
                         ] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Customer $customer
     *
     * @return void
     * @throws \Exception
     */
    public function destroy( Customer $customer )
    {
        $deleted = $customer->delete();

        return redirect()->route( 'customers.index' )
                         ->with( [
                             'message' => __( $deleted ? 'model.deleted' : 'model.not_deleted', [ 'model' => trans_choice( 'model.Customer', 1 ), 'name' => $customer->name ] ),
                             'success' => $deleted,
                         ] );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Int|null                 $id
     *
     * @return array|void
     */
    private function validation( Request $request, Int $id = null )
    {
        $request->validate( [
            'name' => 'required|string|max:255|'.$id ? 'unique:customers,name,'.$id : 'unique:customers',
            'boat' => 'required|string|max:255',
        ] );
    }
}
