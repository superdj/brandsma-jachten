<?php

namespace App\Http\Controllers;

use App\Shelf;
use App\Rack;
use Illuminate\Http\Request;

class ShelfController extends Controller
{
    function __construct()
    {
        $this->middleware( 'permission:view shelves', [ 'only' => [ 'index' ] ] );
        $this->middleware( 'permission:create shelves', [ 'only' => [ 'create', 'store' ] ] );
        $this->middleware( 'permission:edit shelves', [ 'only' => [ 'edit', 'update' ] ] );
        $this->middleware( 'permission:show shelves', [ 'only' => [ 'show' ] ] );
        $this->middleware( 'permission:delete shelves', [ 'only' => [ 'destroy' ] ] );
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Shelf[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index( Request $request )
    {
        $sortBy = $request->sort ? $request->sort : 'rack_id';
        $orderBy = $request->order ? $request->order : 'desc';
        $perPage = $request->perPage ? $request->perPage : 20;
        $q = $request->q ? $request->q : null;

        $shelves = Shelf::search( $q )->orderBy( $sortBy, $orderBy )->paginate( $perPage );

        return view( 'shelves.index', compact( 'shelves', 'sortBy', 'orderBy', 'perPage', 'q' ) );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $racks = Rack::all();

        return view( 'shelves.create', compact( 'racks' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \App\Shelf $shelf
     *
     * @return \Illuminate\Http\Response
     */
    public function show( Shelf $shelf )
    {
        return view( 'shelves.show', compact( 'shelf' ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $request->merge( [ 'user_created' => auth()->user()->id ] );
        $this->validation( $request );

        $stored = Shelf::create( $request->all() );

        return redirect()->route( 'shelves.index' )
                         ->with( [
                             'message' => __( $stored ? 'model.stored' : 'model.not_stored', [ 'model' => trans_choice( 'model.Shelf', 1 ), 'name' => $request->name ] ),
                             'success' => $stored,
                         ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Shelf $shelf
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( Shelf $shelf )
    {
        $racks = Rack::all();

        return view( 'shelves.edit', compact( 'shelf', 'racks' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Shelf               $shelf
     *
     * @return void
     */
    public function update( Request $request, Shelf $shelf )
    {
        $request->merge( [ 'user_updated' => auth()->user()->id ] );
        $this->validation( $request, $shelf->id );

        $updated = $shelf->update( [
            'name'         => $request->name,
            'rack_id'      => $request->rack,
            'user_updated' => auth()->user()->id,
        ] );

        return redirect()->route( 'shelves.index' )
                         ->with( [
                             'message' => __( $updated ? 'model.updated' : 'model.not_updated', [ 'model' => trans_choice( 'model.Shelf', 1 ), 'name' => $request->name ] ),
                             'success' => $updated,
                         ] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Shelf $shelf
     *
     * @return void
     * @throws \Exception
     */
    public function destroy( Shelf $shelf )
    {
        $deleted = $shelf->delete();

        return redirect()->route( 'shelves.index' )
                         ->with( [
                             'message' => __( $deleted ? 'model.deleted' : 'model.not_deleted', [ 'model' => trans_choice( 'model.Shelf', 1 ), 'name' => $shelf->name ] ),
                             'success' => $deleted,
                         ] );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Int                      $id
     *
     * @return array|void
     */
    private function validation( Request $request, Int $id = null )
    {
        $request->validate( [
            'name' => 'required|string|max:255',
            'rack' => 'required|numeric|exists:racks,id',
        ] );
    }
}
