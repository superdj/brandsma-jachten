<?php

namespace App\Http\Controllers;

use App\Order;
use App\Part;
use App\Status;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    function __construct()
    {
        $this->middleware( 'permission:view orders', [ 'only' => [ 'index' ] ] );
        $this->middleware( 'permission:create orders', [ 'only' => [ 'create', 'store' ] ] );
        $this->middleware( 'permission:edit orders', [ 'only' => [ 'edit', 'update' ] ] );
        $this->middleware( 'permission:show orders', [ 'only' => [ 'show' ] ] );
        $this->middleware( 'permission:delete orders', [ 'only' => [ 'destroy' ] ] );
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Order[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index( Request $request )
    {
        $sortBy = $request->sort ? $request->sort : 'created_at';
        $orderBy = $request->order ? $request->order : 'asc';
        $perPage = $request->perPage ? $request->perPage : 20;
        $q = $request->q ? $request->q : null;

        $orders = Order::search( $q )->orderBy( $sortBy, $orderBy )->paginate( $perPage );

        return view( 'orders.index', compact( 'orders', 'sortBy', 'orderBy', 'perPage', 'q' ) );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parts = Part::all();
        $statuses = Status::where( 'model', '=', 'App\Order' )->get();

        return view( 'orders.create', compact( 'parts', 'statuses' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \App\Order $order
     *
     * @return \Illuminate\Http\Response
     */
    public function show( Order $order )
    {
        return view( 'orders.show', compact( 'order' ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $request->merge( [ 'user_created' => auth()->user()->id ] );
        $this->validation( $request );

        $stored = Order::create( $request->all() );

        $this->syncParts($stored, $request);

        return redirect()->route( 'orders.index' )
                         ->with( [
                             'message' => __( $stored ? 'model.stored' : 'model.not_stored', [ 'model' => trans_choice( 'model.Order', 1 ), 'name' => $request->name ] ),
                             'success' => $stored,
                         ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Order $order
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( Order $order )
    {
        $parts = Part::all();
        $statuses = Status::where( 'model', '=', 'App\Order' )->get();

        return view( 'orders.edit', compact( 'order', 'parts', 'statuses' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Order               $order
     *
     * @return void
     */
    public function update( Request $request, Order $order )
    {
        $request->merge( [ 'user_updated' => auth()->user()->id ] );
        $this->validation( $request, $order->id );

        $updated = $order->update( $request->all() );

        $this->syncParts($order, $request);

        return redirect()->route( 'orders.index' )
                         ->with( [
                             'message' => __( $updated ? 'model.updated' : 'model.updated', [ 'model' => trans_choice( 'model.Order', 1 ), 'name' => $request->name ] ),
                             'success' => $updated,
                         ] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Order $order
     *
     * @return void
     * @throws \Exception
     */
    public function destroy( Order $order )
    {
        $deleted = $order->delete();

        if( $deleted )
        {
            return redirect()->route( 'orders.index' )
                             ->with( [
                                 'message' => __( 'model.deleted', [ 'model' => trans_choice( 'model.Order', 1 ), 'name' => $order->name ] ),
                                 'success' => $deleted,
                             ] );
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Int|null                 $id
     *
     * @return array|void
     */
    private function validation( Request $request, Int $id = null )
    {
        $request->validate( [
            'name'      => 'required|string',
            'number'    => 'nullable|string',
            'status'    => 'required|integer|exists:statuses,id',
            'parts'     => 'required|array',
            'parts.*'   => 'exists:parts,id',
            'amounts'   => 'required|array',
            'amounts.*' => 'regex:/^\d+(\.\d{1,2})?$/',
        ] );
    }

    /**
     * @param \App\Order               $order
     * @param \Illuminate\Http\Request $request
     */
    private function syncParts( Order $order, Request $request )
    {
        if(!$request->parts)
        {
            return;
        }

        $parts = [];
        for( $i = 0; $i < count( $request->parts ); $i++ )
        {
            $parts[ $request->parts[ $i ] ] = [ 'amount' => str_replace( ',', '.', $request->amounts[ $i ] ) ];
        }

        $order->parts()->sync( $parts );
    }
}
