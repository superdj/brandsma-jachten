<?php

namespace App\Http\Controllers;

use App\UsedPart;
use App\Shelf;
use App\Assignment;
use Illuminate\Http\Request;

class UsedPartController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:view used-parts', ['only' => ['index']]);
        $this->middleware('permission:create used-parts', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit used-parts', ['only' => ['edit', 'update']]);
        $this->middleware('permission:show used-parts', ['only' => ['show']]);
        $this->middleware('permission:delete used-parts', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\UsedPart[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index( Request $request )
    {
        $sortBy = $request->sort ? $request->sort : 'assignment_id';
        $orderBy = $request->order ? $request->order : 'desc';
        $perPage = $request->perPage ? $request->perPage : 20;
        $q = $request->q ? $request->q : null;

        $used_parts = UsedPart::search($q)->orderBy($sortBy, $orderBy)->paginate($perPage);

        return view( 'used-parts.index', compact( 'used_parts', 'sortBy', 'orderBy', 'perPage', 'q' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \App\Assignment $assignment
     *
     * @return \Illuminate\Http\Response
     */
    public function create( Assignment $assignment )
    {
        $shelves = Shelf::all();
        return view( 'used-parts.create', compact('assignment', 'shelves') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Assignment          $assignment
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request, Assignment $assignment )
    {
        $this->validation( $request );

        /**
         * Create array for used parts
         */
        $sync = [];
        for( $i = 0; $i < count( $request->parts ); $i++ )
        {
            $part_id = explode( '_', $request->parts[$i] )[0];
            $sync[$part_id] = ['amount' => str_replace(',', '.', $request->amounts[$i])];
        }

        $assignment->parts()->attach( $sync );

        /** Create array to decrease stock */
        for( $i = 0; $i < count( $request->parts ); $i++ )
        {
            $ids = explode('_', $request->parts[$i] );
            $part_id = $ids[0];
            $shelf = Shelf::find($ids[1]);
            $stock = $shelf->parts()->where('part_id', $part_id)->first()->pivot->stock;
            $new_stock = $stock - str_replace(',', '.', $request->amounts[$i]);

            $shelf->parts()->updateExistingPivot($part_id, ['stock' => $new_stock]);
        }

        return redirect()->route('assignments.index')
            ->with('message', __('model.stored', ['model' => trans_choice('model.Used part', 2), 'name' => $assignment->name]))
            ->with('success', true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\UsedPart $used_part
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( UsedPart $used_part )
    {
        return view( 'used-parts.edit', compact( 'used_part' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\UsedPart            $usedPart
     *
     * @return void
     */
    public function update( Request $request, UsedPart $usedPart )
    {
        $request->merge(['amount' => str_replace(',', '.', $request->amount)]);

        $request->validate([
            'amount' => 'required|max:10|regex:/^\d+(\.\d{2})?$/'
        ]);

        $updated = $usedPart->update([
            'amount' => $request->amount
        ]);

        if( $updated )
        {
            return redirect()->route('used-parts.index')
            ->with('message', __('model.updated', ['model' => trans_choice('model.Used part', 1), 'name' => $usedPart->part->name]))
            ->with('success', true);
        } else {
            return redirect()->route('used-parts.index')
            ->with('message', __('model.not_updated', ['model' => trans_choice('model.Used part', 1), 'name' => $usedPart->part->name]))
            ->with('success', false);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\UsedPart $used_part
     *
     * @return void
     * @throws \Exception
     */
    public function destroy( UsedPart $used_part )
    {
        $deleted = $used_part->delete();

        if( $deleted )
        {
            return redirect()->route('used-parts.index')
                ->with('message', __('model.deleted', ['model' => trans_choice('model.Time registration', 1), 'name' => $used_part->name]))
                ->with('success', true);
        } else {
            return redirect()->route('used-parts.index')
                ->with('message', __('model.not_deleted', ['model' => trans_choice('model.Time registration', 1), 'name' => $used_part->name]))
                ->with('success', false);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Int|null                 $id
     *
     * @return array|void
     */
    private function validation( Request $request, Int $id = null )
    {
        $request->validate([
            'parts' => 'required|array',
            'parts.*' => 'exists:parts,id',
            'amounts' => 'required|array',
            'amounts.*' => 'regex:/^\d+(\.\d{1,2})?$/'
        ]);
    }
}
