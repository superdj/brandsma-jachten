<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    function __construct()
    {
        $this->middleware( 'permission:view users', [ 'only' => [ 'index' ] ] );
        $this->middleware( 'permission:create users', [ 'only' => [ 'create', 'store' ] ] );
        $this->middleware( 'permission:edit users', [ 'only' => [ 'edit', 'update' ] ] );
        $this->middleware( 'permission:show users', [ 'only' => [ 'show' ] ] );
        $this->middleware( 'permission:delete users', [ 'only' => [ 'destroy' ] ] );
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index( Request $request )
    {
        $sortBy = $request->sort ? $request->sort : 'first_name';
        $orderBy = $request->order ? $request->order : 'asc';
        $perPage = $request->perPage ? $request->perPage : 20;
        $q = $request->q ? $request->q : null;

        $users = User::search( $q )->orderBy( $sortBy, $orderBy )->paginate( $perPage );

        return view( 'users.index', compact( 'users', 'sortBy', 'orderBy', 'perPage', 'q' ) );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'users.create' );
    }

    /**
     * Display the specified resource.
     *
     * @param \App\User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function show( User $user )
    {
        return view( 'users.show', compact( 'user' ) );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( User $user )
    {
        $roles = Role::all();

        return view( 'users.edit', compact( 'user', 'roles' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\User                $user
     *
     * @return void
     */
    public function update( Request $request, User $user )
    {
        $request->merge( [ 'user_updated' => auth()->user()->id ] );
        $this->validation( $request, $user->id );

        $updated = $user->update( $request->all() );

        $user->syncRoles( $request->roles );

        return redirect()->route( 'users.index' )
                         ->with( [
                             'message' => __( $updated ? 'model.updated' : 'model.not_updated', [ 'model' => trans_choice( 'model.User', 1 ), 'name' => $request->first_name.' '.$request->last_name ] ),
                             'success' => $updated,
                         ] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\User $user
     *
     * @return void
     * @throws \Exception
     */
    public function destroy( User $user )
    {
        $deleted = $user->delete();

        return redirect()->route( 'users.index' )
                         ->with( [
                             'message' => __( $deleted ? 'model.deleted' : 'model.not_deleted', [ 'model' => trans_choice( 'model.User', 1 ), 'name' => $user->first_name.' '.$user->last_name ] ),
                             'success' => $deleted,
                         ] );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Int|null                 $id
     *
     * @return array|void
     */
    private function validation( Request $request, Int $id = null )
    {
        $request->validate( [
            'first_name' => 'required|string|max:255',
            'last_name'  => 'required|string|max:255',
            'email'      => 'required|string|email|max:255|'.$id ? 'unique:users,email,'.$id : 'unique:users',
            'roles'      => 'required|array',
            'roles.*'    => 'exists:roles,id',
        ] );
    }
}
