<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Part;
use App\Cart;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \App\Assignment[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        if( session()->has( 'cart' ) )
        {
            $items = session()->get( 'cart' )->items;

            return view( 'cart.index', compact( 'items' ) );
        } else
        {
            return redirect()->back()->with( [ 'message' => __( 'model.not_found', [ 'model' => __( 'model.Cart' ) ] ) ] );
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Part                $part
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function add( Request $request, Part $part )
    {
        $oldCart = session()->has( 'cart' ) ? session()->get( 'cart' ) : null;
        $cart = new Cart( $oldCart );
        $cart->add( $part, str_replace( ',', '.', $request->amount ) );

        $request->session()->put( 'cart', $cart );

        return redirect()->back()
                         ->with( [
                             'message' => __( 'model.added_to_cart', [ 'part' => $part->name ] ),
                             'success' => true,
                         ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Part $part
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( Part $part )
    {
        $item = session()->get( 'cart' )->items[ $part->id ];

        return view( 'cart.edit', compact( 'item', 'part' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Part                $part
     *
     * @return void
     */
    public function update( Request $request, Part $part )
    {
        $oldCart = session()->has( 'cart' ) ? session()->get( 'cart' ) : null;
        $cart = new Cart( $oldCart );
        $cart->update( $part, str_replace( ',', '.', $request->amount ) );

        session()->put( 'cart', $cart );

        return redirect()->route( 'cart.index' )
                         ->with( [
                             'message' => __( 'model.Cart edited' ),
                             'success' => true,
                         ] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Part $part
     *
     * @return void
     * @throws \Exception
     */
    public function destroy( Part $part )
    {
        $oldCart = session()->has( 'cart' ) ? session()->get( 'cart' ) : null;
        $cart = new Cart( $oldCart );
        $cart->destroy( $part );

        session()->put( 'cart', $cart );

        return redirect()->route( 'cart.index' )
                         ->with( [
                             'message' => __( 'model.Cart delete', [ 'part' => $part->name ] ),
                             'success' => true,
                         ] );
    }
}
