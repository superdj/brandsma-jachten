<?php

namespace App\Http\Controllers;

use App\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    function __construct()
    {
        $this->middleware( 'permission:view suppliers', [ 'only' => [ 'index' ] ] );
        $this->middleware( 'permission:create suppliers', [ 'only' => [ 'create', 'store' ] ] );
        $this->middleware( 'permission:edit suppliers', [ 'only' => [ 'edit', 'update' ] ] );
        $this->middleware( 'permission:show suppliers', [ 'only' => [ 'show' ] ] );
        $this->middleware( 'permission:delete suppliers', [ 'only' => [ 'destroy' ] ] );
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Supplier[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index( Request $request )
    {
        $sortBy = $request->sort ? $request->sort : 'name';
        $orderBy = $request->order ? $request->order : 'asc';
        $perPage = $request->perPage ? $request->perPage : 20;
        $q = $request->q ? $request->q : null;

        $suppliers = Supplier::search( $q )->orderBy( $sortBy, $orderBy )->paginate( $perPage );

        return view( 'suppliers.index', compact( 'suppliers', 'sortBy', 'orderBy', 'perPage', 'q' ) );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'suppliers.create', compact( 'shelves' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \App\Supplier $supplier
     *
     * @return \Illuminate\Http\Response
     */
    public function show( Supplier $supplier )
    {
        return view( 'suppliers.show', compact( 'supplier' ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $request->merge( [ 'user_created' => auth()->user()->id ] );
        $this->validation( $request );

        $stored = Supplier::create( $request->all() );

        return redirect()->route( 'suppliers.index' )
                         ->with( [
                             'message' => __( $stored ? 'model.stored' : 'model.not_stored', [ 'model' => trans_choice( 'model.Supplier', 1 ), 'name' => $request->name ] ),
                             'success' => $stored,
                         ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Supplier $supplier
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( Supplier $supplier )
    {
        return view( 'suppliers.edit', compact( 'supplier', 'shelves' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Supplier            $supplier
     *
     * @return void
     */
    public function update( Request $request, Supplier $supplier )
    {
        $request->merge( [ 'user_updated' => auth()->user()->id ] );
        $this->validation( $request, $supplier->id );

        $updated = $supplier->update( $request->all() );

        return redirect()->route( 'suppliers.index' )
                         ->with( [
                             'message' => __( $updated ? 'model.updated' : 'model.not_updated', [ 'model' => trans_choice( 'model.Supplier', 1 ), 'name' => $request->name ] ),
                             'success' => $updated,
                         ] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Supplier $supplier
     *
     * @return void
     * @throws \Exception
     */
    public function destroy( Supplier $supplier )
    {
        $deleted = $supplier->delete();

        return redirect()->route( 'suppliers.index' )
                         ->with( [
                             'message' => __( $deleted ? 'model.deleted' : 'model.not_deleted', [ 'model' => trans_choice( 'model.Supplier', 1 ), 'name' => $supplier->name ] ),
                             'success' => $deleted,
                         ] );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Int|null                 $id
     *
     * @return array|void
     */
    private function validation( Request $request, Int $id = null )
    {
        $request->validate( [
            'name'          => 'required|string|max:255|'.$id ? 'unique:suppliers,name,'.$id : 'unique:suppliers',
            'number'        => 'required|'.$id ? 'unique:suppliers,number,'.$id : 'unique:suppliers',
            'city'          => 'string|max:255|nullable',
            'postal_code'   => 'string|max:255|nullable',
            'street'        => 'string|max:255|nullable',
            'house_number'  => 'string|max:255|nullable',
            'email'         => 'string|email|nullable',
            'phone_number'  => 'string|max:13|nullable',
            'mobile_number' => 'string|max:13|nullable',
            'website'       => 'string|url|nullable',
        ] );
    }
}
