<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    function __construct()
    {
        $this->middleware( 'permission:view tasks', [ 'only' => [ 'index' ] ] );
        $this->middleware( 'permission:create tasks', [ 'only' => [ 'create', 'store' ] ] );
        $this->middleware( 'permission:edit tasks', [ 'only' => [ 'edit', 'update' ] ] );
        $this->middleware( 'permission:delete tasks', [ 'only' => [ 'destroy' ] ] );
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Task[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index( Request $request )
    {
        $sortBy = $request->sort ? $request->sort : 'name';
        $orderBy = $request->order ? $request->order : 'asc';
        $perPage = $request->perPage ? $request->perPage : 20;
        $q = $request->q ? $request->q : null;

        $tasks = Task::search( $q )->orderBy( $sortBy, $orderBy )->paginate( $perPage );

        return view( 'tasks.index', compact( 'tasks', 'sortBy', 'orderBy', 'perPage', 'q' ) );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'tasks.create' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \App\Task $task
     *
     * @return \Illuminate\Http\Response
     */
    public function show( Task $task )
    {
        return view( 'tasks.show', compact( 'task' ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $request->merge( [ 'user_created' => auth()->user()->id ] );
        $this->validation( $request );

        $stored = Task::create( $request->all() );

        return redirect()->route( 'tasks.index' )
                         ->with( [
                             'message' => __( $stored ? 'model.stored' : 'model.not_stored', [ 'model' => trans_choice( 'model.Task', 1 ), 'name' => $request->name ] ),
                             'success' => $stored,
                         ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Task $task
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( Task $task )
    {
        return view( 'tasks.edit', compact( 'task' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Task                $task
     *
     * @return void
     */
    public function update( Request $request, Task $task )
    {
        $request->merge( [ 'user_updated' => auth()->user()->id ] );
        $this->validation( $request, $task->id );

        $updated = $task->update( $request->all() );

        return redirect()->route( 'tasks.index' )
                         ->with( [
                             'message' => __( $updated ? 'model.updated' : 'model.not_updated', [ 'model' => trans_choice( 'model.Task', 1 ), 'name' => $request->name ] ),
                             'success' => $updated,
                         ] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Task $task
     *
     * @return void
     * @throws \Exception
     */
    public function destroy( Task $task )
    {
        $deleted = $task->delete();

        return redirect()->route( 'tasks.index' )
                         ->with( [
                             'message' => __( $deleted ? 'model.deleted' : 'model.not_deleted', [ 'model' => trans_choice( 'model.Task', 1 ), 'name' => $task->name ] ),
                             'success' => $deleted,
                         ] );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Int|null                 $id
     *
     * @return array|void
     */
    private function validation( Request $request, Int $id = null )
    {
        $request->validate( [
            'name' => 'required|string|max:255|'.$id ? 'unique:tasks,name,'.$id : 'unique:tasks',
        ] );
    }
}
