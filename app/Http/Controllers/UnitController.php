<?php

namespace App\Http\Controllers;

use App\Unit;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    function __construct()
    {
        $this->middleware( 'permission:view units', [ 'only' => [ 'index' ] ] );
        $this->middleware( 'permission:create units', [ 'only' => [ 'create', 'store' ] ] );
        $this->middleware( 'permission:edit units', [ 'only' => [ 'edit', 'update' ] ] );
        $this->middleware( 'permission:delete units', [ 'only' => [ 'destroy' ] ] );
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Unit[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index( Request $request )
    {
        $sortBy = $request->sort ? $request->sort : 'name';
        $orderBy = $request->order ? $request->order : 'asc';
        $perPage = $request->perPage ? $request->perPage : 20;
        $q = $request->q ? $request->q : null;

        $units = Unit::search( $q )->orderBy( $sortBy, $orderBy )->paginate( $perPage );

        return view( 'units.index', compact( 'units', 'sortBy', 'orderBy', 'perPage', 'q' ) );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'units.create' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \App\Unit $unit
     *
     * @return \Illuminate\Http\Response
     */
    public function show( Unit $unit )
    {
        return view( 'units.show', compact( 'unit' ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $request->merge( [
            'step', str_replace( ',', '.', $request->step ),
            'user_created' => auth()->user()->id,
        ] );
        $this->validation( $request );

        $stored = Unit::create( [
            'name'         => $request->name,
            'abbreviation' => $request->abbreviation,
            'user_created' => auth()->user()->id,
        ] );

        return redirect()->route( 'units.index' )
                         ->with( [
                             'message' => __( $stored ? 'model.stored' : 'model.not_stored', [ 'model' => trans_choice( 'model.Unit', 1 ), 'name' => $request->name ] ),
                             'success' => $stored,
                         ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Unit $unit
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( Unit $unit )
    {
        return view( 'units.edit', compact( 'unit' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Unit                $unit
     *
     * @return void
     */
    public function update( Request $request, Unit $unit )
    {
        $request->merge( [
            'step', str_replace( ',', '.', $request->step ),
            'user_updated' => auth()->user()->id,
        ] );
        $this->validation( $request, $unit->id );

        $updated = $unit->update( $request->all() );

        return redirect()->route( 'units.index' )
                         ->with( [
                             'message' => __( $updated ? 'model.updated' : 'model.not_updated', [ 'model' => trans_choice( 'model.Unit', 1 ), 'name' => $request->name ] ),
                             'success' => $updated,
                         ] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Unit $unit
     *
     * @return void
     * @throws \Exception
     */
    public function destroy( Unit $unit )
    {
        $deleted = $unit->delete();

        return redirect()->route( 'units.index' )
                         ->with( [
                             'message' => __( $deleted ? 'model.deleted' : 'model.not_deleted', [ 'model' => trans_choice( 'model.Unit', 1 ), 'name' => $unit->name ] ),
                             'success' => $deleted,
                         ] );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Int|null                 $id
     *
     * @return array|void
     */
    private function validation( Request $request, Int $id = null )
    {
        $request->validate( [
            'name'         => 'required|string|max:255|'.$id ? 'unique:units,name,'.$id : 'unique:units',
            'abbreviation' => 'required|string',
        ] );
    }
}
