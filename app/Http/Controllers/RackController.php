<?php

namespace App\Http\Controllers;

use App\Rack;
use App\Warehouse;
use Illuminate\Http\Request;

class RackController extends Controller
{
    function __construct()
    {
        $this->middleware( 'permission:view racks', [ 'only' => [ 'index' ] ] );
        $this->middleware( 'permission:create racks', [ 'only' => [ 'create', 'store' ] ] );
        $this->middleware( 'permission:edit racks', [ 'only' => [ 'edit', 'update' ] ] );
        $this->middleware( 'permission:delete racks', [ 'only' => [ 'destroy' ] ] );
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Rack[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index( Request $request )
    {
        $sortBy = $request->sort ? $request->sort : 'warehouse_id';
        $orderBy = $request->order ? $request->order : 'desc';
        $perPage = $request->perPage ? $request->perPage : 20;
        $q = $request->q ? $request->q : null;

        $racks = Rack::search( $q )->orderBy( $sortBy, $orderBy )->paginate( $perPage );

        return view( 'racks.index', compact( 'racks', 'sortBy', 'orderBy', 'perPage', 'q' ) );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $warehouses = Warehouse::all();

        return view( 'racks.create', compact( 'warehouses' ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $request->merge( [ 'user_created' => auth()->user()->id ] );
        $this->validation( $request );

        $stored = Rack::create( $request->all() );

        return redirect()->route( 'racks.index' )
                         ->with( [
                             'message' => __( $stored ? 'model.stored' : 'model.not_stored', [ 'model' => trans_choice( 'model.Rack', 1 ), 'name' => $request->name ] ),
                             'success' => $stored,
                         ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Rack $rack
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( Rack $rack )
    {
        $warehouses = Warehouse::all();

        return view( 'racks.edit', compact( 'rack', 'warehouses' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Rack                $rack
     *
     * @return void
     */
    public function update( Request $request, Rack $rack )
    {
        $request->merge( [ 'user_updated' => auth()->user()->id ] );
        $this->validation( $request, $rack->id );

        $updated = $rack->update( $request->all() );

        return redirect()->route( 'racks.index' )
                         ->with( [
                             'message' => __( $updated ? 'model.updated' : 'model.not_updated', [ 'model' => trans_choice( 'model.Rack', 1 ), 'name' => $request->name ] ),
                             'success' => $updated,
                         ] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Rack $rack
     *
     * @return void
     * @throws \Exception
     */
    public function destroy( Rack $rack )
    {
        $deleted = $rack->delete();

        return redirect()->route( 'racks.index' )
                         ->with( [
                             'message' => __( $deleted ? 'model.deleted' : 'model.not_deleted', [ 'model' => trans_choice( 'model.Rack', 1 ), 'name' => $rack->name ] ),
                             'success' => $deleted,
                         ] );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Int                      $id
     *
     * @return array|void
     */
    private function validation( Request $request, Int $id = null )
    {
        $request->validate( [
            'name'      => 'required|string|max:255',
            'warehouse' => 'required|numeric|exists:warehouses,id',
        ] );
    }
}
