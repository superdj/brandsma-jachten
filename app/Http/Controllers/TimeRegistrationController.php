<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\TimeRegistration;
use Illuminate\Http\Request;

class TimeRegistrationController extends Controller
{
    function __construct()
    {
        $this->middleware( 'permission:view time-registrations', [ 'only' => [ 'index' ] ] );
        $this->middleware( 'permission:create time-registrations', [ 'only' => [ 'create', 'store' ] ] );
        $this->middleware( 'permission:edit time-registrations', [ 'only' => [ 'edit', 'update' ] ] );
        $this->middleware( 'permission:show time-registrations', [ 'only' => [ 'show' ] ] );
        $this->middleware( 'permission:delete time-registrations', [ 'only' => [ 'destroy' ] ] );
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\TimeRegistration[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index( Request $request )
    {
        $sortBy = $request->sort ? $request->sort : 'user_id';
        $orderBy = $request->order ? $request->order : 'desc';
        $perPage = $request->perPage ? $request->perPage : 20;
        $q = $request->q ? $request->q : null;

        // TODO would be nice to make the roles dynamic
        if( auth()->user()->hasAnyRole( [ 'Directie', 'Admin' ] ) )
        {
            $time_registrations = TimeRegistration::search( $q )->orderBy( $sortBy, $orderBy )->paginate( $perPage );
        } else
        {
            $time_registrations = TimeRegistration::where( 'user_id', '=', auth()->user()->id )->search( $q )->orderBy( $sortBy, $orderBy )->paginate( $perPage );
        }

        return view( 'time-registrations.index', compact( 'time_registrations', 'sortBy', 'orderBy', 'perPage', 'q' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \App\Assignment $assignment
     *
     * @return \Illuminate\Http\Response
     */
    public function create( Assignment $assignment )
    {
        return view( 'time-registrations.create', compact( 'assignment' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \App\TimeRegistration $time_registration
     *
     * @return \Illuminate\Http\Response
     */
    public function show( TimeRegistration $time_registration )
    {
        return view( 'time-registrations.show', compact( 'time_registration' ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $request->merge( [ 'user_created' => auth()->user()->id ] );
        $this->validation( $request );

        $stored = TimeRegistration::create( $request->all() );

        $stored->tasks()->sync( $request->tasks );

        return redirect()->route( 'time-registrations.index' )
                         ->with( [
                             'message' => __( $stored ? 'model.stored' : 'model.not_stored', [ 'model' => trans_choice( 'model.Time registration', 1 ), 'name' => $request->name ] ),
                             'success' => $stored,
                         ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\TimeRegistration $time_registration
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( TimeRegistration $time_registration )
    {
        $assignment = Assignment::find( $time_registration->assignment->id );

        return view( 'time-registrations.edit', compact( 'time_registration', 'assignment' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\TimeRegistration    $time_registration
     *
     * @return void
     */
    public function update( Request $request, TimeRegistration $time_registration )
    {
        $request->merge( [ 'user_updated' => auth()->user()->id ] );
        $this->validation( $request, $time_registration->id );

        $updated = $time_registration->update( $request->all() );

        $time_registration->tasks()->sync( $request->tasks );

        return redirect()->route( 'time-registrations.index' )
                         ->with( [
                             'message' => __( $updated ? 'model.updated' : 'model.not_updated', [ 'model' => trans_choice( 'model.Time registration', 1 ), 'name' => $request->name ] ),
                             'success' => $updated,
                         ] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\TimeRegistration $time_registration
     *
     * @return void
     * @throws \Exception
     */
    public function destroy( TimeRegistration $time_registration )
    {
        $deleted = $time_registration->delete();

        return redirect()->route( 'time-registrations.index' )
                         ->with( [
                             'message' => __( $deleted ? 'model.deleted' : 'model.not_deleted', [ 'model' => trans_choice( 'model.Time registration', 1 ), 'name' => $time_registration->name ] ),
                             'success' => $deleted,
                         ] );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Int|null                 $id
     *
     * @return array|void
     */
    private function validation( Request $request, Int $id = null )
    {
        $request->validate( [
            'time'       => 'required|date_format:H:i',
            'tasks'      => 'nullable|array',
            'tasks.*'    => 'nullable|exists:tasks,id',
            'assignment' => 'required|exists:assignments,id',
            'user'       => 'required|exists:users,id',
        ] );
    }
}
