<?php

namespace App\Http\Controllers;

use App\Status;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    function __construct()
    {
        $this->middleware( 'permission:view statuses', [ 'only' => [ 'index' ] ] );
        $this->middleware( 'permission:create statuses', [ 'only' => [ 'create', 'store' ] ] );
        $this->middleware( 'permission:edit statuses', [ 'only' => [ 'edit', 'update' ] ] );
        $this->middleware( 'permission:delete statuses', [ 'only' => [ 'destroy' ] ] );
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Status[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index( Request $request )
    {
        $sortBy = $request->sort ? $request->sort : 'model';
        $orderBy = $request->order ? $request->order : 'desc';
        $perPage = $request->perPage ? $request->perPage : 20;
        $q = $request->q ? $request->q : null;

        $statuses = Status::search( $q )->orderBy( $sortBy, $orderBy )->paginate( $perPage );

        return view( 'statuses.index', compact( 'statuses', 'sortBy', 'orderBy', 'perPage', 'q' ) );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'statuses.create' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \App\Status $status
     *
     * @return \Illuminate\Http\Response
     */
    public function show( Status $status )
    {
        return view( 'statuses.show', compact( 'status' ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $request->merge( [ 'user_created' => auth()->user()->id ] );
        $this->validation( $request );

        $stored = Status::create( $request->all() );

        return redirect()->route( 'statuses.index' )
                         ->with( [
                             'message' => __( $stored ? 'model.stored' : 'model.not_stored', [ 'model' => trans_choice( 'model.Status', 1 ), 'name' => $request->name ] ),
                             'success' => $stored,
                         ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Status $status
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( Status $status )
    {
        return view( 'statuses.edit', compact( 'status' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Status              $status
     *
     * @return void
     */
    public function update( Request $request, Status $status )
    {
        $request->merge( [ 'user_updated' => auth()->user()->id ] );
        $this->validation( $request, $status->id );

        $updated = $status->update( $request->all() );

        return redirect()->route( 'statuses.index' )
                         ->with( [
                             'message' => __( $updated ? 'model.updated' : 'model.not_updated', [ 'model' => trans_choice( 'model.Status', 1 ), 'name' => $request->name ] ),
                             'success' => $updated,
                         ] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Status $status
     *
     * @return void
     * @throws \Exception
     */
    public function destroy( Status $status )
    {
        $deleted = $status->delete();

        return redirect()->route( 'statuses.index' )
                         ->with( [
                             'message' => __( $deleted ? 'model.deleted' : 'model.not_deleted', [ 'model' => trans_choice( 'model.Status', 1 ), 'name' => $status->name ] ),
                             'success' => $deleted,
                         ] );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Int|null                 $id
     *
     * @return array|void
     */
    private function validation( Request $request, Int $id = null )
    {
        $request->validate( [
            'name'  => 'required|string|max:255|'.$id ? 'unique:statuses,name,'.$id : 'unique:statuses',
            'model' => 'required|string',
            'lock'  => 'boolean',
        ] );
    }
}
