<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Part;
use App\Order;
use App\Assignment;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $parts = DB::table('parts')
            ->leftJoin('part_shelf', 'part_shelf.part_id', '=', 'parts.id')
            ->leftJoin('units', 'units.id', '=', 'parts.unit_id')
            ->select('parts.id', 'parts.minimal_stock', 'parts.name as part', 'units.abbreviation as abbreviation', 'units.name as unit')
            ->selectRaw('SUM(part_shelf.stock) as stock')
            ->groupBy('parts.id', 'parts.minimal_stock', 'parts.name', 'units.abbreviation', 'units.name')
            ->orderBy('stock', 'asc')
            ->limit(10)
            ->get();
        $orders = Order::orderBy('created_at', 'desc')->limit(10)->get();
        $assignments = Assignment::orderBy('end_date', 'asc')->limit(10)->get();
        return view('dashboard.index', compact('parts', 'orders', 'assignments'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function manual()
    {
        return view('dashboard.manual');
    }
}
