<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Customer;
use App\Exports\AssignmentExport;
use App\Status;
use App\Task;
use App\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class AssignmentController extends Controller
{
    function __construct()
    {
        $this->middleware( 'permission:view assignments', [ 'only' => [ 'index' ] ] );
        $this->middleware( 'permission:create assignments', [ 'only' => [ 'create', 'store' ] ] );
        $this->middleware( 'permission:edit assignments', [ 'only' => [ 'edit', 'update' ] ] );
        $this->middleware( 'permission:show assignments', [ 'only' => [ 'show' ] ] );
        $this->middleware( 'permission:delete assignments', [ 'only' => [ 'destroy' ] ] );
        $this->middleware( 'permission:export assignments', [ 'only' => [ 'export' ] ] );
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Assignment[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index( Request $request )
    {
        $sortBy = $request->sort ? $request->sort : 'end_date';
        $orderBy = $request->order ? $request->order : 'asc';
        $perPage = $request->perPage ? $request->perPage : 20;
        $q = $request->q ? $request->q : null;

        $assignments = Assignment::search( $q )->orderBy( $sortBy, $orderBy )->paginate( $perPage );

        return view( 'assignments.index', compact( 'assignments', 'sortBy', 'orderBy', 'perPage', 'q' ) );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::all();
        $statuses = Status::where( 'model', '=', 'App\Assignment' )->get();
        $users = User::all(); // TODO filter on user role. But would be nice if roles are dynamic
        $tasks = Task::all();

        return view( 'assignments.create', compact( 'customers', 'statuses', 'users', 'tasks' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \App\Assignment $assignment
     *
     * @return \Illuminate\Http\Response
     */
    public function show( Assignment $assignment )
    {
        return view( 'assignments.show', compact( 'assignment' ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $this->validation( $request );

        $request->merge( [
            'start_date'   => $request->start_date.' '.$request->start_time,
            'end_date'     => $request->end_date.' '.$request->end_time,
            'user_created' => auth()->user()->id,
        ] );

        $stored = Assignment::create( $request->all() );

        $stored->users()->sync( $request->users );
        $stored->tasks()->sync( $request->tasks );

        return redirect()->route( 'assignments.index' )
                         ->with( [
                             'message' => __( $stored ? 'model.stored' : 'model.not_stored', [ 'model' => trans_choice( 'model.Assignment', 1 ), 'name' => $request->name ] ),
                             'success' => $stored,
                         ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Assignment $assignment
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( Assignment $assignment )
    {
        $customers = Customer::all();
        $statuses = Status::where( 'model', '=', 'App\Assignment' )->get();
        $users = User::all();
        $tasks = Task::all();

        return view( 'assignments.edit', compact( 'assignment', 'customers', 'statuses', 'users', 'tasks' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Assignment          $assignment
     *
     * @return void
     */
    public function update( Request $request, Assignment $assignment )
    {
        $this->validation( $request, $assignment->id );

        $request->merge( [
            'start_date'   => $request->start_date.' '.$request->start_time,
            'end_date'     => $request->end_date.' '.$request->end_time,
            'user_updated' => auth()->user()->id,
        ] );

        $updated = $assignment->update( $request->all() );

        $assignment->users()->sync( $request->users );
        $assignment->tasks()->sync( $request->tasks );

        return redirect()->route( 'assignments.index' )
                         ->with( [
                             'message' => __( $updated ? 'model.updated' : 'model.not_updated', [ 'model' => trans_choice( 'model.Assignment', 1 ), 'name' => $request->name ] ),
                             'success' => $updated,
                         ] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Assignment $assignment
     *
     * @return void
     * @throws \Exception
     */
    public function destroy( Assignment $assignment )
    {
        $deleted = $assignment->delete();

        return redirect()->route('assignments.index')
                         ->with([
                             'message' => __($deleted ? 'model.deleted' : 'model.not_deleted', ['model' => trans_choice('model.Assignment', 1), 'name' => $request->name]),
                             'success' => $deleted
                         ]);
    }

    /**
     * Export all used parts
     *
     * @param \App\Assignment $assignment
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export( Assignment $assignment )
    {
        return Excel::download( new AssignmentExport( $assignment ), $assignment->name.'.xlsx' );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Int|null                 $id
     *
     * @return array|void
     */
    private function validation( Request $request, Int $id = null )
    {
        $request->validate( [
            'name'        => 'required|string',
            'description' => 'nullable|string',
            'start_date'  => 'required|date_format:Y-m-d|before:end_date',
            'start_time'  => 'required|date_format:H:i:s',
            'end_date'    => 'required|date_format:Y-m-d|after:start_date',
            'end_time'    => 'required|date_format:H:i:s',
            'customer'    => 'required|integer|exists:customers,id',
            'status'      => 'required|integer|exists:statuses,id',
            'users'       => 'required|array',
            'users.*'     => 'exists:users,id',
            'tasks'       => 'required|array',
            'tasks.*'     => 'exists:tasks,id',
        ] );
    }
}
