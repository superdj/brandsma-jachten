<?php

namespace App\Http\Controllers;

use App\Category;
use App\Part;
use App\Shelf;
use App\Unit;
use App\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PartController extends Controller
{
    function __construct()
    {
        $this->middleware( 'permission:view parts', [ 'only' => [ 'index' ] ] );
        $this->middleware( 'permission:create parts', [ 'only' => [ 'create', 'store' ] ] );
        $this->middleware( 'permission:edit parts', [ 'only' => [ 'edit', 'update' ] ] );
        $this->middleware( 'permission:show parts', [ 'only' => [ 'show' ] ] );
        $this->middleware( 'permission:delete parts', [ 'only' => [ 'destroy' ] ] );
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Part[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index( Request $request )
    {
        $sortBy = $request->sort ? $request->sort : 'name';
        $orderBy = $request->order ? $request->order : 'asc';
        $perPage = $request->perPage ? $request->perPage : 20;
        $q = $request->q ? $request->q : null;

        $parts = Part::search( $q )->orderBy( $sortBy, $orderBy )->paginate( $perPage );

        return view( 'parts.index', compact( 'parts', 'sortBy', 'orderBy', 'perPage', 'q' ) );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $shelves = Shelf::all();
        $suppliers = Supplier::all();
        $units = Unit::all();

        return view( 'parts.create', compact( 'categories', 'shelves', 'suppliers', 'units' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \App\Part $part
     *
     * @return \Illuminate\Http\Response
     */
    public function show( Part $part )
    {
        $usage = DB::select( 'SELECT SUM(`amount`) as `amount`, MONTH(`created_at`) as `month`, YEAR(`created_at`) as `year`
                            FROM `assignment_part`
                            WHERE `part_id` = ?
                            GROUP BY `year`, `month`', [ $part->id ] );

        return view( 'parts.show', compact( 'part', 'usage' ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $request->merge( [
            'price', str_replace( ',', '.', $request->price ),
            'minimal_stock', str_replace( ',', '.', $request->minimal_stock ),
            'user_created' => auth()->user()->id,
        ] );

        $this->validation( $request );

        $stored = Part::create( $request->all() );

        $this->syncShelves( $stored, $request );

        $stored->categories()->sync( $request->categories );

        return redirect()->route( 'parts.index' )
                         ->with( [
                             'message' => __( $stored ? 'model.stored' : 'model.not_stored', [ 'model' => trans_choice( 'model.Part', 1 ), 'name' => $request->name ] ),
                             'success' => $stored,
                         ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Part $part
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( Part $part )
    {
        $categories = Category::all();
        $shelves = Shelf::all();
        $suppliers = Supplier::all();
        $units = Unit::all();

        return view( 'parts.edit', compact( 'part', 'categories', 'shelves', 'suppliers', 'units' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Part                $part
     *
     * @return void
     */
    public function update( Request $request, Part $part )
    {
        $request->merge( [
            'price', str_replace( ',', '.', $request->price ),
            'minimal_stock', str_replace( ',', '.', $request->minimal_stock ),
            'user_updated' => auth()->user()->id,
        ] );

        $this->validation( $request, $part->id );

        $updated = $part->update( $request->all() );

        $this->syncShelves( $part, $request );
        $this->syncSuppliers( $part, $request );
        $part->categories()->sync( $request->categories );

        return redirect()->route( 'parts.index' )
                         ->with( [
                             'message' => __( $updated ? 'model.updated' : 'model.not_updated', [ 'model' => trans_choice( 'model.Part', 1 ), 'name' => $request->name ] ),
                             'success' => $updated,
                         ] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Part $part
     *
     * @return void
     * @throws \Exception
     */
    public function destroy( Part $part )
    {
        $deleted = $part->delete();

        return redirect()->back()
                         ->with( [
                             'message' => __( $deleted ? 'model.deleted' : 'model.not_deleted', [ 'model' => trans_choice( 'model.Part', 1 ), 'name' => $part->name ] ),
                             'success' => $deleted,
                         ] );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Int|null                 $id
     *
     * @return array|void
     */
    private function validation( Request $request, Int $id = null )
    {
        $request->validate( [
            'name'          => 'required|string|max:255',
            'price'         => 'required|max:10|min:4|regex:/^\d+(\.\d{2})?$/',
            'unit'          => 'required|exists:units,id',
            'minimal_stock' => 'required|min:0|max:10|regex:/^\d+(\.\d{1,2})?$/',
            'categories'    => 'array|nullable',
            'categories.*'  => 'nullable|exists:categories,id',
            'shelves'       => 'array|nullable|required_with:stock',
            'shelves.*'     => 'exists:shelves,id',
            'stock'         => 'array|nullable|required_with:shelves',
            'stock.*'       => 'regex:/^\d+(\.\d{1,2})?$/',
            'suppliers'     => 'array|required_with:numbers,numbers,nett_prices,discounts',
            'suppliers.*'   => 'exists:suppliers,id',
            'numbers'       => 'array|nullable|required_with:suppliers',
            'numbers.*'     => 'string',
            'nett_prices'   => 'array|required_with:suppliers,numbers,discounts',
            'nett_prices.*' => 'max:10|min:4|regex:/^\d+(\.\d{2})?$/',
            'discounts'     => 'array|required_with:suppliers,numbers,nett_prices',
            'discounts.*'   => 'integer|min:0|max:100',
            'preferred'     => 'nullable|exists:suppliers,id',
        ] );
    }

    /**
     * @param \App\Part                $part
     * @param \Illuminate\Http\Request $request
     */
    private function syncSuppliers( Part $part, Request $request )
    {
        if( !$request->suppliers )
        {
            return;
        }

        $suppliers = [];
        for( $i = 0; $i < count( $request->suppliers ); $i++ )
        {
            $suppliers[ $request->suppliers[ $i ] ] = [
                'number'     => $request->numbers[ $i ],
                'nett_price' => str_replace( ',', '.', $request->nett_prices[ $i ] ),
                'discount'   => $request->discounts[ $i ],
                'preferred'  => $request->suppliers[ $i ] == $request->preferred ? 1 : 0,
            ];
        }
        $part->suppliers()->sync( $suppliers );
    }

    /**
     * @param \App\Part                $part
     * @param \Illuminate\Http\Request $request
     */
    private function syncShelves( Part $part, Request $request )
    {
        if( !$request->shelves )
        {
            return;
        }

        $shelves = [];
        for( $i = 0; $i < count( $request->shelves ); $i++ )
        {
            $shelves[ $request->shelves[ $i ] ] = [ 'stock' => str_replace( ',', '.', $request->stock[ $i ] ) ];
        }
        $part->shelves()->sync( $shelves );
    }
}
