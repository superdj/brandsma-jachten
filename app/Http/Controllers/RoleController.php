<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    function __construct()
    {
        $this->middleware( 'permission:view roles', [ 'only' => [ 'index' ] ] );
        $this->middleware( 'permission:create roles', [ 'only' => [ 'create', 'store' ] ] );
        $this->middleware( 'permission:edit roles', [ 'only' => [ 'edit', 'update' ] ] );
        $this->middleware( 'permission:show roles', [ 'only' => [ 'show' ] ] );
        $this->middleware( 'permission:delete roles', [ 'only' => [ 'destroy' ] ] );
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        /*$sortBy = $request->sort ? $request->sort : 'id';
        $orderBy = $request->order ? $request->order : 'desc';
        $perPage = $request->perPage ? $request->perPage : 20;
        $q = $request->q ? $request->q : null;

        $roles = Role::search( $q )->orderBy( $sortBy, $orderBy )->paginate( $perPage );
        return view( 'roles.index', compact( 'roles', 'sortBy', 'orderBy', 'perPage', 'q' ) );*/
        $roles = Role::all();

        return view( 'roles.index', compact( 'roles' ) );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::all();

        return view( 'roles.create', compact( 'permissions' ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $this->validation( $request );

        $role = Role::create( $request->all() );
        $role->syncPermissions( $request->permissions );

        return redirect()->route( 'roles.index' )
                         ->with(
                             [
                                 'message' => __( $role ? 'model.stored' : 'model.not_stored', [ 'model' => trans_choice( 'model.Role', 1 ), 'name' => $request->name ] ),
                                 'success' => $role,
                             ] );
    }

    /**
     * Display the specified resource.
     *
     * @param \Spatie\Permission\Models\Role $role
     *
     * @return \Illuminate\Http\Response
     */
    public function show( Role $role )
    {
        return view( 'roles.show', compact( 'role' ) );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \Spatie\Permission\Models\Role $role
     *
     * @return void
     */
    public function edit( Role $role )
    {
        $permissions = Permission::all();

        return view( 'roles.edit', compact( 'role', 'permissions' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request       $request
     * @param \Spatie\Permission\Models\Role $role
     *
     * @return void
     */
    public function update( Request $request, Role $role )
    {
        $this->validation( $request, $role->id );

        $updated = $role->update( $request->all() );

        $role->syncPermissions( $request->permissions );

        return redirect()->route( 'roles.index' )
                         ->with( [
                             'message' => __( $updated ? 'model.updated' : 'model.not_updated', [ 'model' => trans_choice( 'model.Role', 1 ), 'name' => $request->name ] ),
                             'success' => $updated,
                         ] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \Spatie\Permission\Models\Role $role
     *
     * @return void
     * @throws \Exception
     */
    public function destroy( Role $role )
    {
        $deleted = $role->delete();

        return redirect()->route( 'roles.index' )
                         ->with( [
                             'message' => __( $deleted ? 'model.deleted' : 'model.not_deleted', [ 'model' => trans_choice( 'model.Rack', 1 ), 'name' => $role->name ] ),
                             'success' => $deleted,
                         ] );
    }

    private function validation( Request $request, Int $id = null )
    {
        $request->validate( [
            'name'          => 'required|'.$id ? 'unique:roles,name,'.$id : 'unique:roles',
            'permissions'   => 'required|array',
            'permissions.*' => 'exists:permissions,id',
        ] );
    }
}
