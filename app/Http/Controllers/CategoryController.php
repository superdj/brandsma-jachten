<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    function __construct()
    {
        $this->middleware( 'permission:view categories', [ 'only' => [ 'index' ] ] );
        $this->middleware( 'permission:create categories', [ 'only' => [ 'create', 'store' ] ] );
        $this->middleware( 'permission:edit categories', [ 'only' => [ 'edit', 'update' ] ] );
        $this->middleware( 'permission:show categories', [ 'only' => [ 'show' ] ] );
        $this->middleware( 'permission:delete categories', [ 'only' => [ 'destroy' ] ] );
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Category[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index( Request $request )
    {
        $sortBy = $request->sort ? $request->sort : 'name';
        $orderBy = $request->order ? $request->order : 'asc';
        $perPage = $request->perPage ? $request->perPage : 20;
        $q = $request->q ? $request->q : null;

        $categories = Category::search( $q )->orderBy( $sortBy, $orderBy )->paginate( $perPage );

        return view( 'categories.index', compact( 'categories', 'sortBy', 'orderBy', 'perPage', 'q' ) );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'categories.create' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \App\Category $category
     *
     * @return \Illuminate\Http\Response
     */
    public function show( Category $category )
    {
        return view( 'categories.show', compact( 'category' ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $request->merge( [ 'user_created' => auth()->user()->id ] );
        $this->validation( $request );

        $stored = Category::create( $request->all() );

        return redirect()->route( 'categories.index' )
                         ->with( [
                             'message', __( $stored ? 'model.stored' : 'model.not_stored', [ 'model' => trans_choice( 'model.Category', 1 ), 'name' => $request->name ] ),
                             'success' => $stored,
                         ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Category $category
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( Category $category )
    {
        return view( 'categories.edit', compact( 'category' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Category            $category
     *
     * @return void
     */
    public function update( Request $request, Category $category )
    {
        $request->merge( [ 'user_updated' => auth()->user()->id ] );
        $this->validation( $request, $category->id );

        $updated = $category->update( $request->all() );

        return redirect()->route( 'categories.index' )
                         ->with( [
                             'message', __( $updated ? 'model.updated' : 'model.not_updated', [ 'model' => trans_choice( 'model.Category', 1 ), 'name' => $request->name ] ),
                             'success' => $updated,
                         ] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Category $category
     *
     * @return void
     * @throws \Exception
     */
    public function destroy( Category $category )
    {
        $deleted = $category->delete();

        return redirect()->route( 'categories.index' )
                         ->with( [
                             'message', __( $deleted ? 'model.deleted' : 'model.not_deleted', [ 'model' => trans_choice( 'model.Category', 1 ), 'name' => $category->name ] ),
                             'success' => $deleted,
                         ] );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Int|null                 $id
     *
     * @return array|void
     */
    private function validation( Request $request, Int $id = null )
    {
        $request->validate( [
            'name' => 'required|string|max:255|'.$id ? 'unique:categories,name,'.$id : 'unique:categories',
        ] );
    }
}
