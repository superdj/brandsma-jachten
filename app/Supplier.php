<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable = [ 'name', 'number', 'city', 'postal_code', 'street', 'house_number', 'email', 'phone_number', 'mobile_number', 'website', 'user_created', 'user_updated' ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo( User::class, 'user_created' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function parts()
    {
        return $this->belongsToMany( Part::class )
                    ->withPivot( [ 'nett_price', 'number', 'discount', 'preferred' ] );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updater()
    {
        return $this->belongsTo( User::class, 'user_updated' );
    }

    /**
     * @param $query
     * @param $q
     *
     * @return mixed
     */
    public function scopeSearch( $query, $q )
    {
        if( $q === null )
        {
            return $query;
        }

        return $query->where( 'id', 'LIKE', "%{$q}%" )
                     ->orWhere( 'name', 'LIKE', "%{$q}%" )
                     ->orWhere( 'city', 'LIKE', "%{$q}%" )
                     ->orWhere( 'postal_code', 'LIKE', "%{$q}%" )
                     ->orWhere( 'street', 'LIKE', "%{$q}%" )
                     ->orWhere( 'house_number', 'LIKE', "%{$q}%" )
                     ->orWhere( 'email', 'LIKE', "%{$q}%" )
                     ->orWhere( 'phone_number', 'LIKE', "%{$q}%" )
                     ->orWhere( 'mobile_number', 'LIKE', "%{$q}%" )
                     ->orWhere( 'website', 'LIKE', "%{$q}%" );
    }
}
