<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $fillable = [ 'name', 'user_created', 'user_updated' ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo( User::class, 'user_created' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function racks()
    {
        return $this->hasMany( Rack::class );
    }

    public function shelves()
    {
        return $this->hasManyThrough( Shelf::class, Rack::class );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updater()
    {
        return $this->belongsTo( User::class, 'user_updated' );
    }

    /**
     * @param $query
     * @param $q
     *
     * @return mixed
     */
    public function scopeSearch( $query, $q )
    {
        if( $q === null )
        {
            return $query;
        }

        return $query->where( 'name', 'LIKE', "%{$q}%" );
    }
}
