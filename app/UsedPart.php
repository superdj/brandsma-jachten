<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsedPart extends Model
{
    protected $table = 'assignment_part';

    protected $guarded = ['id'];

    protected $casts = [
        'parts' => 'array',
        'amounts' => 'array'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function assignment()
    {
        return $this->belongsTo( Assignment::class );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function part()
    {
        return $this->belongsTo( Part::class );
    }

    public function scopeSearch( $query, $q )
    {
        if( $q === null ) {
            return $query;
        }

        return $query->where('name', 'LIKE', "%{$q}%")
            ->orWhere('abbreviation', 'LIKE', "%{$q}%");
    }
}
