<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $casts = [
        'parts'   => 'array',
        'amounts' => 'array',
    ];
    protected $fillable = [ 'name', 'number', 'status_id', 'user_created', 'user_updated' ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo( User::class, 'user_created' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function parts()
    {
        return $this->belongsToMany( Part::class )->withPivot( 'amount' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo( Status::class );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updater()
    {
        return $this->belongsTo( User::class, 'user_updated' );
    }

    /**
     * @param $query
     * @param $q
     *
     * @return mixed
     */
    public function scopeSearch( $query, $q )
    {
        if( $q === null )
        {
            return $query;
        }

        return $query->where( 'id', 'LIKE', "%{$q}%" )
                     ->orWhere( 'name', 'LIKE', "%{$q}%" );
    }
}
