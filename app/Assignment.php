<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    protected $casts = [
        'users' => 'array',
        'tasks' => 'array',
    ];
    protected $dates = [ 'start_date', 'end_date' ];
    protected $fillable = [ 'name', 'description', 'start_date', 'end_date', 'customer_id', 'status_id', 'user_created', 'user_updated' ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo( User::class, 'user_created' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo( Customer::class );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function parts()
    {
        return $this->belongsToMany( Part::class )
                    ->withPivot( 'amount' )
                    ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo( Status::class );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tasks()
    {
        return $this->belongsToMany( Task::class );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function time_registrations()
    {
        return $this->hasMany( TimeRegistration::class );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updater()
    {
        return $this->belongsTo( User::class, 'user_updated' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany( User::class );
    }

    /**
     * @param $query
     * @param $q
     *
     * @return mixed
     */
    public function scopeSearch( $query, $q )
    {
        if( $q === null )
        {
            return $query;
        }

        return $query->where( 'id', 'LIKE', "%{$q}%" )
                     ->orWhere( 'name', 'LIKE', "%{$q}%" );
    }
}
