<?php
/**
 * All sorts of helper functions
 */

/**
 * Calculate the total sum of time provided in an array
 * @param array $times
 *
 * @return string
 */
function sum_time( Array $times )
{
    $sum = strtotime( '00:00:00' );
    $sum2 = 0;

    foreach ($times as $time )
    {

        $sum1 = strtotime( $time ) - $sum;

        $sum2 = $sum2 + $sum1;
    }

    return $sum + $sum2;
}
