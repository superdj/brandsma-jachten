<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->name('home');

Auth::routes();

Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function() {
    Route::get('/', 'DashboardController@index')->name('dashboard.index');
    Route::get('manual', 'DashboardController@manual')->name('dashboard.manual');

    Route::resource('users', 'UserController');

    Route::resource('roles', 'RoleController');

    Route::resource('parts', 'PartController');

    Route::resource('customers', 'CustomerController');

    Route::resource('warehouses', 'WarehouseController');

    Route::resource('racks', 'RackController');

    Route::resource('shelves', 'ShelfController');

    Route::resource('suppliers', 'SupplierController');

    Route::resource('statuses', 'StatusController');

    Route::resource('orders', 'OrderController');

    Route::get('cart', 'CartController@index')->name('cart.index');
    Route::get('cart/{part}/edit', 'CartController@edit')->name('cart.edit');
    Route::post('cart/{part}/add', 'CartController@add')->name('cart.add');
    Route::put('cart/{part}', 'CartController@update')->name('cart.update');
    Route::delete('cart/{part}', 'CartController@destroy')->name('cart.destroy');

    Route::resource('assignments', 'AssignmentController');
    Route::get('assignments/{assignment}/export', 'AssignmentController@export')->name('assignment.export');

    Route::resource('tasks', 'TaskController');

    Route::resource('categories', 'CategoryController');

    Route::get('time-registrations', 'TimeRegistrationController@index')->name('time-registrations.index');
    Route::post('time-registrations', 'TimeRegistrationController@store')->name('time-registrations.store');
    Route::get('time-registrations/{assignment}/create/', 'TimeRegistrationController@create')->name('time-registrations.create');
    Route::get('time-registrations/{time_registration}', 'TimeRegistrationController@show')->name('time-registrations.show');
    Route::put('time-registrations/{time_registration}', 'TimeRegistrationController@update')->name('time-registrations.update');
    Route::delete('time-registrations/{time_registration}', 'TimeRegistrationController@destroy')->name('time-registrations.destroy');
    Route::get('time-registrations/{time_registration}/edit', 'TimeRegistrationController@edit')->name('time-registrations.edit');

    Route::get('used-parts', 'UsedPartController@index')->name('used-parts.index');
    Route::post('used-parts/{assignment}', 'UsedPartController@store')->name('used-parts.store');
    Route::get('used-parts/{assignment}/create/', 'UsedPartController@create')->name('used-parts.create');
    Route::put('used-parts/{used_part}', 'UsedPartController@update')->name('used-parts.update');
    Route::delete('used-parts/{used_part}', 'UsedPartController@destroy')->name('used-parts.destroy');
    Route::get('used-parts/{used_part}/edit', 'UsedPartController@edit')->name('used-parts.edit');

    Route::resource('units', 'UnitController');
});
