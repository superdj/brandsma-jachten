<?php

return [
    'not_found' => 'No :model found',
    'add' => 'Add :model',
    'edit' => 'Edit :model',
    'delete' => 'Delete :model',
    'info' => ':model: :name information',
    'edit_title' => 'Edit :model: :name',

    'stored' => ':model: :name created',
    'not_stored' => ':model: :name, not created',
    'updated' => ':model: :name updated',
    'not_updated' => ':model: :name, not updated',
    'deleted' => ':model: :name deleted',
    'not_deleted' => ':model: :name, not deleted',

    'back' => 'Back to :page',
    'Updated by' => 'Updated by',
    'Created by' => 'Created by',
    'Edit' => 'Edit',
    'Delete' => 'Delete',
    'reset' => 'Reset form',
    'Actions' => 'Actions',
    'Name' => 'Name',
    'Info' => 'Info',
    'Email' => 'Email',

    'Customer' => 'Customer|Customers',
    'customer' => 'customer|customers',
    'Boat' => 'Boat',

    'Part' => 'Part|Parts',
    'part' => 'part|parts',
    'Parts number' => 'Parts number',
    'Stock' => 'Stock',
    'Minimal stock' => 'Minimal stock',
    'Location' => 'Location|Locations',
    'Price' => 'Price',
    'Worth' => 'Worth',
    'Total' => 'Total',

    'Rack' => 'Rack|Racks',
    'rack' => 'rack|racks',

    'Shelf' => 'Shelf|Shelves',
    'shelf' => 'shelf|shelves',

    'Supplier' => 'Supplier|Suppliers',
    'supplier' => 'supplier|suppliers',
    'Address' => 'Address',
    'Phone number' => 'Phone number',
    'Mobile number' => 'Mobile number',
    'City' => 'City',
    'Street' => 'Street',
    'House number' => 'House number',
    'Postal code' => 'Postal code',
    'Website' => 'Website',

    'User' => 'User|Users',
    'user' => 'user|users',
    'First name' => 'First name',
    'Last name' => 'Last name',

    'Warehouse' => 'Warehouse|Warehouses',
    'warehouse' => 'warehouse|warehouses',

    'Password' => 'Password',
    'Confirm password' => 'Confirm password',
    'Remember me' => 'Remember me',
    'Login' => 'Login',
    'Forgot password' => 'Forgot password'
];
