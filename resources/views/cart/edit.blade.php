@extends('layouts.app')
@section('title', __('model.Edit cart', ['part' => $part->name]))

@section('content')
    <form method="post" action="{{ route('cart.update', $part->id) }}">
        @csrf
        @method('put')

        <div class="row">
            <div class="xs12">
                <a href="{{ route('cart.index') }}" class="button button--contained button--contained--icon-left">
                    <i class="material-icons icon">arrow_back</i>
                    {{ __('model.back', ['page' => __('model.Cart')]) }}
                </a>
            </div>

            <div class="xs12 sm6 md3">
                <div class="row padding--0">
                    <div class="xs12">
                        <div class="text-field text-field--filled {{ $errors->has('amount') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <input id="amount" type="number" min="0.00" step="0.01" class="text-field__input" name="amount" value="{{ old('amount') ?? $item['amount'] }}" required autofocus>
                            <label for="amount" class="text-field__label">{{ __('model.Amount') }}*</label>

                            @if ($errors->has('amount'))
                                <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('amount') }}
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="xs12">
                <button type="submit" class="button button--contained button--contained--icon-left">
                    <i class="material-icons icon">save</i>
                    {{ __('model.edit', ['model' => __('model.Cart')]) }}
                </button>

                <button type="reset" class="button button--text button--text--icon-left">
                    <i class="material-icons icon">settings_backup_restore</i>
                    {{ __('model.reset') }}
                </button>
            </div>
        </div>
    </form>
@endsection
