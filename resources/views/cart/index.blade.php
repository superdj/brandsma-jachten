@extends('layouts.app')
@section('title', __('model.Cart'))

@section('content')
<div class="row">
    <div class="xs12">
        <a href="{{ route('orders.create') }}" class="button button--contained button--contained--icon-left">
            <i class="material-icons icon">add</i>
            {{ __('model.To order') }}
        </a>
    </div>

    <div class="xs12">
        @if($items)
            <table class="data-table data-table--hover">
                <thead>
                <tr>
                    <th>{{ trans_choice('model.Part', 1) }}</th>
                    <th>{{ __('model.Amount') }}</th>
                    <th>{{ __('model.Actions') }}</th>
                </tr>
                </thead>

                <tbody>
                    @foreach($items as $item)
                        <tr>
                            <td>
                                <a href="{{ route('parts.show', $item['item']->id) }}">
                                    {{ $item['item']->name }}
                                </a>
                            </td>
                            <td>
                                {{ $item['amount'] }}
                                {{ $item['item']->unit->abbreviation }}
                            </td>
                            <td>
                                <a href="{{ route('cart.edit', $item['item']->id) }}" class="button button--outlined button--outlined--icon-left">
                                    <i class="material-icons icon">edit</i>
                                    {{ __('model.Edit') }}
                                </a>

                                <form method="post" action="{{ route('cart.destroy', $item['item']->id) }}">
                                    @csrf
                                    @method('delete')

                                    <button
                                        type="button"
                                        class="button button--outlined button--outlined--icon-left"
                                        data-trigger="dialog-{{ $item['item']->id }}"
                                    >
                                        <i class="material-icons icon">remove_shopping_cart</i>
                                        {{ __('model.Delete') }}
                                    </button>

                                    <div class="dialog dialog--confirm" id="dialog-{{ $item['item']->id }}">
                                        <div class="dialog__header">
                                            {{ __('model.delete', ['model' => trans_choice('model.Part', 1)]) }}
                                        </div>

                                        <div class="dialog__content">
                                            {{ __('model.delete_question', [
                                                'model' => trans_choice('model.part', 1),
                                                'name' => $item['item']->name
                                            ]) }}?
                                        </div>

                                        <div class="dialog__actions">
                                            <button
                                                type="button"
                                                class="button button--text"
                                                data-trigger="dialog-{{ $item['item']->id }}"
                                            >
                                                {{ __('model.Cancel') }}
                                            </button>

                                            <button type="submit" class="button button--text">{{ __('model.Delete') }}</button>
                                        </div>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            @include('partials.not-found-banner', ['model' => trans_choice('model.part', 2)])
        @endif
    </div>
</div>
@endsection
