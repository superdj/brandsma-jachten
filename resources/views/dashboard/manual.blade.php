@extends('layouts.app')
@section('title', __('model.Manual'))

@section('content')
    <div class="row">
        <div class="xs12 md5 lg3">
            <h5>Inhoudsopgave</h5>

            <ul>
                <li><a href="#menu">Menu</a></li>
                <li>
                    <a href="#parts">Onderdelen</a>

                    <ul>
                        <li><a href="#parts-add">Onderdelen toevoegen/ bewerken</a></li>
                        <li><a href="#parts-delete">Onderdelen verwijderen</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#orders">Bestellingen</a>

                    <ul>
                        <li><a href="#orders-add">Bestellingen toevoegen/ bewerken</a></li>
                        <li><a href="#orders-delete">Bestellingen verwijderen</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#assignments">Opdrachten</a>

                    <ul>
                        <li><a href="#assignments-add">Opdrachten toevoegen/ bewerken</a></li>
                        <li><a href="#assignments-delete">Opdrachten verwijderen</a></li>
                        <li><a href="#assignments-used">Gebruikte onderdelen</a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="xs12 md7 lg6">
            <article class="row padding--0" id="menu">
                <h2 class="xs12">Menu</h2>

                <div class="xs12">
                    <p>
                        Het menu kan bereikt worden door op het hamburger menu icoon te klikken.
                        Vanuit dit menu kan er naar verscheidene overzichtspagina's genavigeerd worden.
                    </p>

                    <img src="{{ asset('storage/menu-icon.jpg') }}" alt="Menu icon">
                </div>
            </article>

            <article class="row padding--0" id="parts">
                <h2 class="xs12">Onderdelen</h2>

                <div class="xs12">
                    <p>
                        De onderdelen worden gebruikt voor zowel de <a href="#assignments">opdrachten</a> als de <a href="#orders">bestellingen</a>.
                        Voor het bekijken van alle onderdelen moet er eerst via het menu naar de <a href="{{ route('parts.index') }}">overzichtspagina</a> genavigeerd worden.
                    </p>
                </div>

                <section class="xs12 row padding--0" id="parts-add">
                    <h3 class="xs12">Onderdelen toevoegen/ bewerken</h3>

                    <div class="xs12">
                        <p>
                            Vanuit de <a href="{{ route('parts.index') }}">overzichtspagina</a> kan een nieuwe onderdeel toegevoegd worden.
                        </p>

                        <img src="{{ asset('storage/add-part.jpg') }}" alt="Add part">

                        <p>
                            Of je kan een onderdeel bewerken.
                        </p>

                        <img src="{{ asset('storage/edit-part.jpg') }}" alt="Edit part">

                        <p>
                            Voor het toevoegen van een onderdeel locatie en daarmee voorraad,
                            moet er eerst een magazijn met stellingen en planken aangemaakt zijn.
                            Voordat een leverancier kan worden toegevoegd moet de leverancier ook eerst aangemaakt worden.
                        </p>

                        <p>
                            Bij het aangeven van de locatie bestaat de waarde uit "Magazijn - Stelling - Plank".
                        </p>

                        <p>
                            Bij een leverancier wordt om de voorkeur gevraagd.
                            Dit geeft aan van welke leverancier het onderdeelnummer verder in het systeem gebruikt wordt.
                        </p>

                        <p>
                            Met de minimale voorraad wordt aangegeven hoeveel onderdelen er altijd moeten zijn.
                            Deze waarde wordt ook gebruikt voor het versturen van emails wanneer de voorraad onder deze waarde is.
                        </p>

                        <p>
                            Nadat alle velden zijn ingevuld moet het onderdeel nog opgeslagen worden.
                            Wanneer het onderdeel niet wordt opgeslagen en er wordt wel genavigeerd dan zal het onderdeel niet opgeslagen worden.
                        </p>
                    </div>
                </section>

                <section class="xs12 row padding--0" id="parts-delete">
                    <h3 class="xs12">Onderdeel verwijderen</h3>

                    <div class="xs12">
                        <p>
                            Het verwijderen van een onderdeel kan ook vanaf de overzichtspagina.
                            Voordat het onderdeel wordt verwijderd wordt er eerst om bevestiging gevraagd.
                            Wanneer er een onderdeel wordt verwijderd dan zijn er een aantal dingen die gebeuren.
                            Alle koppelingen tussen onderdelen en: bestellingen, opdrachten en leveranciers word verbroken.
                            Dit wil zeggen dat het verwijderen van een onderdeel ongewenste gevolgen kan hebben.
                        </p>
                    </div>
                </section>
            </article>

            <article class="row padding--0" id="orders">
                <h2 class="xs12">Bestellingen</h2>

                <section class="xs12">
                    <p>
                        Voor het bekijken van alle bestellingen moet er eerst naar de <a href="{{ route('orders.index') }}">overzichtspagina</a> genavigeerd worden.
                    </p>
                </section>

                <section class="xs12 row padding--0" id="orders-add">
                    <h3 class="xs12">Bestelling toevoegen/ bewerken</h3>

                    <div class="xs12">
                        <p>
                            Voordat een bestelling gemaakt kan worden moeten er eerst onderdelen aanwezig zijn.
                            Alleen de onderdelen die in het dasboard staan kunnen gebruikt worden voor het maken van een bestelling.
                        </p>

                        <p>
                            Het maken van een bestelling kan op 2 manieren.
                            De eerste is via de <a href="{{ route('orders.index') }}">overzichtspagina</a> van alle bestellingen.
                            De 2e manier is via een onderdeel zelf. Hierdoor is het makkelijker te vinden welke onderdelen er bestelt moeten worden.
                            In de informatiepagina van een onderdeel is het mogelijk om direct het onderdeel toe te voegen aan een bestelling.
                        </p>

                        <img src="{{ asset('storage/order-part.jpg') }}" alt="Add part to order"/>

                        <p>
                            Vanuit de bestelling toevoegen pagina kan er gezocht worden naar onderdelen op basis van onderdeelnaam en onderdeelnummer.
                            Ook hier wordt alleen het onderdeelnummer gebruikt van de leverancier die de voorkeur heeft.
                        </p>

                        <p>
                            Het bewerken gaat net als het toevoegen van de bestelling.
                            Vanuit de overzichtspagina kan dus ook een bestelling bewerkt worden.
                            Een bestelling kan alleen bewerkt worden zolang de status dit toestaat.
                        </p>
                    </div>
                </section>

                <section class="xs12 row padding--0" id="order-delete">
                    <h3 class="xs12">Bestelling verwijderen</h3>

                    <div class="xs12">
                        <p>
                            Het verwijderen van een bestelling gaat ook via de overzichtspagina.
                            Voordat de bestelling daadwerkelijk wordt verwijderd wordt er eerst een bevestiging gevraagd.
                        </p>
                    </div>
                </section>
            </article>

            <article class="row padding--0" id="assignments">
                <h2 class="xs12">Opdrachten</h2>

                <section class="xs12">
                    <p>
                        Voor het bekijken van alle opdrachten moet er eerst via het menu naar de overzichtspagina genavigeer worden.
                        Vanuit hier kunnen opdrachten worden toegevoegd, bewerkt en verwijderd.
                        Daarnaast is het mogelijk om de urenregistratie en gebruikte onderdelen per opdracht aan te geven.
                    </p>
                </section>

                <section class="xs12 row padding--0" id="assignments-add">
                    <h3 class="xs12">Opdrachten toevoegen/ bewerken</h3>

                    <div class="xs12">
                        <p>
                            Zoals vermeld kan het toevoegen en bewerken van een opdracht vanuit de overzichtspagina.
                            Het bewerken van een opdracht kan alleen zolang de status dit toestaat.
                        </p>

                        <p>
                            De start- en einddatum worden gebruikt om de tabelcellen van de overzichtspagina te kleuren.
                            Dit zelfde geldt voor de aangegeven taken.
                        </p>
                    </div>
                </section>

                <section class="xs12 row padding--0" id="assignments-delete">
                    <h3 class="xs12">Opdrachten verwijderen</h3>

                    <div class="xs12">
                        <p>
                            Het verwijderen van een opdracht gaat ook via de overzichtspagina.
                            Voordat de opdracht daadwerkelijk wordt verwijderd wordt er eerst een bevestiging gevraagd.
                        </p>
                    </div>
                </section>

                <section class="xs12 row padding--0" id="assignments-used">
                    <h3 class="xs12">Gebruikte onderdelen</h3>

                    <div class="xs12">
                        <p>
                            De gebruikte onderdelen kunnen per opdracht worden toegevoegd.
                            Dit kan ook weer vanuit de overzichtspagina.
                            Dit kan echter alleen wanneer de status dit toestaat.
                        </p>

                        <p>
                            Bij het toevoegen van gebruikte onderdelen kan er worden gezocht op onderdeelnaam en onderdeelnummer.
                            De waarde van het onderdeel bestaat uit "Magazijn - Stelling - Plank - Onderdeel - Onderdeelnummer"
                            Door het onderdeel van de goede locatie te selecteren blijft de voorraad altijd up to date.
                        </p>

                        <p>
                            Vanuit het overzicht van alle gebruikte onderdelen kan het aantal dat gebruikt is worden aangepast.
                            Dit kan alleen zolang de opdracht nog niet is afgerond.
                        </p>
                    </div>
                </section>
            </article>
        </div>
    </div>
@endsection
