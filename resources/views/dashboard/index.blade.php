@extends('layouts.app')
@section('title', 'Dashboard')

@section('content')
    <div class="row">
        <div class="xs12 sm6 md4">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        @can('view parts')
                            <a href="{{ route('parts.index') }}">
                                <i class="material-icons icon">build</i>
                                {{ trans_choice('model.Part', 2) }}
                            </a>
                        @endcan

                        @cannot('view parts')
                            <i class="material-icons icon">build</i>
                            {{ trans_choice('model.Part', 2) }}
                        @endcannot
                    </div>
                </div>

                <div class="card__text">
                    @if($parts->count() > 0)
                        <table class="data-table data-table--hover">
                            <thead>
                            <tr>
                                <th>{{ trans_choice('model.Part', 1) }}</th>
                                <th>{{ __('model.Stock') }}</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($parts as $part)
                                <tr>
                                    <td>
                                        @can('show parts')
                                            <a href="{{ route('parts.show', $part->id) }}">
                                                {{ $part->part }}
                                            </a>
                                        @endcan

                                        @cannot('show parts')
                                            {{ $part->part }}
                                        @endcannot
                                    </td>
                                    <td class="{{ $part->stock < $part->minimal_stock ? 'text--red-900 red-50' : 'text--green-900 green-50' }}">
                                        {{ $part->stock }}
                                        {{ $part->abbreviation }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        @include('partials.not-found-banner', ['model' => trans_choice('model.part', 2)])
                    @endif
                </div>

                @can('create parts')
                    <div class="card__actions">
                        <a href="{{ route('parts.create') }}" class="button button--text">
                            {{ __('model.add', ['model' => trans_choice('model.Part', 1)]) }}
                        </a>
                    </div>
                @endcan
            </div>
        </div>

        <div class="xs12 sm6 lg4">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        @can('view orders')
                            <a href="{{ route('orders.index') }}">
                                <i class="material-icons icon">shopping_cart</i>
                                {{ trans_choice('model.Order', 2) }}
                            </a>
                        @endcan

                        @cannot('view orders')
                            <i class="material-icons icon">assignment</i>
                            {{ trans_choice('model.Order', 2) }}
                        @endcannot
                    </div>
                </div>

                <div class="card__text">
                    @if($orders->count() > 0)
                        <table class="data-table data-table--hover">
                            <thead>
                            <tr>
                                <th>{{ trans_choice('model.Order', 1) }}</th>
                                <th class="data-table--numeric">{{ trans_choice('model.Part', 2) }}</th>
                                <th>{{ trans_choice('model.Status', 1) }}</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <td>
                                        @can('show orders')
                                            <a href="{{ route('orders.show', $order->id) }}">
                                                {{ $order->name }}
                                            </a>
                                        @endcan

                                        @cannot('show orders')
                                            {{ $order->name }}
                                        @endcannot
                                    </td>
                                    <td class="data-table--numeric">{{ $order->parts->count() }}</td>
                                    <td>{{ $order->status->name }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        @include('partials.not-found-banner', ['model' => trans_choice('model.order', 2)])
                    @endif
                </div>

                @can('create orders')
                    <div class="card__actions">
                        <a href="{{ route('orders.create') }}" class="button button--text">
                            {{ __('model.add', ['model' => trans_choice('model.Order', 1)]) }}
                        </a>
                    </div>
                @endcan
            </div>
        </div>

        <div class="xs12 sm6 lg4">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        @can('view assignments')
                            <a href="{{ route('assignments.index') }}">
                                <i class="material-icons icon">assignment</i>
                                {{ trans_choice('model.Assignment', 2) }}
                            </a>
                        @endcan

                        @cannot('view assignments')
                            <i class="material-icons icon">assignment</i>
                            {{ trans_choice('model.Assignment', 2) }}
                        @endcannot
                    </div>
                </div>

                <div class="card__text">
                    @if($assignments->count() > 0)
                        <table class="data-table data-table--hover">
                            <thead>
                            <tr>
                                <th>{{ trans_choice('model.Assignment', 1) }}</th>
                                <th>{{ trans_choice('model.Task', 2) }}</th>
                                <th>{{ __('model.Deadline') }}</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($assignments as $assignment)
                                @php
                                    $tasks_done = [];

                                    foreach($assignment->time_registrations as $registration)
                                    {
                                        foreach($registration->tasks as $task)
                                        {
                                            $tasks_done[] = $task->id;
                                        }
                                    }
                                @endphp

                                <tr>
                                    <td>
                                        @can('show assignments')
                                            <a href="{{ route('assignments.show', $assignment->id) }}">
                                                {{ $assignment->name }}
                                            </a>
                                        @endcan

                                        @cannot('show assignments')
                                            {{ $assignment->name }}
                                        @endcannot
                                    </td>
                                    <td>{{ count($tasks_done) }}/ {{ $assignment->tasks->count() }}</td>
                                    <td class="{{ $assignment->end_date->isPast() ? 'text--red-900 red-50' : 'text--green-900 green-50'}}">
                                        {{ Carbon\Carbon::now()->diffForHumans( $assignment->end_date->format('d-m-Y H:i:s') ) }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        @include('partials.not-found-banner', ['model' => trans_choice('model.assignment', 2)])
                    @endif
                </div>

                @can('create assignments')
                    <div class="card__actions">
                        <a href="{{ route('assignments.create') }}" class="button button--text">
                            {{ __('model.add', ['model' => trans_choice('model.Assignment', 1)]) }}
                        </a>
                    </div>
                @endcan
            </div>
        </div>
    </div>
@endsection
