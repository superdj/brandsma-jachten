@extends('layouts.app')
@section('title', __('model.info', ['name' => $customer->name, 'model' => trans_choice('model.Customer', 1)]))

@section('content')
    <div class="row">
        <div class="xs12">
            <a href="{{ route('customers.index') }}" class="button button--contained button--contained--icon-left">
                <i class="material-icons icon">arrow_back</i>
                {{ __('model.back', ['page' => trans_choice('model.user', 2)]) }}
            </a>
        </div>

        <div class="xs12 sm4 md3">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        <i class="material-icons icon">info</i>
                        {{ __('model.General') }}
                    </div>
                </div>

                <div class="card__text">
                    <ul class="list">
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.name', ['model' => trans_choice('model.Customer', 1)]) }}</span>
                                <span class="list__content__text">{{ $customer->name }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.name', ['model' => __('model.Boat')]) }}</span>
                                <span class="list__content__text">{{ $customer->boat }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Created by', 1) }}</span>
                                <span class="list__content__text">
                                    @can('show users')
                                        <a href="{{ route('users.show', $customer->creator->id) }}">
                                            {{ $customer->creator->first_name }}
                                        </a>
                                    @endcan

                                    @cannot('show users')
                                        {{ $customer->creator->first_name }}
                                    @endcannot
                                </span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Updated by', 1) }}</span>
                                <span class="list__content__text">
                                    @if($customer->updater)
                                        @can('show users')
                                            <a href="{{ route('users.show', $customer->updater->id) }}">
                                            {{ $customer->updater->first_name }}
                                        </a>
                                        @endcan

                                        @cannot('show users')
                                            {{ $customer->updater->first_name }}
                                        @endcannot
                                    @endif
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>

                @can('edit customers')
                    <div class="card__actions">
                        <a href="{{ route('customers.edit', $customer->id) }}" class="button button--text">
                            {{ __('model.edit', ['model' => trans_choice('model.Customer', 1)]) }}
                        </a>
                    </div>
                @endcan
            </div>
        </div>

        <div class="xs12 md7">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        @can('view assignments')
                            <a href="{{ route('assignments.index') }}">
                                <i class="material-icons icon">assignment</i>
                                {{ trans_choice('model.Assignment', 2) }}
                            </a>
                        @endcan

                        @cannot('view assignments')
                            <i class="material-icons icon">assignment</i>
                            {{ trans_choice('model.Assignment', 2) }}
                        @endcannot
                    </div>
                </div>

                <div class="card__text">
                    @if($customer->assignments->count() > 0)
                        <table class="data-table data-table--hover">
                            <thead>
                            <tr>
                                <th>{{ __('model.Name') }}</th>
                                <th>{{ trans_choice('model.Mechanic', 2) }}</th>
                                <th>{{ __('model.Time left') }}</th>
                                <th>{{ trans_choice('model.Time registration', 1) }}</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($customer->assignments as $assignment)
                                <tr>
                                    <td>
                                        @can('show assignments')
                                            <a href="{{ route('assignments.show', $assignment->id) }}">
                                                {{ $assignment->name }}
                                            </a>
                                        @endcan

                                        @cannot('show assignments')
                                            {{ $assignment->name }}
                                        @endcan
                                    </td>
                                    <td>{{ $assignment->users->count() }}</td>
                                    <td>{{ Carbon\Carbon::now()->diffForHumans($assignment->end_date) }}</td>
                                    <td>
                                        @php
                                            $times = $assignment->time_registrations->pluck('time')->toArray();
                                             $tot = sum_time($times);
                                        @endphp

                                        {{ date('H:i:s', strtotime($tot)) }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="banner banner--single-line">
                            <div class="banner__content">
                                {{ __('model.not_found', ['model' => trans_choice('model.Assignment', 2)]) }}
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
