@extends('layouts.app')
@section('title', trans_choice('model.Customer', 2))

@section('content')
    <div class="row">
        @include('partials.create-button')

        @include('partials.filter',
            [
                'items' => $customers,
                'fields' => [
                   'id' => __('model.Id'),
                   'name' => __('model.Name'),
                   'boat' => __('model.Boat'),
                   'user_created' => __('model.Creator'),
                   'user_updated' => __('model.Updater'),
                   'created_at' => __('model.Created at'),
                   'updated_at' => __('model.Updated at'),
                ]
            ]
       )

        @include('partials.pagination', ['items' => $customers])

        <div class="xs12">
            @if($customers->count() > 0)
                <table class="data-table data-table--hover data-table--responsive">
                    <thead>
                    <tr>
                        <th>{{ __('model.name', ['model' => trans_choice('model.Customer', 1)]) }}</th>
                        <th>{{ __('model.name', ['model' => __('model.Boat')]) }}</th>
                        <th class="data-table--numeric">{{ trans_choice('model.Assignment', 2) }}</th>
                        <th>{{ __('model.Actions') }}</th>
                    </tr>
                    </thead>

                    <tbody>
                        @foreach($customers as $customer)
                            <tr>
                                <td>
                                    @can('show customers')
                                        <a href="{{ route('customers.show', $customer->id) }}">
                                            {{ $customer->name }}
                                        </a>
                                    @endcan

                                    @cannot('show customers')
                                        {{ $customer->name }}
                                    @endcannot
                                </td>
                                <td>{{ $customer->boat }}</td>
                                <td class="data-table--numeric">{{ $customer->assignments->count() }}</td>
                                <td>
                                    @include('partials.actions',
                                        [
                                            'item' => $customer,
                                            'actions' => ['edit', 'show', 'delete']
                                        ]
                                    )
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @else
                @include('partials.not-found-banner', ['model' => trans_choice('model.customer', 2)])
            @endif
        </div>

        @include('partials.pagination', ['items' => $customers])
    </div>
@endsection;
