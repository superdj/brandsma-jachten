@extends('layouts.app')
@section('title', __('model.add', ['model' => trans_choice('model.Used part', 2).': "'.$assignment->name.'"']))

@section('content')
    <form method="POST" action="{{ route('used-parts.store', $assignment->id) }}">
        @csrf

        <div class="row">
            <div class="xs12">
                <a href="{{ route('assignments.index') }}" class="button button--contained button--contained--icon-left">
                    <i class="material-icons icon">arrow_back</i>
                    {{ __('model.back', ['page' => trans_choice('model.assignment', 2)]) }}
                </a>
            </div>

            <div class="xs12 sm6 md3">
                <div class="row padding--0">
                    <h5 class="xs12">{{ trans_choice('model.Used part', 1) }}</h5>

                    <datalist id="part_shelf">
                        @foreach($shelves as $shelf)
                            @foreach($shelf->parts as $part)
                                @if($part->suppliers->count() > 0)
                                    @php
                                        $number = '';
                                        foreach($part->suppliers as $supplier)
                                        {
                                            if($supplier->pivot->preferred)
                                            {
                                                $number = $supplier->number;
                                            }
                                        }
                                    @endphp

                                    @if(!empty($number))
                                        <option value="{{ $part->id }}_{{ $shelf->id }}">
                                            {{ $part->name }} -
                                            {{ $number }} -
                                            {{ $shelf->rack->warehouse->name }} -
                                            {{ $shelf->rack->name }} -
                                        {{ $shelf->name }}
                                    @else
                                        <option value="{{ $part->id }}_{{ $shelf->id }}">
                                            {{ $part->name }} -
                                            {{ $shelf->rack->warehouse->name }} -
                                            {{ $shelf->rack->name }} -
                                        {{ $shelf->name }}
                                    @endif
                                @else
                                    <option value="{{ $part->id }}_{{ $shelf->id }}">
                                        {{ $part->name }} -
                                        {{ $shelf->rack->warehouse->name }} -
                                        {{ $shelf->rack->name }} -
                                       {{ $shelf->name }}
                                    </option>
                                @endif
                            @endforeach
                        @endforeach
                    </datalist>

                    <div class="xs12" id="parts">
                        @if(old('parts'))
                            @foreach(old('parts') as $part)
                                <div class="row padding--0 part">
                                    <div class="xs12 md8">
                                        <div class="text-field text-field--filled {{ $errors->has('parts') ? 'text-field--helper-text text-field--invalid' : '' }}">
                                            <input
                                                id="part-{{ $loop->index }}"
                                                type="text"
                                                list="part_shelf"
                                                class="text-field__input"
                                                name="parts[]"
                                                value="{{ old('parts') }}"
                                                required
                                            >
                                            <label for="part-{{ $loop->index }}" class="text-field__label">{{ trans_choice('model.Part', 2) }}*</label>

                                            @if ($errors->has('parts'))
                                                <span class="text-field__helper-text" role="alert">
                                                    {{ $errors->first('parts') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="xs12 md4">
                                        <div class="text-field text-field--filled {{ $errors->has('amounts') ? 'text-field--helper-text text-field--invalid' : '' }}">
                                            <input id="amount-{{ $loop->index }}"
                                                   type="number"
                                                   class="text-field__input"
                                                   name="amounts[]"
                                                   value="{{ old('amounts') ?? '0.00' }}"
                                                   min="0.00" step="0.01"
                                                   required
                                            >
                                            <label for="amount-{{ $loop->index }}" class="text-field__label">{{ __('model.Amount') }}*</label>

                                            @if ($errors->has('amounts'))
                                                <span class="text-field__helper-text" role="alert">
                                                    {{ $errors->first('amounts') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="xs12">
                                        <button class="button button--outlined delete-part" type="button">
                                            {{ __('model.delete', ['model' => trans_choice('model.Part', 1)]) }}
                                        </button>
                                    </div>

                                    <hr class="divider xs12">
                            @endforeach
                        @endif
                    </div>

                    <div class="xs12">
                        <button class="button button--outlined" type="button" id="add-part">
                            {{ __('model.add', ['model' => trans_choice('model.Part', 1)]) }}
                        </button>
                    </div>
                </div>
            </div>

            <div class="xs12">
                <button type="submit" class="button button--contained button--contained--icon-left">
                    <i class="material-icons icon">save</i>
                    {{ __('model.add', ['model' => trans_choice('model.Used part', 2)]) }}
                </button>

                <button type="reset" class="button button--text button--text--icon-left">
                    <i class="material-icons icon">settings_backup_restore</i>
                    {{ __('model.reset') }}
                </button>
            </div>
        </div>
        </div>
    </form>
@endsection

@section('js')
    <script>
        const addPart = document.getElementById('add-part');
        const partsContainer = document.getElementById('parts');
        let deleteParts = document.getElementsByClassName('delete-part');
        let parts = {{ old('parts') ? count(old('parts')) : 0 }};

        addPart.addEventListener('click', () => {
            let html = `
                <div class="xs12 md8">
                    <div class="text-field text-field--filled">
                        <input id="part-${parts}" type="text" list="part_shelf" class="text-field__input" name="parts[]" required>
                        <label for="part-${parts}" class="text-field__label">{{ trans_choice('model.Part', 1) }}*</label>
                    </div>
                </div>

                <div class="xs12 md4">
                    <div class="text-field text-field--filled">
                        <input id="amount-${parts}" type="number" min="0.00" step="0.01" class="text-field__input" name="amounts[]" required>
                        <label for="amount-${parts}" class="text-field__label">{{ __('model.Amount') }}*</label>
                    </div>
                </div>

                <div class="xs12">
                    <button class="button button--outlined delete-part" type="button">
                        {{ __('model.delete', ['model' => trans_choice('model.Part', 1)]) }}
                    </button>
                </div>

                <hr class="divider xs12">`;

            partsContainer.appendChild(document.createElement('id'));
            let lastChild = partsContainer.lastChild;
            lastChild.className = 'row padding--0 part';
            lastChild.innerHTML = html;
            parts++;

            for( let deletePart of deleteParts )
            {
                deletePart.addEventListener('click', () => {
                    let part = deletePart.closest('.part');
                    part.remove();
                });
            }
        });
    </script>
@endsection
