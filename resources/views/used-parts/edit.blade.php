@extends('layouts.app')
@section('title', __('model.edit_title', [
    'name' => $used_part->part->name,
    'model' => trans_choice('model.used part', 1)
]))

@section('content')
    <form method="POST" action="{{ route('used-parts.update', $used_part->id) }}">
        @method('put')
        @csrf

        <div class="row">
            <div class="xs12">
                <a href="{{ route('used-parts.index') }}" class="button button--contained button--contained--icon-left">
                    <i class="material-icons icon">arrow_back</i>
                    {{ __('model.back', ['page' => trans_choice('model.Used part', 2)]) }}
                </a>
            </div>

            <div class="xs12 sm6 md3">
                <div class="row padding--0">
                    <h5 class="xs12">{{ trans_choice('model.Used part', 1) }}</h5>

                    <div class="xs12">
                        <div class="text-field text-field--filled {{ $errors->has('amount') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <input id="amount" type="number" min="0.00" step="0.01" class="text-field__input" name="amount" value="{{ old('amount') ?? $used_part->amount }}" required autofocus>
                            <label for="amount" class="text-field__label">{{ __('model.Amount') }}*</label>

                            @if ($errors->has('amount'))
                                <span class="text-field__helper-text" role="alert">
                                    {{ $errors->first('amount') }}
                                </span>
                            @endif
                        </div>
                    </div>
            </div>

            <div class="xs12">
                <button type="submit" class="button button--contained button--contained--icon-left">
                    <i class="material-icons icon">save</i>
                    {{ __('model.edit', ['model' => trans_choice('model.used part', 1)]) }}
                </button>

                <button type="reset" class="button button--text button--text--icon-left">
                    <i class="material-icons icon">settings_backup_restore</i>
                    {{ __('model.reset') }}
                </button>
            </div>
        </div>
        </div>
    </form>
@endsection
