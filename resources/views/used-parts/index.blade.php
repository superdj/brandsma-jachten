@extends('layouts.app')
@section('title', trans_choice('model.Used part', 2))

@section('content')
    <div class="row">
        @include('partials.filter',
           [
               'items' => $used_parts,
               'fields' => [
                    'id' => __('model.Id'),
                    'assignment_id' => trans_choice('model.Assignment', 1),
                    'part_id' => trans_choice('model.Part', 1),
                    'amount' => __('model.Amount')
               ]
           ]
        )

        @include('partials.pagination', ['items' => $used_parts])

        <div class="xs12">
            @if($used_parts->count() > 0)
                <table class="data-table data-table--hover data-table--responsive">
                    <thead>
                    <tr>
                        <th>{{ trans_choice('model.Assignment', 1) }}</th>
                        <th>{{ trans_choice('model.Customer', 1) }}</th>
                        <th>{{ __('model.Boat') }}</th>
                        <th>{{ trans_choice('model.Part', 1) }}</th>
                        <th class="data-table--numeric">{{ __('model.Amount') }}</th>
                        <th>{{ __('model.Actions') }}</th>
                    </tr>
                    </thead>

                    <tbody>
                        @foreach($used_parts as $used_part)
                            <tr>
                                <td>
                                    @can('show assignments')
                                        <a href="{{ route('assignments.show', $used_part->assignment->id) }}">
                                            {{ $used_part->assignment->name }}
                                        </a>
                                    @endcan

                                    @cannot('show assignments')
                                        {{ $used_part->assignment->name }}
                                    @endcannot
                                </td>
                                <td>
                                    @can('show customers')
                                        <a href="{{ route('customers.show', $used_part->assignment->customer->id) }}">
                                            {{ $used_part->assignment->customer->name }}
                                        </a>
                                    @endcan

                                    @cannot('show customers')
                                        {{ $used_part->assignment->customer->name }}
                                    @endcannot
                                </td>
                                <td>
                                    @can('show customers')
                                        <a href="{{ route('customers.show', $used_part->assignment->customer->id) }}">
                                            {{ $used_part->assignment->customer->boat }}
                                        </a>
                                    @endcan

                                    @cannot('show customers')
                                        {{ $used_part->assignment->customer->boat }}
                                    @endcannot
                                </td>
                                <td>
                                    @can('show assignments')
                                        <a href="{{ route('parts.show', $used_part->part->id) }}">
                                            {{ $used_part->part->name }}
                                        </a>
                                    @endcan

                                    @cannot('show parts')
                                        {{ $used_part->part->name }}
                                    @endcannot
                                </td>
                                <td class="data-table--numeric">
                                    {{ $used_part->amount }}
                                    {{ $used_part->part->unit->abbreviation }}
                                </td>
                                <td>
                                    @if(!$used_part->assignment->status->lock)
                                        @include('partials.actions',
                                        [
                                            'item' => $used_part,
                                            'actions' => ['edit', 'delete']
                                        ]
                                    )
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @else
                @include('partials.not-found-banner', ['model' => trans_choice('model.used part', 2)])
            @endif
        </div>

        @include('partials.pagination', ['items' => $used_parts])
    </div>
@endsection;
