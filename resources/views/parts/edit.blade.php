@extends('layouts.app')
@section('title', __('model.edit_title', ['name' => $part->name, 'model' => trans_choice('model.part', 1)]))

@section('content')
    <form method="POST" action="{{ route('parts.update', $part->id) }}">
        @method('put')
        @csrf

        <div class="row">
            <div class="xs12">
                <a href="{{ route('parts.index') }}" class="button button--contained button--contained--icon-left">
                    <i class="material-icons icon">arrow_back</i>
                    {{ __('model.back', ['page' => trans_choice('model.part', 2)]) }}
                </a>
            </div>

            <div class="xs12 sm6 md3">
                <div class="row padding--0">
                    <h5 class="xs12">{{ trans_choice('model.Part', 1) }}</h5>

                    <div class="xs12">
                        <div class="text-field text-field--filled {{ $errors->has('name') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <input id="name" type="text" class="text-field__input" name="name" value="{{ old('name') ?? $part->name }}" required autofocus>
                            <label for="name" class="text-field__label">{{ __('model.name', ['model' => trans_choice('model.Part', 1)]) }}*</label>

                            @if ($errors->has('name'))
                                <span class="text-field__helper-text" role="alert">
                                    {{ $errors->first('name') }}
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="xs12">
                        <div class="text-field text-field--filled {{ $errors->has('price') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <input id="price" type="number" min="0.00" step="0.01" class="text-field__input" name="price" value="{{ old('price') ?? $part->price }}" required>
                            <label for="price" class="text-field__label">{{ __('model.Gross price') }}*</label>

                            @if ($errors->has('price'))
                                <span class="text-field__helper-text" role="alert">
                                    {{ $errors->first('price') }}
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="xs12">
                        <div class="text-field text-field--filled text-field--select {{ $errors->has('categories') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <div class="text-field__input"></div>

                            <select name="categories[]" id="categories" multiple>
                                @foreach($categories as $category)
                                    <option
                                        value="{{ $category->id }}"
                                        {{ old('categories') && old('categories') !== null && in_array($category->id, old('categories')) || $part->categories && in_array($category->id, $part->categories->pluck('id')->toArray()) ? 'selected' : '' }}
                                    >
                                        {{ $category->name }}
                                    </option>
                                @endforeach
                            </select>

                            <label for="categories" class="text-field__label">{{trans_choice('model.Category', 2) }}</label>

                            @if ($errors->has('categories'))
                                <span class="text-field__helper-text" role="alert">
                                    {{ $errors->first('categories') }}
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="xs12">
                        <div class="text-field text-field--filled text-field--select {{ $errors->has('unit_id') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <div class="text-field__input"></div>

                            <select name="unit_id" id="unit_id">
                                @foreach($units as $unit)
                                    <option value="{{ $unit->id }}" {{ old('unit_id') === $unit->id || $unit->id === $part->unit_id ? 'selected' : '' }}>
                                        {{ $unit->name }}
                                    </option>
                                @endforeach
                            </select>

                            <label for="unit_id" class="text-field__label">{{trans_choice('model.Unit', 1) }}*</label>

                            @if ($errors->has('unit_id'))
                                <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('unit_id') }}
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="xs12">
                        <div class="text-field text-field--filled {{ $errors->has('minimal_stock') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <input id="minimal_stock" type="number" min="0.00" step="0.01" class="text-field__input" name="minimal_stock" value="{{ old('minimal_stock') ?? $part->minimal_stock }}">
                            <label for="minimal_stock" class="text-field__label">{{ __('model.Minimal stock') }}</label>

                            @if ($errors->has('minimal_stock'))
                                <span class="text-field__helper-text" role="alert">
                                    {{ $errors->first('minimal_stock') }}
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            @include('parts.form-shelves', [
                'part' => $part,
                'shelves' => $shelves
            ])

            @include('parts.form-suppliers', [
                'part' => $part,
                'suppliers' => $suppliers
            ])

            <div class="xs12">
                <button type="submit" class="button button--contained button--contained--icon-left">
                    <i class="material-icons icon">save</i>
                    {{ __('model.edit', ['model' => trans_choice('model.Part', 1)]) }}
                </button>

                <button type="reset" class="button button--text button--text--icon-left">
                    <i class="material-icons icon">settings_backup_restore</i>
                    {{ __('model.reset') }}
                </button>
            </div>
        </div>
    </form>
@endsection
