@extends('layouts.app')
@section('title', __('model.info', ['name' => $part->name, 'model' => trans_choice('model.Part', 1)]))

@section('content')
    <div class="row">
        <div class="xs12">
            <a href="{{ route('parts.index') }}" class="button button--contained button--contained--icon-left">
                <i class="material-icons icon">arrow_back</i>
                {{ __('model.back', ['page' => trans_choice('model.part', 2)]) }}
            </a>
        </div>

        <div class="xs12 sm6 md3">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        <i class="material-icons icon">info</i> {{ __('model.General') }}
                    </div>
                </div>

                <div class="card__text">
                    <ul class="list">
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.name', ['model' => trans_choice('model.Part', 1)]) }}</span>
                                <span class="list__content__text">{{ $part->name }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.Gross price') }}</span>
                                <span class="list__content__text">&euro;{{ number_format($part->price, 2, ',', '.') }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Category', 2) }}</span>
                                <span class="list__content__text">
                                    @foreach($part->categories as $category)
                                        @can('show categories')
                                            <a href="{{ route('categories.show', $category->id) }}">
                                                {{ $category->name }}
                                            </a>
                                        @endcan

                                        @cannot('show categories')
                                            {{ $category->name }}
                                        @endcannot

                                        {{ $loop->last ? '' : ',' }}
                                    @endforeach
                                </span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.Minimal stock') }}</span>
                                <span class="list__content__text">{{ $part->minimal_stock }} {{ $part->unit->abbreviation }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Created by', 1) }}</span>
                                <span class="list__content__text">
                                    @can('show users')
                                        <a href="{{ route('users.show', $part->creator->id) }}">
                                            {{ $part->creator->first_name }}
                                        </a>
                                    @endcan

                                    @cannot('show users')
                                        {{ $part->creator->first_name }}
                                    @endcannot
                                </span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Updated by', 1) }}</span>
                                <span class="list__content__text">
                                    @if($part->updater)
                                        @can('show users')
                                            <a href="{{ route('users.show', $part->updater->id) }}">
                                            {{ $part->updater->first_name }}
                                        </a>
                                        @endcan

                                        @cannot('show users')
                                            {{ $part->updater->first_name }}
                                        @endcannot
                                    @endif
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>

                @can('edit parts')
                    <div class="card__actions">
                        <a href="{{ route('parts.edit', $part->id) }}" class="button button--text">
                            {{ __('model.edit', ['model' => trans_choice('model.Part', 1)]) }}
                        </a>
                    </div>
                @endcan
            </div>
        </div>

        <div class="xs12 sm6 md4">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        <i class="material-icons icon">storage</i>
                        {{ __('model.Stock') }}
                    </div>
                </div>

                <div class="card__text">
                    @if($part->shelves->count() > 0)
                        <table class="data-table data-table--hover">
                            <thead>
                            <tr>
                                <th>{{ trans_choice('model.Location', 1) }}</th>
                                <th class="data-table--numeric">{{ __('model.Stock') }}</th>
                                <th class="data-table--numeric">{{ __('model.Worth') }}</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($part->shelves->sortBy('pivot.stock') as $shelf)
                                <tr>
                                    <td>
                                        @can('show shelves')
                                            <a href="{{ route('shelves.show', $shelf->id) }}">
                                                {{ $shelf->rack->warehouse->name }} -
                                                {{ $shelf->rack->name }} -
                                                {{ $shelf->name }}
                                            </a>
                                        @endcan

                                        @cannot('show shelves')
                                            {{ $shelf->rack->warehouse->name }} -
                                            {{ $shelf->rack->name }} -
                                            {{ $shelf->name }}
                                        @endcannot
                                    </td>
                                    <td class="data-table--numeric">
                                        {{ $shelf->pivot->stock }}
                                        {{ $part->unit->abbreviation }}
                                    </td>
                                    <td class="data-table--numeric">
                                        &euro;{{ number_format($shelf->pivot->stock * $part->price, 2, ',', '.') }}
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td><strong>{{ __('model.Total') }}</strong></td>
                                <td class="data-table--numeric">
                                    {{ $part->shelves()->sum('stock') }}
                                    {{ $part->unit->abbreviation }}
                                </td>
                                <td class="data-table--numeric">
                                    &euro;{{ number_format($part->shelves()->sum('stock') * $part->price, 2, ',', '.') }}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    @else
                        @include('partials.not-found-banner', ['model' => trans_choice('model.location', 2)])
                    @endif
                </div>
            </div>
        </div>

        <div class="xs12 sm6 md4">
            <div class="row padding--0">
                @can('create orders')
                    <div class="xs12">
                        <form action="{{ route('cart.add', $part->id) }}" method="post">
                            @csrf
                            <div class="card">
                                <div class="card__header">
                                    <div class="card__title">
                                        <i class="material-icons icon">add_shopping_cart</i>
                                        {{ __('model.Order part') }}
                                    </div>
                                </div>

                                <div class="card__text">
                                    <div class="row padding--0">
                                        <div class="xs12">
                                            <div class="text-field text-field--filled">
                                                @php
                                                    $amount = null;
                                                    if(session('cart') !== null && isset(session('cart')->items[$part->id]))
                                                    {
                                                        $amount = session('cart')->items[$part->id]['amount'];
                                                    } else {
                                                        $amount = old('amount');
                                                    }
                                                @endphp
                                                <input
                                                    type="number"
                                                    min="0.00"
                                                    step="0.01"
                                                    name="amount"
                                                    id="amount"
                                                    class="text-field__input"
                                                    value="{{ $amount }}"
                                                >
                                                <label for="amount" class="text-field__label">{{ __('model.Amount') }}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card__actions">
                                    <button type="submit" class="button button--text button--text--icon-left">
                                        {{ __('model.add to cart') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                @endcan

                <div class="xs12">
                    <div class="card">
                        <div class="card__header">
                            @can('view parts')
                                <a href="{{ route('orders.index') }}">
                                    <i class="material-icons icon">shopping_cart</i>
                                    {{ trans_choice('model.Order', 2) }}
                                </a>
                            @endcan

                            @cannot('view parts')
                                {{ trans_choice('model.Order', 2) }}
                            @endcannot
                        </div>

                        <div class="card__text">
                            @if($part->orders->count() > 0)
                                <table class="data-table data-table--hover">
                                    <thead>
                                    <tr>
                                        <th>{{ __('model.Name') }}</th>
                                        <th class="data-table--numeric">{{ __('model.Amount') }}</th>
                                        <th class="data-table--numeric">{{ __('model.Worth') }}</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($part->orders as $order)
                                        <tr>
                                            <td>
                                                @can('show orders')
                                                    <a href="{{ route('orders.show', $order->id) }}">
                                                        {{ $order->name }}
                                                    </a>
                                                @endcan

                                                @cannot('show orders')
                                                    {{ $order->name }}
                                                @endcannot
                                            </td>
                                            <td class="data-table--numeric">{{ $order->pivot->amount }} {{ $part->unit->abbreviation }}</td>
                                            <td class="data-table--numeric">&euro;{{ number_format($order->pivot->amount * $part->price, 2, ',', '.') }}</td>
                                        </tr>
                                    @endforeach

                                    <tr>
                                        <td><strong>{{ __('model.Total') }}</strong></td>
                                        <td class="data-table--numeric">{{ $part->orders()->sum('amount') }}</td>
                                        <td class="data-table--numeric">&euro;{{ number_format($part->orders()->sum('amount') * $part->price, 2, ',', '.') }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            @else
                                @include('partials.not-found-banner', ['model' => trans_choice('model.order', 2)])
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="xs12 sm6 ">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        @can('view suppliers')
                            <a href="{{ route('suppliers.index') }}">
                                <i class="material-icons icon">local_shipping</i>
                                {{ trans_choice('model.Supplier', 2) }}
                            </a>
                        @endcan

                        @cannot('view suppliers')
                            <i class="material-icons icon">local_shipping</i>
                            {{ trans_choice('model.Supplier', 2) }}
                        @endcannot
                    </div>
                </div>

                <div class="card__text">
                    @if($part->suppliers->count() > 0)
                        <table class="data-table data-table--hover">
                            <thead>
                            <tr>
                                <th>{{ trans_choice('model.Supplier', 1) }}</th>
                                <th>{{ __('model.Parts number') }}</th>
                                <th>{{ __('model.Nett price') }}</th>
                                <th>{{ __('model.Discount') }}</th>
                                <th>{{ __('model.Preferred') }}</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($part->suppliers->sortBy('name') as $supplier)
                                <tr>
                                    <td>
                                        @can('show suppliers')
                                            <a href="{{ route('suppliers.show', $supplier->id) }}">
                                                {{ $supplier->name }}
                                            </a>
                                        @endcan

                                        @cannot('show suppliers')
                                            {{ $supplier->name }}
                                        @endcannot
                                    </td>
                                    <td>{{ $supplier->pivot->number }}</td>
                                    <td>&euro;{{ number_format($supplier->pivot->nett_price, '2', ',', '.') }}</td>
                                    <td>{{ $supplier->pivot->discount }}%</td>
                                    <td>
                                        @if($supplier->pivot->preferred)
                                            <i class="material-icons icon text--green">check_box</i>
                                        @else
                                            <i class="material-icons icon text--red">check_box_outline_blank</i>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        @include('partials.not-found-banner', ['model' => trans_choice('model.supplier', 2)])
                    @endif
                </div>
            </div>
        </div>

        <div class="xs12 md6">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        <i class="material-icons icon">timeline</i>
                        {{ __('model.Part usage') }}
                    </div>
                </div>

                <div class="card__text">
                    @if(count($usage) > 0)
                        <div id="assignment_part"></div>
                    @else
                        @include('partials.not-found-banner', ['model' => trans_choice('model.used part', 2)])
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@if(count($usage) > 0)
    @php
        $series = [];
        foreach( $usage as $key => $value )
        {
            echo $value->year;
            $series[$value->year][$value->month] = (int)$value->amount;
        }

        foreach( $series as $key => $value )
        {
            for( $i = 1; $i <= 12; $i++ )
            {
                if( !array_key_exists( $i, $value ) )
                {
                    $series[$key][$i] = 0;
                }
            }

            ksort($series[$key]);
        }
    @endphp

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script>
		let options = {
			chart: {
				type: 'heatmap',
				toolbar: false,
				height: 500
			},
			dataLabels: {
				enabled: true
			},
			series: [
                    @foreach($series as $key => $value)
				{
					name: '{{ $key }}',
					data: [{{ implode(',',$value) }}]
				},
                @endforeach
			],
			xaxis: {
				categories: [
                    @foreach(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'] as $month)
						'{{ trans_choice('model.'.$month, 1) }}',
                    @endforeach
				]
			}
		};

		const chart = new ApexCharts(
			document.getElementById( 'assignment_part' ),
			options
		);

		chart.render();
    </script>
@endsection
@endif
