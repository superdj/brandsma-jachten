@extends('layouts.app')
@section('title', trans_choice('model.Part', 2))

@section('content')
    <div class="row">
        @can('create parts')
            <div class="xs12">
                <a href="{{ route('parts.create') }}" class="button button--contained button--contained--icon-left">
                    <i class="material-icons icon">add</i>
                    {{ __('model.add', ['model' => trans_choice('model.Part', 1)]) }}
                </a>
            </div>
        @endcan

        @include('partials.filter',
            [
                'items' => $parts,
                'fields' => [
                    'id' => __('model.Id'),
                    'name' => __('model.Name'),
                    'minimal_stock' => __('model.Minimal stock'),
                    'price' => __('model.Price'),
                    'user_created' => __('model.Creator'),
                    'user_updated' => __('model.Updater'),
                    'created_at' => __('model.Created at'),
                    'updated_at' => __('model.Updated at'),
                ]
            ]
        )

        @include('partials.pagination', ['items' => $parts])

        <div class="xs12">
            @if($parts->count() > 0)
                <table class="data-table data-table--hover data-table--responsive">
                    <thead>
                    <tr>
                        <th>{{ __('model.name', ['model' => trans_choice('model.Part', 1)]) }}</th>
                        <th class="data-table--numeric">{{ __('model.Stock') }}</th>
                        <th class="data-table--numeric">{{ trans_choice('model.Location', 2) }}</th>
                        <th class="data-table--numeric">{{ __('model.Gross price') }}</th>
                        <th>{{ __('model.Actions') }}</th>
                    </tr>
                    </thead>

                    <tbody>
                        @foreach($parts as $part)
                            <tr>
                                <td>
                                    @can('show parts')
                                        <a href="{{ route('parts.show', $part->id) }}">
                                            {{ $part->name }}
                                        </a>
                                    @endcan

                                    @cannot('show parts')
                                        {{ $part->name }}
                                    @endcannot
                                </td>
                                <td class="data-table--numeric {{ $part->minimal_stock && $part->shelves()->sum('stock') < $part->minimal_stock ? 'text--red-900 red-50' : 'text--green-900 green-50' }}">
                                    {{ $part->shelves()->sum('stock') }} / {{ $part->minimal_stock }}
                                    {{ $part->unit->abbreviation ? $part->unit->abbreviation : $part->unit->name }}
                                </td>
                                <td class="data-table--numeric">{{ $part->shelves->count() }}</td>
                                <td class="data-table--numeric">&euro;{{ number_format($part->price, 2, ',', '.') }}</td>
                                <td>
                                    @include('partials.actions',
                                        [
                                            'item' => $part,
                                            'actions' => ['edit', 'show', 'delete']
                                        ]
                                    )
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @else
                @include('partials.not-found-banner', ['model' => trans_choice('model.part', 2)])
            @endif
        </div>

        @include('partials.pagination', ['items' => $parts])
    </div>
@endsection;
