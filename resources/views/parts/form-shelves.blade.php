<div class="xs12 sm6 md3">
    <div class="row padding--0">
        <h5 class="xs12">{{ trans_choice('model.Location', 2) }}</h5>

        <datalist id="shelves">
            @foreach( $shelves as $shelf )
                <option value="{{ $shelf->id }}">
                    {{ $shelf->rack->warehouse->name }} -
                    {{ $shelf->rack->name }} -
                    {{ $shelf->name }}
                </option>
            @endforeach
        </datalist>

        <div class="xs12" id="locations">
            @if(old('shelves'))
                @foreach(old('shelves') as $shelf)
                    <div class="row padding--0 location">
                        <div class="xs12 md8">
                            <div class="text-field text-field--filled">
                                <input
                                    id="shelf_id-{{ $loop->index }}"
                                    type="text"
                                    list="shelves"
                                    class="text-field__input"
                                    name="shelves[]"
                                    value="{{ old('shelves')[$loop->index] }}"
                                    required
                                >
                                <label for="shelf_id-{{ $loop->index }}" class="text-field__label">{{ trans_choice('model.Location', 1) }}*</label>
                            </div>
                        </div>

                        <div class="xs12 md4">
                            <div class="text-field text-field--filled">
                                <input
                                    id="stock-{{ $loop->index }}"
                                    type="text"
                                    value="{{ old('stock') ? old('stock')[$loop->index] : 1 }}"
                                    min="0.00"
                                    step="0.01"
                                    class="text-field__input"
                                    name="stock[]"
                                    required
                                >
                                <label for="stock-{{ $loop->index }}" class="text-field__label">{{ __('model.Stock') }}*</label>
                            </div>
                        </div>

                        <div class="xs12">
                            <button class="button button--outlined delete-location" type="button">
                                {{ __('model.delete', ['model' => trans_choice('model.Location', 1)]) }}
                            </button>
                        </div>

                        <hr class="divider xs12">
                    </div>
                @endforeach
            @endif

            @if($part && $part->shelves && !old('shelves'))
                @foreach($part->shelves as $shelf)
                    <div class="row padding--0 location">
                        <div class="xs12 md8">
                            <div class="text-field text-field--filled">
                                <input
                                    id="shelf_id-{{ $loop->index }}"
                                    type="text"
                                    list="shelves"
                                    class="text-field__input"
                                    name="shelves[]"
                                    value="{{ $shelf->id ?? '' }}"
                                    required
                                >
                                <label for="shelf_id-{{ $loop->index }}" class="text-field__label">{{ trans_choice('model.Location', 1) }} *</label>
                            </div>
                        </div>

                        <div class="xs12 md4">
                            <div class="text-field text-field--filled">
                                <input
                                    id="stock-{{ $loop->index }}"
                                    type="text"
                                    value="{{ $shelf->pivot->stock ?? 0 }}"
                                    min="0.00"
                                    step="0.01"
                                    class="text-field__input"
                                    name="stock[]"
                                    required
                                >
                                <label for="stock-{{ $loop->index }}" class="text-field__label">{{ __('model.Stock') }}*</label>
                            </div>
                        </div>

                        <div class="xs12">
                            <button class="button button--outlined delete-location" type="button">
                                {{ __('model.delete', ['model' => trans_choice('model.Location', 1)]) }}
                            </button>
                        </div>

                        <hr class="divider xs12">
                    </div>
                @endforeach
            @endif
        </div>

        <div class="xs12">
            <button class="button button--outlined" type="button" id="add-location">
                {{ __('model.add', ['model' => trans_choice('model.Location', 1)]) }}
            </button>
        </div>
    </div>
</div>

<script>
    const addLocation = document.getElementById( 'add-location' );
    const locationsContainer = document.getElementById( 'locations' );
    let deleteLocations = document.getElementsByClassName( 'delete-location' );

    @php
        $locations = 0;
        if( old('shelves') )
        {
            $locations = count(old('shelves'));
        }

        if( $part && $part->shelves )
        {
            $locations = $part->shelves->count();
        }
    @endphp

    let locations = {{ $locations }};

    addLocation.addEventListener( 'click', () =>
    {
        let html = `
                <div class="xs12 md8">
                    <div class="text-field text-field--filled">
                        <input id="shelf_id-${locations}" type="text" list="shelves" class="text-field__input" name="shelves[]" required>
                        <label for="shelf_id-${locations}" class="text-field__label">{{ trans_choice('model.Location', 1) }}*</label>
                    </div>
                </div>

                <div class="xs12 md4">
                    <div class="text-field text-field--filled">
                        <input id="stock-${locations}" type="number" value="0" min="0.00" step="0.01" class="text-field__input" name="stock[]" required>
                        <label for="stock-${locations}" class="text-field__label">{{ __('model.Stock') }}*</label>
                    </div>
                </div>

                <div class="xs12">
                    <button class="button button--outlined delete-location" type="button">
                        {{ __('model.delete', ['model' => trans_choice('model.Location', 1)]) }}
                    </button>
                </div>

                <hr class="divider xs12">
        `;

        locationsContainer.appendChild( document.createElement( 'id' ) );
        let lastChild = locationsContainer.lastChild;
        lastChild.className = 'row padding--0 location';
        lastChild.innerHTML = html;
        locations++;

        for( let deleteLocation of deleteLocations ) {
            deleteLocation.addEventListener( 'click', () =>
            {
                let location = deleteLocation.closest( '.location' );
                location.remove();
            } );
        }
    });

    for( let deleteLocation of deleteLocations ) {
        deleteLocation.addEventListener( 'click', () =>
        {
            let location = deleteLocation.closest( '.location' );
            location.remove();
        } );
    }
</script>
