<div class="xs12 sm6 md3">
    <div class="row padding--0">
        <h5 class="xs12">{{ trans_choice('model.Supplier', 2) }}</h5>

        <datalist id="suppliers-list">
            @foreach( $suppliers as $supplier )
                <option value="{{ $supplier->id }}">
                    {{ $supplier->name }}
                </option>
            @endforeach
        </datalist>

        <div class="xs12" id="suppliers">
            @if(old('suppliers'))
                @foreach(old('suppliers') as $supplier)
                    <div class="row padding--0 supplier">
                        <div class="xs12 md6">
                            <div class="text-field text-field--filled">
                                <input
                                    id="supplier_id-{{ $loop->index }}"
                                    type="text" list="suppliers-list"
                                    class="text-field__input"
                                    name="suppliers[]"
                                    value="{{ old('suppliers')[$loop->index] }}"
                                    required
                                >
                                <label for="supplier_id-{{ $loop->index }}" class="text-field__label">{{ trans_choice('model.Supplier', 1) }}*</label>
                            </div>
                        </div>

                        <div class="xs12 md6">
                            <div class="text-field text-field--filled">
                                <input
                                    id="number-{{ $loop->index }}"
                                    type="text"
                                    class="text-field__input"
                                    name="numbers[]"
                                    value="{{ old('numbers') ? old('numbers')[$loop->index] : '' }}"
                                    required
                                >
                                <label for="number-{{ $loop->index }}" class="text-field__label">{{ __('model.Parts number') }}*</label>
                            </div>
                        </div>

                        <div class="xs12 md6">
                            <div class="text-field text-field--filled">
                                <input
                                    id="price-{{ $loop->index }}"
                                    type="number"
                                    value="{{ old('nett_prices') ? old('nett_prices')[$loop->index] : '0.00' }}"
                                    min="0.00"
                                    step="0.01"
                                    class="text-field__input"
                                    name="nett_prices[]"
                                    required
                                >
                                <label for="price-{{ $loop->index }}" class="text-field__label">{{ __('model.Nett price') }}*</label>
                            </div>
                        </div>

                        <div class="xs12 md6">
                            <div class="text-field text-field--filled">
                                <input
                                    id="discount-{{ $loop->index }}"
                                    type="number"
                                    value="{{ old('discounts') ? old('discounts')[$loop->index] : 0 }}"
                                    min="0"
                                    max="100"
                                    step="1"
                                    class="text-field__input"
                                    name="discounts[]"
                                    required
                                >
                                <label for="discount-{{ $loop->index }}" class="text-field__label">{{ __('model.Discount') }}*</label>
                            </div>
                        </div>

                        <div class="xs12 md6">
                            <label>
                                <input
                                    type="radio"
                                    class="radio"
                                    value="{{ old('suppliers')[$loop->index] }}"
                                    name="preferred"
                                    {{ old('preferred') && old('preferred') == old('suppliers')[$loop->index] ? 'checked' : '' }}
                                >
                                {{ __('model.Preferred') }}
                            </label>
                        </div>

                        <div class="xs12">
                            <button class="button button--outlined delete-supplier" type="button">
                                {{ __('model.delete', ['model' => trans_choice('model.Supplier', 1)]) }}
                            </button>
                        </div>

                        <hr class="divider xs12">
                    </div>
                @endforeach
            @endif

            @if($part && $part->suppliers && !old('suppliers'))
                @foreach($part->suppliers as $supplier)
                    <div class="row padding--0 supplier">
                        <div class="xs12 md6">
                            <div class="text-field text-field--filled">
                                <input
                                    id="supplier_id-{{ $loop->index }}"
                                    type="text" list="suppliers-list"
                                    class="text-field__input"
                                    name="suppliers[]"
                                    value="{{ $supplier->pivot->supplier_id ?? '' }}"
                                    required
                                >
                                <label for="supplier_id-{{ $loop->index }}" class="text-field__label">{{ trans_choice('model.Supplier', 1) }}*</label>
                            </div>
                        </div>

                        <div class="xs12 md6">
                            <div class="text-field text-field--filled">
                                <input
                                    id="number-{{ $loop->index }}"
                                    type="text"
                                    class="text-field__input"
                                    name="numbers[]"
                                    value="{{ $supplier->pivot->number ?? '' }}"
                                    required
                                >
                                <label for="number-{{ $loop->index }}" class="text-field__label">{{ __('model.Parts number') }}*</label>
                            </div>
                        </div>

                        <div class="xs12 md6">
                            <div class="text-field text-field--filled">
                                <input
                                    id="price-{{ $loop->index }}"
                                    type="number"
                                    value="{{ $supplier->pivot->nett_price ?? '0.00' }}"
                                    min="0.00"
                                    step="0.01"
                                    class="text-field__input"
                                    name="nett_prices[]"
                                    required
                                >
                                <label for="price-{{ $loop->index }}" class="text-field__label">{{ __('model.Nett price') }}*</label>
                            </div>
                        </div>

                        <div class="xs12 md6">
                            <div class="text-field text-field--filled">
                                <input
                                    id="discount-{{ $loop->index }}"
                                    type="number"
                                    value="{{ $supplier->pivot->discount ?? 0 }}"
                                    min="0"
                                    max="100"
                                    step="1"
                                    class="text-field__input"
                                    name="discounts[]"
                                    required
                                >
                                <label for="discount-{{ $loop->index }}" class="text-field__label">{{ __('model.Discount') }}*</label>
                            </div>
                        </div>

                        <div class="xs12 md6">
                            <label>
                                <input
                                    type="radio"
                                    class="radio"
                                    value="{{ $supplier->pivot->supplier_id }}"
                                    name="preferred"
                                    {{ $supplier->pivot->preferred ? 'checked' : '' }}
                                >
                                {{ __('model.Preferred') }}
                            </label>
                        </div>

                        <div class="xs12">
                            <button class="button button--outlined delete-supplier" type="button">
                                {{ __('model.delete', ['model' => trans_choice('model.Supplier', 1)]) }}
                            </button>
                        </div>

                        <hr class="divider xs12">
                    </div>
                @endforeach
            @endif
        </div>

        <div class="xs12">
            <button class="button button--outlined" type="button" id="add-supplier">
                {{ __('model.add', ['model' => trans_choice('model.Supplier', 1)]) }}
            </button>
        </div>
    </div>
</div>

<script>
    const addSupplier = document.getElementById( 'add-supplier' );
    const suppliersContainer = document.getElementById( 'suppliers' );
    let deleteSuppliers = document.getElementsByClassName( 'delete-supplier' );
    let preferredSuppliers = document.getElementsByClassName('radio');
    let supplierSelect = document.querySelectorAll('[id^="supplier_id-"]');

    function deleteSupplier()
    {
        for( let deleteSupplier of deleteSuppliers ) {
            deleteSupplier.addEventListener( 'click', () =>
            {
                let supplier = deleteSupplier.closest( '.supplier' );
                supplier.remove();
            } );
        }
    }

    function changeRadioValue()
    {
        for(let i = 0; i < supplierSelect.length; i++)
        {
            supplierSelect[i].addEventListener('change', () => {
                preferredSuppliers[i].value = supplierSelect[i].value;
            });
        }
    }

    @php
        $suppliers = 0;

        if( old('suppliers') )
        {
            $suppliers = count(old('suppliers'));
        }

        if( $part && $part->suppliers )
        {
            $suppliers = $part->suppliers->count();
        }
    @endphp
    let suppliers = {{ $suppliers }};

    addSupplier.addEventListener( 'click', () =>
    {
        let html = `
            <div class="xs12 md6">
                <div class="text-field text-field--filled">
                    <input id="supplier_id-${suppliers}" type="text" list="suppliers-list" class="text-field__input" name="suppliers[]" required>
                    <label for="supplier_id-${suppliers}" class="text-field__label">{{ trans_choice('model.Supplier', 1) }}*</label>
                </div>
            </div>

            <div class="xs12 md6">
                <div class="text-field text-field--filled">
                    <input id="number-${suppliers}" type="text" class="text-field__input" name="numbers[]" required>
                    <label for="number-${suppliers}" class="text-field__label">{{ __('model.Parts number') }}*</label>
                </div>
            </div>

            <div class="xs12 md6">
                <div class="text-field text-field--filled">
                    <input id="price-${suppliers}" type="number" value="0.00" min="0.00" step="0.01" class="text-field__input" name="nett_prices[]" required>
                    <label for="price-${suppliers}" class="text-field__label">{{ __('model.Nett price') }}*</label>
                </div>
            </div>

            <div class="xs12 md6">
                <div class="text-field text-field--filled">
                    <input id="discount-${suppliers}" type="number" value="0" min="0" max="100" step="1" class="text-field__input" name="discounts[]" required>
                    <label for="discount-${suppliers}" class="text-field__label">{{ __('model.Discount') }}*</label>
                </div>
            </div>

            <div class="xs12 md6">
                <label>
                    <input type="radio" class="radio" value="1" name="preferred">
                    {{ __('model.Preferred') }}
                </label>
            </div>

            <div class="xs12">
                <button class="button button--outlined delete-supplier" type="button">
                    {{ __('model.delete', ['model' => trans_choice('model.Supplier', 1)]) }}
                </button>
            </div>

            <hr class="divider xs12">
        `;

        suppliersContainer.appendChild( document.createElement( 'id' ) );
        let lastChild = suppliersContainer.lastChild;
        lastChild.className = 'row padding--0 supplier';
        lastChild.innerHTML = html;
        suppliers++;

        supplierSelect = document.querySelectorAll('[id^="supplier_id-"]');

        console.log(preferredSuppliers, supplierSelect);

        deleteSupplier();
        changeRadioValue();
    } );

    deleteSupplier();
    changeRadioValue();
</script>
