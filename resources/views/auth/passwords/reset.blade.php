@extends('layouts.auth')

@section('content')
    <div class="card__header primary text--white">{{ __('model.Reset password') }}</div>

        <form method="POST" action="{{ route('password.update') }}">
            <div class="card__text">
                <div class="row padding--0">
                    @csrf

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="xs12">
                        <div class="text-field text-field--filled {{ $errors->has('email') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <input id="email" type="email" class="text-field__input" name="email" value="{{ $email ?? old('email') }}" required autofocus>
                            <label for="email" class="text-field__label">{{ __('model.Email') }}</label>

                            @if ($errors->has('email'))
                                <span class="text-field__helper-text" role="alert">
                                    {{ $errors->first('email') }}
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="xs12">
                        <div class="text-field text-field--filled {{ $errors->has('password') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <input id="password" type="password" class="text-field__input" name="password" required>
                            <label for="password" class="text-field__label">{{ __('model.Password') }}</label>

                            @if ($errors->has('password'))
                                <span class="text-field__helper-text" role="alert">
                                    {{ $errors->first('password') }}
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="xs12">
                        <div class="text-field text-field--filled">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card__actions">
                <button type="submit" class="button button--contained">
                    {{ __('model.Reset password') }}
                </button>

                <a href="{{ route('login') }}" class="button button--outlined">
                    {{ __('model.Login') }}
                </a>
            </div>
        </form>
@endsection
