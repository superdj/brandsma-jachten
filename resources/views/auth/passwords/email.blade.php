@extends('layouts.auth')

@section('content')
    <div class="card__header primary text--white">{{ __('model.Reset password') }}</div>


    @if (session('status'))
        <div class="banner banner--single-line" role="alert">
            <div class="banner__content">
                {{ session('status') }}
            </div>
        </div>
    @endif

    <form method="POST" action="{{ route('password.email') }}">
        <div class="card__text">
            <div class="row padding--0">
                @csrf

                <div class="xs12">
                    <div class="text-field text-field--filled {{ $errors->has('email') ? 'text-field--helper-text text-field--invalid' : '' }}">
                        <input id="email" type="email" class="text-field__input" name="email" value="{{ old('email') }}" required>
                        <label for="email" class="text-field__label">{{ __('model.Email') }}</label>

                        @if ($errors->has('email'))
                            <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('email') }}
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="card__actions">
            <button type="submit" class="button button--contained">
                {{ __('model.Send Password Reset Link') }}
            </button>

            <a href="{{ route('login') }}" class="button button--outlined">
                {{ __('model.Login') }}
            </a>
        </div>
    </form>
@endsection
