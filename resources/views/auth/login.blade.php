@extends('layouts.auth')

@section('content')
    <div class="card__header primary"><div class="card__title text--white">{{ __('model.Login') }}</div></div>

    <form method="POST" action="{{ route('login') }}">
        <div class="card__content">
            <div class="row padding--0">
                @csrf

                <div class="xs12">
                    <div class="text-field text-field--filled {{ $errors->has('email') ? 'text-field--helper-text text-field--invalid' : '' }}">
                        <input id="email" type="email" class="text-field__input" name="email" value="{{ old('email') }}" required autofocus>
                        <label for="email" class="text-field__label">{{ __('model.Email') }}</label>

                        @if ($errors->has('email'))
                            <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('email') }}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="xs12">
                    <div class="text-field text-field--filled {{ $errors->has('password') ? 'text-field--helper-text text-field--invalid' : '' }}">
                        <input id="password" type="password" class="text-field__input" name="password" required>
                        <label for="password" class="text-field__label">{{ __('model.Password') }}</label>

                        @if ($errors->has('password'))
                            <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('password') }}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="xs12">
                    <label>
                        <input type="checkbox" name="remember" class="switch" {{ old('remember') ? 'checked' : '' }}>
                        {{ __('model.Remember me') }}
                    </label>
                </div>
            </div>
        </div>

        <div class="card__actions">
            <button type="submit" class="button button--contained">
                {{ __('model.Login') }}
            </button>

            @if (Route::has('password.request'))
                <a class="button button--contained" href="{{ route('password.request') }}">
                    {{ __('model.Forgot password') }}?
                </a>
            @endif
        </div>
    </form>
@endsection
