@extends('layouts.app')
@section('title', __('model.info', ['name' => $shelf->rack->warehouse->name.' - '.$shelf->rack->name.' - '.$shelf->name, 'model' => trans_choice('model.Shelf', 1)]))

@section('content')
    <div class="row">
        <div class="xs12">
            <a href="{{ route('shelves.index') }}" class="button button--contained button--contained--icon-left">
                <i class="material-icons icon">arrow_back</i>
                {{ __('model.back', ['page' => trans_choice('model.shelf', 2)]) }}
            </a>
        </div>

        <div class="xs12 sm4 md3">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        <i class="material-icons icon">info</i>
                        {{ __('model.General') }}
                    </div>
                </div>

                <div class="card__text">
                    <ul class="list">
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.name', ['model' => trans_choice('model.Shelf', 1)]) }}</span>
                                <span class="list__content__text">{{ $shelf->name }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Rack', 1) }}</span>
                                <span class="list__content__text">{{ $shelf->rack->name }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Warehouse', 1) }}</span>
                                <span class="list__content__text">{{ $shelf->rack->warehouse->name}}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Created by', 1) }}</span>
                                <span class="list__content__text">
                                    @can('show users')
                                        <a href="{{ route('users.show', $shelf->creator->id) }}">
                                            {{ $shelf->creator->first_name }}
                                        </a>
                                    @endcan

                                    @cannot('show users')
                                        {{ $shelf->creator->first_name }}
                                    @endcannot
                                </span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Updated by', 1) }}</span>
                                <span class="list__content__text">
                                    @if($shelf->updater)
                                        @can('show users')
                                            <a href="{{ route('users.show', $shelf->updater->id) }}">
                                            {{ $shelf->updater->first_name }}
                                        </a>
                                        @endcan

                                        @cannot('show users')
                                            {{ $shelf->updater->first_name }}
                                        @endcannot
                                    @endif
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>

                @can('edit shelves')
                    <div class="card__actions">
                        <a href="{{ route('shelves.edit', $shelf->id) }}" class="button button--text">
                            {{ __('model.edit', ['model' => trans_choice('model.Shelf', 1)]) }}
                        </a>
                    </div>
                @endcan
            </div>
        </div>

        <div class="xs12 sm8 md7">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        <i class="material-icons icon">storage</i>
                        {{ __('model.Stock') }}
                    </div>
                </div>

                <div class="card__text">
                    @if($shelf->parts->count() > 0)
                        <table class="data-table data-table--hover">
                            <thead>
                            <tr>
                                <th>{{ trans_choice('model.Part', 1) }}</th>
                                <th>{{ __('model.Stock') }}</th>
                                <th>{{ __('model.Worth') }}</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($shelf->parts as $part)
                                <tr>
                                    <td>
                                        @can('show parts')
                                            <a href="{{ route('parts.show', $part->id) }}">
                                                {{ $part->name }}
                                            </a>
                                        @endcan

                                        @cannot('show parts')
                                            {{ $part->name }}
                                        @endcannot
                                    </td>
                                    <td class="{{ $part->pivot->sum('stock') < $part->minimal_stock ? 'text--red-900 red-50' : 'text--green-900 green-50' }}">
                                        {{ $part->pivot->stock }} / {{ $part->minimal_stock }}
                                        {{ $part->unit->abbreviation }}
                                    </td>
                                    <td>&euro;{{ number_format( $part->pivot->stock * $part->price, 2, ',', ',' ) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        @include('partials.not-found-banner', ['model' => trans_choice('model.part', 2)])
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
