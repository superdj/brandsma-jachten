@extends('layouts.app')
@section('title', trans_choice('model.Shelf', 2))

@section('content')
    <div class="row">
        @include('partials.create-button')

        @include('partials.filter',
            [
                'items' => $shelves,
                'fields' => [
                    'id' => __('model.Id'),
                    'name' => __('model.Name'),
                    'rack_id' => trans_choice('model.Rack', 1),
                    'user_created' => __('model.Creator'),
                    'user_updated' => __('model.Updater'),
                    'created_at' => __('model.Created at'),
                    'updated_at' => __('model.Updated at'),
                ]
            ]
        )

        @include('partials.pagination', ['items' => $shelves])

        <div class="xs12">
            @if($shelves->count() > 0)
                <table class="data-table data-table--hover data-table--responsive">
                    <thead>
                    <tr>
                        <th>{{ __('model.name', ['model' => trans_choice('model.Shelf', 1)]) }}</th>
                        <th>{{ trans_choice('model.Warehouse', 1) }}</th>
                        <th>{{ trans_choice('model.Rack', 1) }}</th>
                        <th class="data-table--numeric">{{ trans_choice('model.Part', 2) }}</th>
                        <th>{{ __('model.Actions') }}</th>
                    </tr>
                    </thead>

                    <tbody>
                        @foreach($shelves as $shelf)
                            <tr>
                                <td>
                                    <a href="{{ route('shelves.show', $shelf->id) }}">
                                        {{ $shelf->name }}
                                    </a>
                                </td>
                                <td>{{ $shelf->rack->warehouse->name }}</td>
                                <td>{{ $shelf->rack->name }}</td>
                                <td class="data-table--numeric">{{ $shelf->parts->count() }}</td>
                                <td>
                                    @include('partials.actions',
                                        [
                                            'item' => $shelf,
                                            'actions' => ['edit', 'show', 'delete']
                                        ]
                                    )
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @else
                @include('partials.not-found-banner', ['model' => trans_choice('model.shelf', 2)])
            @endif
        </div>

        @include('partials.pagination', ['items' => $shelves])
    </div>
@endsection;
