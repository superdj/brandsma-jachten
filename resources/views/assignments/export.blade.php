<table>
    <thead>
    <tr>
        <th>{{ trans_choice('model.Assignment', 1) }}</th>
        <th>{{ trans_choice('model.Customer', 1) }}</th>
        <th>{{ trans_choice('model.Part', 1) }}</th>
        <th>{{ __('model.Parts number') }}</th>
        <th>{{ __('model.Gross price') }}</th>
        <th>{{ __('model.Amount') }}</th>
        <th>{{ __('model.Total') }}</th>
    </tr>
    </thead>

    <tbody>
        @foreach($assignment->parts as $part)
            <tr>
                <td>{{ $assignment->name }}</td>
                <td>{{ $assignment->customer->name }}</td>
                <td>{{ $part->name }}</td>
                <td>
                    @php
                    $number = '';
                    if($part->suppliers)
                    {
                        foreach($part->suppliers as $supplier)
                        {
                            if($supplier->pivot->preferred)
                            {
                                $number = $supplier->pivot->number;
                            }
                        }
                    }
                    @endphp
                    {{ $number }}
                </td>
                <td>&euro;{{ number_format($part->price, 2, ',', '.') }}</td>
                <td>{{ $part->pivot->amount }}</td>
                <td>&euro;{{ number_format($part->price * $part->pivot->amount, 2, ',', '.') }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
