@extends('layouts.app')
@section('title', __('model.edit_title', ['name' => $assignment->name, 'model' => trans_choice('model.Assignment', 1)]))

@section('content')

    <form method="POST" action="{{ route('assignments.update', $assignment->id) }}">
        @method('put')
        @csrf

        <div class="row">
            <div class="xs12">
                <a href="{{ route('assignments.index') }}" class="button button--contained button--contained--icon-left">
                    <i class="material-icons icon">arrow_back</i>
                    {{ __('model.back', ['page' => trans_choice('model.assignment', 2)]) }}
                </a>
            </div>

            <div class="xs12 sm6 md3">
                <div class="row padding--0">
                    <h5 class="xs12">{{ trans_choice('model.Assignment', 1) }}</h5>

                    <div class="xs12">
                        <div class="text-field text-field--filled {{ $errors->has('name') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <input id="name" type="text" class="text-field__input" name="name" value="{{ old('name') ?? $assignment->name }}" required autofocus>
                            <label for="name" class="text-field__label">{{ __('model.name', ['model' => trans_choice('model.Assignment', 1)]) }}*</label>

                            @if ($errors->has('name'))
                                <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('name') }}
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="xs12">
                        <datalist id="customers">
                            @foreach($customers as $customer)
                                <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                            @endforeach
                        </datalist>

                        <div class="text-field text-field--filled {{ $errors->has('customer_id') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <input
                                id="customer_id"
                                type="text"
                                class="text-field__input"
                                list="customers"
                                name="customer_id"
                                value="{{ old('customer_id') ?? $assignment->customer->id }}"
                                required
                            >
                            <label for="customer_id" class="text-field__label">{{ trans_choice('model.Customer', 1) }}*</label>

                            @if ($errors->has('customer_id'))
                                <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('customer_id') }}
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="xs12">
                        <div class="text-field text-field--filled text-field--select {{ $errors->has('users') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <div class="text-field__input"></div>

                            <select id="users" name="users[]" multiple>
                                @foreach($users as $user)
                                    <option
                                        value="{{ $user->id }}"
                                        {{ old('users') && in_array( $user->id, old('users') ) || in_array( $user->id, $assignment->users->pluck('id')->toArray() )  ? 'selected' : '' }}
                                    >
                                        {{ $user->first_name }}
                                    </option>
                                @endforeach
                            </select>

                            <label for="users" class="text-field__label">{{ trans_choice('model.Mechanic', 2) }}*</label>

                            @if ($errors->has('users'))
                                <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('users') }}
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="xs12">
                        <div class="text-field text-field--filled text-field--select {{ $errors->has('tasks') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <div class="text-field__input"></div>

                            <select id="tasks" name="tasks[]" multiple>
                                @foreach($tasks as $task)
                                    <option
                                        value="{{ $task->id }}"
                                        {{ old('tasks') && in_array($task->id, old('tasks')) || in_array( $task->id, $assignment->tasks->pluck('id')->toArray() ) ? 'selected' : '' }}
                                    >
                                        {{ $task->name }}
                                    </option>
                                @endforeach
                            </select>

                            <label for="tasks" class="text-field__label">{{ trans_choice('model.Task', 2) }}*</label>

                            @if ($errors->has('tasks'))
                                <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('tasks') }}
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="xs12">
                        <div class="text-field text-field--filled {{ $errors->has('description') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <textarea id="description" class="text-field__input" name="description">{{ old('description') ?? $assignment->description }}</textarea>
                            <label for="description" class="text-field__label">{{ trans_choice('model.Annotation', 2) }}</label>

                            @if ($errors->has('description'))
                                <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('description') }}
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="xs12">
                        <div class="row padding--0">
                            <div class="xs12 md8">
                                <div class="text-field text-field--filled {{ $errors->has('start_date') ? 'text-field--helper-text text-field--invalid' : '' }}">
                                    <input
                                        id="start_date"
                                        type="date"
                                        class="text-field__input"
                                        name="start_date"
                                        value="{{ old('start_date') ?? explode(' ', $assignment->start_date)[0]}}"
                                        required
                                    >
                                    <label for="start_date" class="text-field__label">{{ __('model.Start date') }}*</label>

                                    @if ($errors->has('start_date'))
                                        <span class="text-field__helper-text" role="alert">
                                        {{ $errors->first('start_date') }}
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="xs12 md4">
                                <div class="text-field text-field--filled {{ $errors->has('start_time') ? 'text-field--helper-text text-field--invalid' : '' }}">
                                    <input
                                        id="start_time"
                                        type="time"
                                        class="text-field__input"
                                        name="start_time"
                                        value="{{ old('start_time') ?? explode(' ', $assignment->start_date)[1] }}"
                                        min="07:00:00"
                                        max="16:30:00"
                                        step="1"
                                        required
                                    >
                                    <label for="start_time" class="text-field__label">{{ __('model.Start time') }}*</label>

                                    @if ($errors->has('start_time'))
                                        <span class="text-field__helper-text" role="alert">
                                        {{ $errors->first('start_time') }}
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="xs12">
                        <div class="row padding--0">
                            <div class="xs12 md8">
                                <div class="text-field text-field--filled {{ $errors->has('end_date') ? 'text-field--helper-text text-field--invalid' : '' }}">
                                    <input
                                        id="end_date"
                                        type="date"
                                        class="text-field__input"
                                        name="end_date"
                                        value="{{ old('end_date') ?? explode(' ', $assignment->end_date)[0] }}"
                                        min="{{ date('Y-m-d') }}"
                                        required
                                    >
                                    <label for="end_date" class="text-field__label">{{ __('model.End date') }}*</label>

                                    @if ($errors->has('end_date'))
                                        <span class="text-field__helper-text" role="alert">
                                        {{ $errors->first('end_date') }}
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="xs12 md4">
                                <div class="text-field text-field--filled {{ $errors->has('end_time') ? 'text-field--helper-text text-field--invalid' : '' }}">
                                    <input
                                        id="end_time"
                                        type="time"
                                        class="text-field__input"
                                        name="end_time"
                                        value="{{ old('end_time') ?? explode(' ', $assignment->end_date)[1] }}"
                                        min="07:00:00"
                                        max="16:30:00"
                                        step="1"
                                        required
                                    >
                                    <label for="end_time" class="text-field__label">{{ __('model.End time') }}*</label>

                                    @if ($errors->has('end_time'))
                                        <span class="text-field__helper-text" role="alert">
                                        {{ $errors->first('end_time') }}
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="xs12">
                        <datalist id="statuses">
                            @foreach($statuses as $status)
                                <option value="{{ $status->id }}">{{ $status->name }}</option>
                            @endforeach
                        </datalist>

                        <div class="text-field text-field--filled {{ $errors->has('status') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <input
                                id="status"
                                type="text"
                                class="text-field__input"
                                list="statuses"
                                name="status"
                                value="{{ old('status') ?? $assignment->status->id }}"
                                required
                            >
                            <label for="status" class="text-field__label">{{ trans_choice('model.Status', 1) }}*</label>

                            @if ($errors->has('status'))
                                <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('status') }}
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="xs12">
                <button type="submit" class="button button--contained button--contained--icon-left">
                    <i class="material-icons icon">save</i>
                    {{ __('model.edit', ['model' => trans_choice('model.Assignment', 1)]) }}
                </button>

                <button type="reset" class="button button--text button--text--icon-left">
                    <i class="material-icons icon">settings_backup_restore</i>
                    {{ __('model.reset') }}
                </button>
            </div>
        </div>
    </form>
@endsection
