@extends('layouts.app')
@section('title', __('model.info', ['name' => $assignment->name, 'model' => trans_choice('model.Assignment', 1)]))

@section('content')
    <div class="row">
        <div class="xs12">
            <a href="{{ route('assignments.index') }}" class="button button--contained button--contained--icon-left">
                <i class="material-icons icon">arrow_back</i>
                {{ __('model.back', ['page' => trans_choice('model.assignment', 2)]) }}
            </a>
        </div>

        <div class="xs12 sm6 md3">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        <i class="material-icons icon">info</i>
                        {{ __('model.General') }}
                    </div>
                </div>

                <div class="card__text">
                    <ul class="list">
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.name', ['model' => trans_choice('model.Assignment', 1)]) }}</span>
                                <span class="list__content__text">{{ $assignment->name }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Annotation', 2) }}</span>
                                <span class="list__content__text">{{ $assignment->description }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.Start date') }}</span>
                                <span class="list__content__text">{{ $assignment->start_date->format('d-m-Y H:i:s') }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.End date') }}</span>
                                <span class="list__content__text">{{ $assignment->end_date->format('d-m-Y H:i:s') }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.Deadline') }}</span>
                                <span class="list__content__text">{{ Carbon\Carbon::now()->diffForHumans( $assignment->end_date ) }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Customer', 1) }}</span>
                                <span class="list__content__text">
                                    @can('show customers')
                                        <a href="{{ route('customers.show', $assignment->customer->id) }}">
                                            {{ $assignment->customer->name }}
                                        </a>
                                    @endcan

                                    @cannot('show customers')
                                        {{ $assignment->customer->name }}
                                    @endcan
                                </span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Status', 1) }}</span>
                                <span class="list__content__text">{{ $assignment->status->name }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Mechanic', 2) }}</span>
                                <span class="list__content__text">
                                    @foreach($assignment->users as $user)
                                        @can('show users')
                                            <a href="{{ route('users.show', $user->id) }}">
                                                {{ $user->first_name }}
                                            </a>
                                        @endcan

                                        @cannot('show users')
                                            {{ $user->first_name }}
                                        @endcan

                                        {{ $loop->last ? '' : ',' }}
                                    @endforeach
                                </span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Created by', 1) }}</span>
                                <span class="list__content__text">
                                    @can('show users')
                                        <a href="{{ route('users.show', $assignment->creator->id) }}">
                                            {{ $assignment->creator->first_name }}
                                        </a>
                                    @endcan

                                    @cannot('show users')
                                        {{ $assignment->creator->first_name }}
                                    @endcannot
                                </span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Updated by', 1) }}</span>
                                <span class="list__content__text">
                                    @if($assignment->updater)
                                        @can('show users')
                                            <a href="{{ route('users.show', $assignment->updater->id) }}">
                                            {{ $assignment->updater->first_name }}
                                        </a>
                                        @endcan

                                        @cannot('show users')
                                            {{ $assignment->updater->first_name }}
                                        @endcannot
                                    @endif
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>

                @can('edit assignments')
                    @if(!$assignment->status->lock)
                        <div class="card__actions">
                            <a href="{{ route('assignments.edit', $assignment->id) }}" class="button button--text">
                                {{ __('model.edit', ['model' => trans_choice('model.Assignment', 1)]) }}
                            </a>
                        </div>
                    @endif
                @endcan
            </div>
        </div>

        <div class="xs12 sm6 md5 lg4">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        @can('view tasks')
                            <a href="{{ route('tasks.index') }}">
                                <i class="material-icons icon">assignment</i>
                                {{ trans_choice('model.Task', 2) }}
                            </a>
                        @endcan

                        @cannot('view tasks')
                            <i class="material-icons icon">assignment</i>
                            {{ trans_choice('model.Task', 2) }}
                        @endcannot
                    </div>
                </div>

                <div class="card__text">
                    @if($assignment->tasks->count() > 0)
                        <table class="data-table data-table--hover">
                            <thead>
                            <tr>
                                <th>{{ trans_choice('model.Task', 1) }}</th>
                                <th>{{ trans_choice('model.Status', 1) }}</th>
                            </tr>
                            </thead>

                            <tbody>
                            @php
                                $tasks_done = [];

                                foreach($assignment->time_registrations as $registration)
                                {
                                    foreach($registration->tasks as $tasks)
                                    {
                                        $tasks_done[] = $tasks->id;
                                    }
                                }
                            @endphp

                            @foreach($assignment->tasks as $task)
                                <tr>
                                    <td>{{ $task->name }}</td>
                                    <td>
                                        @if(in_array($task->id, $tasks_done))
                                            <i class="material-icons icon text--green">check_box</i>
                                        @else
                                            <i class="material-icons icon text--red">check_box_outline_blank</i>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        @include('partials.not-found-banner', ['model' => trans_choice('model.task', 2)])
                    @endif
                </div>
            </div>
        </div>

        <div class="xs12 sm6 md3">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        @can('view time-registrations')
                            <a href="{{ route('time-registrations.index') }}">
                                <i class="material-icons icon">alarm</i>
                                {{ trans_choice('model.Time registration', 1) }}
                            </a>
                        @endcan

                        @cannot('view time-registrations')
                            <i class="material-icons icon">alarm</i>
                            {{ trans_choice('model.Time registration', 1) }}
                        @endcannot
                    </div>
                </div>

                <div class="card__text">
                    @if($assignment->time_registrations->count() > 0)
                        <table class="data-table data-table--hover">
                            <thead>
                            <tr>
                                <th>{{ trans_choice('model.Mechanic', 1) }}</th>
                                <th>{{ __('model.Time') }}</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($assignment->time_registrations as $time_registration)
                                <tr>
                                    <td>
                                        <a href="{{ route('users.show', $time_registration->user->id) }}">
                                            {{ $time_registration->user->first_name }}
                                        </a>
                                    </td>
                                    <td>{{ $time_registration->time }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        @include('partials.not-found-banner', ['model' => trans_choice('model.time registration', 2)])
                    @endif
                </div>
            </div>
        </div>

        <div class="xs12 sm6 md4 lg3">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        @can('view used-parts')
                            <a href="{{ route('used-parts.index') }}">
                                <i class="material-icons icon">build</i>
                                {{ trans_choice('model.Used part', 2) }}
                            </a>
                        @endcan

                        @cannot('view used-parts')
                            <i class="material-icons icon">build</i>
                            {{ trans_choice('model.Used part', 2) }}
                        @endcannot
                    </div>
                </div>

                @can('export assignments')
                    @if($assignment->parts->count() > 20 )
                        <div class="card__actions">
                            <a href="{{ route('assignment.export', $assignment->id) }}" class="button button--text">
                                {{ __('model.export', ['model' => trans_choice('model.used part', 2)]) }}
                            </a>
                        </div>
                    @endif
                @endcan

                <div class="card__text">
                    @if($assignment->parts->count() > 0)
                        <table class="data-table data-table--hover">
                            <thead>
                            <tr>
                                <th>{{ trans_choice('model.Part', 1) }}</th>
                                <th>{{ __('model.Amount') }}</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($assignment->parts as $part)
                                <tr>
                                    <td>
                                        @can('show parts')
                                            <a href="{{ route('parts.show', $part->id) }}">
                                                {{ $part->name }}
                                            </a>
                                        @endcan

                                        @cannot('show parts')
                                            {{ $part->name }}
                                        @endcan
                                    </td>
                                    <td>
                                        {{ $part->pivot->amount }}
                                        {{ $part->unit->abbreviation }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="banner banner--single-line">
                            <div class="banner__content">
                                {{ __('model.not_found', ['model' => trans_choice('model.used part', 2)]) }}
                            </div>
                        </div>
                    @endif
                </div>

                @can('export assignments')
                    @if($assignment->parts->count() > 0)
                        <div class="card__actions">
                            <a href="{{ route('assignment.export', $assignment->id) }}" class="button button--text">
                                {{ __('model.export', ['model' => trans_choice('model.used part', 2)]) }}
                            </a>
                        </div>
                    @endif
                @endcan
            </div>
        </div>
    </div>
@endsection
