@extends('layouts.app')
@section('title', trans_choice('model.Assignment', 2))

@section('content')
    <div class="row">
        @include('partials.create-button')

        @include('partials.filter',
             [
                 'items' => $assignments,
                 'fields' => [
                    'id' => __('model.Id'),
                    'name' => __('model.Name'),
                    'customer_id' => trans_choice('model.Customer', 1),
                    'status_id' => trans_choice('Status', 1),
                    'start_date' => __('model.Start date'),
                    'end_date' => __('model.End date'),
                    'user_created' => __('model.Creator'),
                    'user_updated' => __('model.Updater'),
                    'created_at' => __('model.Created at'),
                    'updated_at' => __('model.Updated at'),
                 ]
             ]
        )

        @include('partials.pagination', ['items' => $assignments])

        <div class="xs12">
            @if($assignments->count() > 0)
                <table class="data-table data-table--hover data-table--responsive">
                    <thead>
                    <tr>
                        <th>{{ __('model.name', ['model' => trans_choice('model.Assignment', 1)]) }}</th>
                        <th>{{ trans_choice('model.Customer', 1) }}</th>
                        <th>{{ __('model.Deadline') }}</th>
                        <th class="data-table--numeric">{{ trans_choice('model.Task', 2) }}</th>
                        <th>{{ trans_choice('model.Status', 1) }}</th>
                        <th>{{ __('model.Actions') }}</th>
                    </tr>
                    </thead>

                    <tbody>
                        @foreach($assignments as $assignment)
                            @php
                                $tasks_done = [];

                                foreach($assignment->time_registrations as $registration)
                                {
                                    foreach($registration->tasks as $task)
                                    {
                                        $tasks_done[] = $task->id;
                                    }
                                }
                            @endphp
                            <tr>
                                <td>
                                    @can('show assignments')
                                        <a href="{{ route('assignments.show', $assignment->id) }}">
                                            {{ $assignment->name }}
                                        </a>
                                    @endcan

                                    @cannot('show assignments')
                                        {{ $assignment->name }}
                                    @endcan
                                </td>
                                <td>
                                    @can('show customers')
                                        <a href="{{ route('customers.show', $assignment->customer->id) }}">
                                            {{ $assignment->customer->name }}
                                        </a>
                                    @endcan

                                    @cannot('show customers')
                                        {{ $assignment->customer->name }}
                                    @endcan
                                </td>
                                <td class="{{ $assignment->end_date->isPast() ? 'text--red-900 red-50' : 'text--green-900 green-50' }}">
                                    {{ Carbon\Carbon::now()->diffForHumans( $assignment->end_date ) }}
                                </td>
                                <td class="data-table--numeric {{ count($tasks_done) === $assignment->tasks->count() ? 'text--green-900 green-50' : 'text--red-900 red-50' }}">
                                    {{ count($tasks_done) }}/ {{ $assignment->tasks->count() }}
                                </td>
                                <td>{{ $assignment->status->name }}</td>
                                <td>
                                    @include('partials.actions',
                                        [
                                            'item' => $assignment,
                                            'actions' => ['edit', 'show', 'time-registration', 'delete']
                                        ]
                                    )
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @else
                @include('partials.not-found-banner', ['model' => trans_choice('model.assignment', 2)])
            @endif
        </div>

        @include('partials.pagination', ['items' => $assignments])
    </div>
@endsection;
