@extends('layouts.app')
@section('title', trans_choice('model.Supplier', 2))

@section('content')
    <div class="row">
        @include('partials.create-button')

        @include('partials.filter',
            [
                'items' => $suppliers,
                'fields' => [
                    'id' => __('model.Id'),
                    'name' => __('model.Name'),
                    'city' => __('model.City'),
                    'postal_code' => __('model.Postal code'),
                    'street' => __('model.Street'),
                    'house_number' => __('model.House number'),
                    'email' => __('model.Email'),
                    'phone_number' => __('model.Phone number'),
                    'mobile_number' => __('model.Mobile number'),
                    'website' => __('model.Website'),
                    'user_created' => __('model.Creator'),
                    'user_updated' => __('model.Updater'),
                    'created_at' => __('model.Created at'),
                    'updated_at' => __('model.Updated at'),
                ]
            ]
        )

        @include('partials.pagination', ['items' => $suppliers])

        <div class="xs12">
            @if($suppliers->count() > 0)
                <table class="data-table data-table--hover data-table--responsive">
                    <thead>
                    <tr>
                        <th>{{ __('model.name', ['model' => trans_choice('model.Supplier', 1)]) }}</th>
                        <th>{{ __('model.Address') }}</th>
                        <th>{{ __('model.Phone number') }}</th>
                        <th>{{ __('model.Mobile number') }}</th>
                        <th>{{ __('model.Email') }}</th>
                        <th>{{ __('model.Actions') }}</th>
                    </tr>
                    </thead>

                    <tbody>
                        @foreach($suppliers as $supplier)
                            <tr>
                                <td>
                                    @can('show suppliers')
                                        <a href="{{ route('suppliers.show', $supplier->id) }}">
                                            {{ $supplier->name }}
                                        </a>
                                    @endcan

                                    @cannot('show suppliers')
                                        {{ $supplier->name }}
                                    @endcannot
                                </td>
                                <td>
                                    <a
                                        href="https://www.google.com/maps/search/?api=1&query={{ str_replace( ' ', '+', $supplier->city.'+'.$supplier->postal_code.'+'.$supplier->street.'+'.$supplier->house_number ) }}"
                                        target="_blank"
                                    >
                                        {{ $supplier->street }}
                                        {{ $supplier->house_number }},
                                        {{ $supplier->postal_code }}
                                        {{ $supplier->city }}
                                    </a>
                                </td>
                                <td>
                                    <a href="tel:{{ $supplier->phone_number }}">
                                        {{ $supplier->phone_number }}
                                    </a>
                                </td>
                                <td>
                                    <a href="tel:{{ $supplier->mobile_number }}">
                                        {{ $supplier->mobile_number }}
                                    </a>
                                </td>
                                <td>
                                    <a href="mailto:{{ $supplier->email }}">
                                        {{ $supplier->email }}
                                    </a>
                                </td>
                                <td>
                                    @include('partials.actions',
                                        [
                                            'item' => $supplier,
                                            'actions' => ['edit', 'show', 'delete']
                                        ]
                                    )
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @else
                @include('partials.not-found-banner', ['model' => trans_choice('model.supplier', 2)])
            @endif
        </div>

        @include('partials.pagination', ['items' => $suppliers])
    </div>
@endsection;
