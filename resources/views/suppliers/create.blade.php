@extends('layouts.app')
@section('title', __('model.add', ['model' => trans_choice('model.Supplier', 1)]))

@section('content')

<form method="POST" action="{{ route('suppliers.store') }}">
    @csrf
    <div class="row">
        <div class="xs12">
            <a href="{{ route('suppliers.index') }}" class="button button--contained button--contained--icon-left">
                <i class="material-icons icon">arrow_back</i>
                {{ __('model.back', ['page' => trans_choice('model.supplier', 2)]) }}
            </a>
        </div>

        <div class="xs12 sm6 md3">
             <div class="row padding--0">
                 <h5 class="xs12">{{ trans_choice('model.Supplier', 1) }}</h5>

                <div class="xs12">
                    <div class="text-field text-field--filled {{ $errors->has('name') ? 'text-field--helper-text text-field--invalid' : '' }}">
                        <input id="name" type="text" class="text-field__input" name="name" value="{{ old('name') }}" required autofocus>
                        <label for="name" class="text-field__label">{{ __('model.name', ['model' => trans_choice('model.Supplier', 1)]) }}*</label>

                        @if ($errors->has('name'))
                            <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('name') }}
                            </span>
                        @endif
                    </div>
                </div>

                 <div class="xs12">
                     <div class="text-field text-field--filled {{ $errors->has('number') ? 'text-field--helper-text text-field--invalid' : '' }}">
                         <input id="number" type="text" class="text-field__input" name="number" value="{{ old('number') }}" required>
                         <label for="number" class="text-field__label">{{ __('model.Suppliers number') }}*</label>

                         @if ($errors->has('number'))
                             <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('number') }}
                            </span>
                         @endif
                     </div>
                 </div>

                 <div class="xs12 md7">
                     <div class="text-field text-field--filled {{ $errors->has('street') ? 'text-field--helper-text text-field--invalid' : '' }}">
                         <input id="street" type="text" class="text-field__input" name="street" value="{{ old('street') }}">
                         <label for="street" class="text-field__label">{{ __('model.Street') }}</label>

                         @if ($errors->has('street'))
                             <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('street') }}
                            </span>
                         @endif
                     </div>
                 </div>

                 <div class="xs12 md5">
                     <div class="text-field text-field--filled {{ $errors->has('house_number') ? 'text-field--helper-text text-field--invalid' : '' }}">
                         <input id="house_number" type="text" class="text-field__input" name="house_number" value="{{ old('house_number') }}">
                         <label for="house_number" class="text-field__label">{{ __('model.House number') }}</label>

                         @if ($errors->has('house_number'))
                             <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('house_number') }}
                            </span>
                         @endif
                     </div>
                 </div>

                 <div class="xs12 md5">
                     <div class="text-field text-field--filled {{ $errors->has('postal_code') ? 'text-field--helper-text text-field--invalid' : '' }}">
                         <input id="postal_code" type="text" class="text-field__input" name="postal_code" value="{{ old('postal_code') }}">
                         <label for="postal_code" class="text-field__label">{{ __('model.Postal code') }}</label>

                         @if ($errors->has('postal_code'))
                             <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('postal_code') }}
                            </span>
                         @endif
                     </div>
                 </div>

                 <div class="xs12 md7">
                     <div class="text-field text-field--filled {{ $errors->has('city') ? 'text-field--helper-text text-field--invalid' : '' }}">
                         <input id="city" type="text" class="text-field__input" name="city" value="{{ old('city') }}">
                         <label for="city" class="text-field__label">{{ __('model.City') }}</label>

                         @if ($errors->has('city'))
                             <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('city') }}
                            </span>
                         @endif
                     </div>
                 </div>

                 <div class="xs12 md6">
                     <div class="text-field text-field--filled {{ $errors->has('phone_number') ? 'text-field--helper-text text-field--invalid' : '' }}">
                         <input id="phone_number" type="text" class="text-field__input" name="phone_number" value="{{ old('phone_number') }}" max="13">
                         <label for="phone_number" class="text-field__label">{{ __('model.Phone number') }}</label>

                         @if ($errors->has('phone_number'))
                             <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('phone_number') }}
                            </span>
                         @endif
                     </div>
                 </div>

                 <div class="xs12 md6">
                     <div class="text-field text-field--filled {{ $errors->has('mobile_number') ? 'text-field--helper-text text-field--invalid' : '' }}">
                         <input id="mobile_number" type="text" class="text-field__input" name="mobile_number" value="{{ old('mobile_number') }}" max="13">
                         <label for="mobile_number" class="text-field__label">{{ __('model.Mobile number') }}</label>

                         @if ($errors->has('mobile_number'))
                             <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('mobile_number') }}
                            </span>
                         @endif
                     </div>
                 </div>

                 <div class="xs12">
                     <div class="text-field text-field--filled {{ $errors->has('website') ? 'text-field--helper-text text-field--invalid' : '' }}">
                         <input id="website" type="url" class="text-field__input" name="website" value="{{ old('website') }}">
                         <label for="website" class="text-field__label">{{ __('model.Website') }}</label>

                         @if ($errors->has('website'))
                             <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('website') }}
                            </span>
                         @endif
                     </div>
                 </div>

                 <div class="xs12">
                     <div class="text-field text-field--filled {{ $errors->has('email') ? 'text-field--helper-text text-field--invalid' : '' }}">
                         <input id="email" type="email" class="text-field__input" name="email" value="{{ old('email') }}">
                         <label for="email" class="text-field__label">{{ __('model.Email') }}</label>

                         @if ($errors->has('email'))
                             <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('email') }}
                            </span>
                         @endif
                     </div>
                 </div>
             </div>
        </div>

        <div class="xs12">
            <button type="submit" class="button button--contained button--contained--icon-left">
                <i class="material-icons icon">save</i>
                {{ __('model.add', ['model' => trans_choice('model.Supplier', 1)]) }}
            </button>

            <button type="reset" class="button button--text button--text--icon-left">
                <i class="material-icons icon">settings_backup_restore</i>
                {{ __('model.reset') }}
            </button>
        </div>
    </div>
</form>
@endsection
