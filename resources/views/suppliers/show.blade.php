@extends('layouts.app')
@section('title', __('model.info', ['model' => trans_choice('model.Supplier', 1), 'name' => $supplier->name]))

@section('content')
    <div class="row">
        <div class="xs12">
            <a href="{{ route('suppliers.index') }}" class="button button--contained button--contained--icon-left">
                <i class="material-icons icon">arrow_back</i>
                {{ __('model.back', ['page' => trans_choice('model.supplier', 2)]) }}
            </a>
        </div>

        <div class="xs12 sm6 md3">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        <i class="material-icons icon">info</i>
                        {{ __('model.General') }}
                    </div>
                </div>

                <div class="card__text">
                    <ul class="list">
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.name', ['model' => trans_choice('model.Supplier', 1)]) }}</span>
                                <span class="list__content__text">
                                    <a href="{{ $supplier->website }}" target="_blank">
                                        {{ $supplier->name }}
                                    </a>
                                </span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.Suppliers number') }}</span>
                                <span class="list__content__text">{{ $supplier->number }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.Email') }}</span>
                                <span class="list__content__text">
                                    <a href="mailto:{{ $supplier->email }}">
                                        {{ $supplier->email }}
                                    </a>
                                </span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.Phone number') }}</span>
                                <span class="list__content__text">
                                    <a href="tel:{{ $supplier->phone_number }}">
                                        {{ $supplier->phone_number }}
                                    </a>
                                </span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.Mobile number') }}</span>
                                <span class="list__content__text">
                                    <a href="tel:{{ $supplier->mobile_number }}">
                                        {{ $supplier->mobile_number }}
                                    </a>
                                </span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.Address') }}</span>
                                <span class="list__content__text">
                                    <a
                                        href="https://www.google.com/maps/search/?api=1&query={{ str_replace( ' ', '+', $supplier->city.'+'.$supplier->postal_code.'+'.$supplier->street.'+'.$supplier->house_number ) }}"
                                        target="_blank"
                                    >
                                        {{ $supplier->street }}
                                        {{ $supplier->house_number }},
                                        {{ $supplier->postal_code }}
                                        {{ $supplier->city }}
                                    </a>
                                </span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Created by', 1) }}</span>
                                <span class="list__content__text">
                                    @can('show users')
                                        <a href="{{ route('users.show', $supplier->creator->id) }}">
                                            {{ $supplier->creator->first_name }}
                                        </a>
                                    @endcan

                                    @cannot('show users')
                                        {{ $supplier->creator->first_name }}
                                    @endcannot
                                </span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Updated by', 1) }}</span>
                                <span class="list__content__text">
                                    @if($supplier->updater)
                                        @can('show users')
                                            <a href="{{ route('users.show', $supplier->updater->id) }}">
                                            {{ $supplier->updater->first_name }}
                                        </a>
                                        @endcan

                                        @cannot('show users')
                                            {{ $supplier->updater->first_name }}
                                        @endcannot
                                    @endif
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>

                @can('edit suppliers')
                    <div class="card__actions">
                        <a href="{{ route('suppliers.edit', $supplier->id) }}" class="button button--text">
                            {{ __('model.edit', ['model' => trans_choice('model.Supplier', 1)]) }}
                        </a>
                    </div>
                @endcan
            </div>
        </div>

        <div class="xs12 sm6 md7">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        @can('view parts')
                            <a href="{{ route('parts.index') }}">
                                <i class="material-icons icon">build</i>
                                {{ trans_choice('model.Part', 2) }}
                            </a>
                        @endcan

                        @cannot('view parts')
                            <i class="material-icons icon">build</i>
                            {{ trans_choice('model.Part', 2) }}
                        @endcannot
                    </div>
                </div>

                <div class="card__text">
                    @if( $supplier->parts->count() > 0 )
                        <table class="data-table data-table--hover">
                            <thead>
                            <tr>
                                <th>{{ trans_choice('model.Part', 1) }}</th>
                                <th>{{ __('model.Parts number') }}</th>
                                <th>{{ __('model.Gross price') }}</th>
                                <th>{{ __('model.Nett price') }}</th>
                                <th>{{ __('model.Discount') }}</th>
                                <th>{{ __('model.Stock') }}</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($supplier->parts as $part)
                                <tr>
                                    <td>
                                        @can('show parts')
                                            <a href="{{ route('parts.show', $part->id) }}">
                                                {{ $part->name }}
                                            </a>
                                        @endcan

                                        @cannot('show parts')
                                            {{ $part->name }}
                                        @endcannot
                                    </td>
                                    <td>{{ $part->pivot->number }}</td>
                                    <td>&euro;{{ number_format($part->price, 2, ',', '.') }}</td>
                                    <td>&euro;{{ number_format($part->pivot->nett_price, 2, ',', '.') }}</td>
                                    <td>{{ $part->pivot->discount }}%</td>

                                    @php
                                        $stock = 0;
                                        for($i = 0; $i < count($part->shelves); $i++)
                                        {
                                            $stock += $part->shelves[$i]->pivot->stock;
                                        }
                                    @endphp

                                    <td class="{{ $stock < $part->minimal_stock ? 'text--red-900 red-50' : 'text--green-900 green-50' }}">
                                        {{  $stock }} /
                                        {{ $part->minimal_stock }} {{ $part->unit->abbreviation }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        @include('partials.not-found-banner', ['model' => trans_choice('model.part', 2)])
                    @endif
                </div>

                <div class="card__actions">
                    <button type="button" class="button button--text" data-trigger="add-parts">
                        {{ __('model.add', ['model' => trans_choice('model.Part', 2)]) }}
                    </button>

                    <button type="button" type="button" class="button button--text">{{ __('model.export', ['model' => trans_choice('model.part', 2)] ) }}</button>

                    <div class="dialog" id="add-parts">
                        <form action="" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="dialog__header">{{ __('model.add', ['model' => trans_choice('model.Part', 2)]) }}</div>

                            <div class="dialog__content">
                                <div class="row padding--0">
                                    <div class="xs12">
                                        <input type="file" name="parts" accept=".xlsx, .xls, .csv">
                                    </div>
                                </div>
                            </div>

                            <div class="dialog__actions">
                                <button type="button" class="button button--text" data-trigger="add-parts">
                                    {{ __('model.Cancel') }}
                                </button>

                                <button type="submit" class="button button--text">
                                    {{ __('model.add', ['model' => trans_choice('model.Part', 2)]) }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
