@extends('layouts.app')
@section('title', trans_choice('model.Unit', 2))

@section('content')
    <div class="row">
        @include('partials.create-button')

        @include('partials.filter',
             [
                 'items' => $units,
                 'fields' => [
                    'id' => __('model.Id'),
                    'name' => __('model.Name'),
                    'abbreviation' => __('model.Abbreviation'),
                    'user_created' => __('model.Creator'),
                    'user_updated' => __('model.Updater'),
                    'created_at' => __('model.Created at'),
                    'updated_at' => __('model.Updated at'),
                 ]
             ]
        )

        @include('partials.pagination', ['items' => $units])

        <div class="xs12">
            @if($units->count() > 0)
                <table class="data-table data-table--hover data-table--responsive">
                    <thead>
                    <tr>
                        <th>{{ __('model.name', ['model' => trans_choice('model.Unit', 1)]) }}</th>
                        <th>{{ __('model.Abbreviation') }}</th>
                        <th class="data-table--numeric">{{ trans_choice('model.Part', 2) }}</th>
                        <th>{{ __('model.Actions') }}</th>
                    </tr>
                    </thead>

                    <tbody>
                        @foreach($units as $unit)
                            <tr>
                                <td>
                                    @can('show units')
                                        <a href="{{ route('units.show', $unit->id) }}">
                                            {{ $unit->name }}
                                        </a>
                                    @endcan

                                    @cannot('show units')
                                        {{ $unit->name }}
                                    @endcannot
                                </td>
                                <td>
                                    @can('show units')
                                        <a href="{{ route('units.show', $unit->id) }}">
                                            {{ $unit->abbreviation }}
                                        </a>
                                    @endcan

                                    @cannot('show units')
                                        {{ $unit->abbreviation }}
                                    @endcannot
                                </td>
                                <td class="data-table--numeric">{{ $unit->parts->count() }}</td>
                                <td>
                                    @include('partials.actions',
                                        [
                                            'item' => $unit,
                                            'actions' => ['edit', 'show', 'delete']
                                        ]
                                    )
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @else
                @include('partials.not-found-banner', ['model' => trans_choice('model.unit', 2)])
            @endif
        </div>

        @include('partials.pagination', ['items' => $units])
    </div>
@endsection;
