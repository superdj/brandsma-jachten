@extends('layouts.app')
@section('title', __('model.info', ['model' => trans_choice('model.Unit', 1), 'name' => $unit->name]))

@section('content')
    <div class="row">
        <div class="xs12">
            <a href="{{ route('units.index') }}" class="button button--contained button--contained--icon-left">
                <i class="material-icons icon">arrow_back</i>
                {{ __('model.back', ['page' => trans_choice('model.unit', 2)]) }}
            </a>
        </div>

        <div class="xs12 sm6 md3">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        <i class="material-icons icon">info</i>
                        {{ __('model.General') }}
                    </div>
                </div>

                <div class="card__text">
                    <ul class="list">
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.name', ['model' => trans_choice('model.Unit', 1)]) }}</span>
                                <span class="list__content__text">{{ $unit->name }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.Abbreviation') }}</span>
                                <span class="list__content__text">{{ $unit->abbreviation }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Created by', 1) }}</span>
                                <span class="list__content__text">
                                    @can('show users')
                                        <a href="{{ route('users.show', $unit->creator->id) }}">
                                            {{ $unit->creator->first_name }}
                                        </a>
                                    @endcan

                                    @cannot('show users')
                                        {{ $unit->creator->first_name }}
                                    @endcannot
                                </span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Updated by', 1) }}</span>
                                <span class="list__content__text">
                                    @if($unit->updater)
                                        @can('show users')
                                            <a href="{{ route('users.show', $unit->updater->id) }}">
                                            {{ $unit->updater->first_name }}
                                        </a>
                                        @endcan

                                        @cannot('show users')
                                            {{ $unit->updater->first_name }}
                                        @endcannot
                                    @endif
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>

                @can('edit units')
                    <div class="card__actions">
                        <a href="{{ route('units.edit', $unit->id) }}" class="button button--text">
                            {{ __('model.edit', ['model' => trans_choice('model.Unit', 1)]) }}
                        </a>
                    </div>
                @endcan
            </div>
        </div>

        <div class="xs12 md7 lg5">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        @can('view parts')
                            <a href="{{ route('parts.index') }}">
                                <i class="material-icons icon">build</i>
                                {{ trans_choice('model.Part', 2) }}
                            </a>
                        @endcan

                        @cannot('view parts')
                            <i class="material-icons icon">build</i>
                            {{ trans_choice('model.Part', 2) }}
                        @endcannot
                    </div>
                </div>

                <div class="card__text">
                    @if( $unit->parts->count() > 0 )
                        <table class="data-table data-table--hover">
                            <thead>
                            <tr>
                                <th>{{ trans_choice('model.Part', 1) }}</th>
                                <th>{{ __('model.Gross price') }}</th>
                                <th class="data-table--numeric">{{ __('model.Stock') }}</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($unit->parts as $part)
                                <tr>
                                    <td>
                                        @can('show parts')
                                            <a href="{{ route('parts.show', $part->id) }}">
                                                {{ $part->name }}
                                            </a>
                                        @endcan

                                        @cannot('show parts')
                                            {{ $part->name }}
                                        @endcannot
                                    </td>
                                    <td>&euro;{{ number_format($part->price, 2, ',', '.') }}</td>

                                    @php
                                        $stock = 0;
                                        for($i = 0; $i < count($part->shelves); $i++)
                                        {
                                            $stock += $part->shelves[$i]->pivot->stock;
                                        }
                                    @endphp

                                    <td class="data-table--numeric {{ $stock < $part->minimal_stock ? 'red-50 text--red-900' : 'green-50 text--green-900' }}">
                                        {{  $stock }} /
                                        {{ $part->minimal_stock }} {{ $part->unit->abbreviation }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        @include('partials.not-found-banner', ['model' => trans_choice('model.part', 2)])
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
