<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1, user-scalable=no">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link href="//fonts.googleapis.com/css?family=Lato|Roboto+Slab" rel="stylesheet">
        <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/auth.css') }}" rel="stylesheet">

        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('storage/favicon.ico') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('storage/apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('storage/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('storage/favicon-16x16.png') }}">
        <link rel="mask-icon" href="{{ asset('storage/safari-pinned-tab.svg') }}" color="#232B54">
        <meta name="msapplication-TileColor" content="#232B54">
        <meta name="theme-color" content="#232B54">
        <meta name="msapplication-navbutton-color" content="#232B54">
        <meta name="apple-mobile-web-app-status-bar-style" content="#232B54">
    </head>

    <body>
        <main class="container">
            <div class="row">
                <div class="sm-down-hide sm2 lg4"></div><!-- Push div to center -->

                <div class="xs12 sm8 lg5 xl4">
                    <img src="{{ asset('storage/logo-brandsma2.svg') }}" alt="logo">
                </div>
            </div>

            <div class="row">
                <div class="sm-down-hide sm2 lg4"></div><!-- Push div to center -->

                <div class="xs12 sm8 lg5 xl4">
                    <div class="card">
                        @yield('content')
                    </div>
                </div>
            </div>
        </main>

        <script src="{{ asset('js/main.js') }}" defer></script>
    </body>
</html>
