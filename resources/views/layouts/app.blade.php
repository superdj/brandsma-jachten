<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1, user-scalable=no">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title') | {{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link href="//fonts.googleapis.com/css?family=Lato:400,700|Roboto+Slab" rel="stylesheet">
        <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!-- Styles -->
        @yield('css')
        <link href="{{ asset('css/stylesheet.css') }}" rel="stylesheet">

        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('storage/favicon.ico') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('storage/apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('storage/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('storage/favicon-16x16.png') }}">
        <link rel="mask-icon" href="{{ asset('storage/safari-pinned-tab.svg') }}" color="#232B54">
        <meta name="msapplication-TileColor" content="#232B54">
        <meta name="theme-color" content="#232B54">
        <meta name="msapplication-navbutton-color" content="#232B54">
        <meta name="apple-mobile-web-app-status-bar-style" content="#232B54">
    </head>

    <body>
        @include('partials.header')

        <main class="container container--fluid">
            @if(session('message'))
                <div class="row">
                    <div class="xs12">
                        <div class="snackbar snackbar--single-line {{ session('success') ? 'green-50' : 'red-50' }}">
                            <div class="snackbar__content {{ session('success') ? 'text--green-900' : 'text--red-900' }}">{{ session('message') }}</div>
                        </div>
                    </div>
                </div>
            @endif

            @if($errors->any())
                <div class="row">
                    <div class="xs12 md3">
                        <ul class="list red-50">
                            @foreach($errors->all() as $error)
                                <li class="list--single-line">
                            <span class="list__content text--red-900">
                                {{ $error }}
                            </span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif

            @yield('content')
        </main>

        @include('partials.menu')

        @yield('js')
        <script src="{{ asset('js/main.js') }}" defer></script>
    </body>
</html>
