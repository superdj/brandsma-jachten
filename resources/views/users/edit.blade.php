@extends('layouts.app')
@section('title', __('model.edit_title', ['name' => $user->first_name.' '.$user->last_name, 'model' => trans_choice('model.user', 1)]))

@section('content')

    <form method="POST" action="{{ route('users.update', $user->id) }}">
        @method('put')
        @csrf

        <div class="row">
            <div class="xs12">
                <a href="{{ route('users.index') }}" class="button button--contained button--contained--icon-left">
                    <i class="material-icons icon">arrow_back</i>
                    {{ __('model.back', ['page' => trans_choice('model.user', 2)]) }}
                </a>
            </div>

            <div class="xs12 sm6 md3">
                <div class="row padding--0">
                    <h5 class="xs12">{{ trans_choice('model.User', 1) }}</h5>

                    <div class="xs12">
                        <div class="text-field text-field--filled {{ $errors->has('first_name') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <input id="first_name" type="text" class="text-field__input" name="first_name" value="{{ old('first_name') ?? $user->first_name }}" required autofocus>
                            <label for="first_name" class="text-field__label">{{ __('model.First name') }}*</label>

                            @if ($errors->has('first_name'))
                                <span class="text-field__helper-text" role="alert">
                                    {{ $errors->first('first_name') }}
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="xs12">
                        <div class="text-field text-field--filled {{ $errors->has('last_name') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <input id="last_name" type="text" class="text-field__input" name="last_name" value="{{ old('last_name') ?? $user->last_name }}" required autofocus>
                            <label for="last_name" class="text-field__label">{{ __('model.Last name') }}*</label>

                            @if ($errors->has('last_name'))
                                <span class="text-field__helper-text" role="alert">
                                    {{ $errors->first('last_name') }}
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="xs12">
                        <div class="text-field text-field--filled {{ $errors->has('email') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <input id="email" type="email" class="text-field__input" name="email" value="{{ old('email') ?? $user->email }}" required>
                            <label for="email" class="text-field__label">{{ __('model.Email') }}*</label>

                            @if ($errors->has('email'))
                                <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('email') }}
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="xs12">
                        <div class="text-field text-field--filled text-field--select {{ $errors->has('roles') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <div class="text-field__input"></div>
                            <select id="roles" name="roles[]" multiple>
                                @foreach($roles as $role)
                                    <option value="{{ $role->id }}"
                                        {{ old('roles') && in_array( $role->id, old('roles')) || in_array( $role->id, $user->roles->pluck('id')->toArray()) ? 'selected' : '' }}
                                    >
                                        {{ $role->name }}
                                    </option>
                                @endforeach
                            </select>
                            <label for="roles" class="text-field__label">{{ trans_choice('model.Role', 2) }}*</label>

                            @if ($errors->has('roles'))
                                <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('roles') }}
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="xs12">
                <button type="submit" class="button button--contained button--contained--icon-left">
                    <i class="material-icons icon">save</i>
                    {{ __('model.edit', ['model' => trans_choice('model.User', 1)]) }}
                </button>

                <button type="reset" class="button button--text button--text--icon-left">
                    <i class="material-icons icon">settings_backup_restore</i>
                    {{ __('model.reset') }}
                </button>
            </div>
        </div>
    </form>
@endsection
