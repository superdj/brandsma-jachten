@extends('layouts.app')
@section('title', trans_choice('model.User', 2))

@section('content')
    <div class="row">
        @include('partials.create-button')

        @include('partials.filter',
           [
               'items' => $users,
               'fields' => [
                    'id' => __('model.Id'),
                    'first_name' => __('model.First name'),
                    'last_name' => __('model.Last name'),
                    'email' => __('model.Email'),
                    'user_created' => __('model.Creator'),
                    'user_updated' => __('model.Updater'),
                    'created_at' => __('model.Created at'),
                    'updated_at' => __('model.Updated at'),
               ]
           ]
        )

        @include('partials.pagination', ['items' => $users])

        <div class="xs12">
            @if($users->count() > 0)
                <table class="data-table data-table--hover data-table--responsive">
                    <thead>
                    <tr>
                        <th>{{ __('model.First name') }}</th>
                        <th>{{ __('model.Last name') }}</th>
                        <th>{{ __('model.Email') }}</th>
                        <th class="data-table--numeric">{{ trans_choice('model.Assignment', 2) }}</th>
                        <th class="data-table--numeric">{{ trans_choice('model.Role', 2) }}</th>
                        <th>{{ __('model.Actions') }}</th>
                    </tr>
                    </thead>

                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>
                                    @can('show users')
                                        <a href="{{ route('users.show', $user->id) }}">
                                            {{ $user->first_name }}
                                        </a>
                                    @endcan

                                    @cannot('show users')
                                        {{ $user->first_name }}
                                    @endcannot
                                </td>
                                <td>
                                    @can('show users')
                                        <a href="{{ route('users.show', $user->id) }}">
                                            {{ $user->last_name }}
                                        </a>
                                    @endcan

                                    @cannot('show users')
                                        {{ $user->last_name }}
                                    @endcannot
                                </td>
                                <td>
                                    <a href="mailto:{{ $user->email }}">
                                        {{ $user->email }}
                                    </a>
                                </td>
                                <td class="data-table--numeric">{{ $user->assignments->count() }}</td>
                                <td class="data-table--numeric">{{ $user->roles->count() }}</td>
                                <td>
                                    @include('partials.actions',
                                        [
                                            'item' => $user,
                                            'actions' => ['edit', 'show', 'delete']
                                        ]
                                    )
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @else
                @include('partials.not-found-banner', ['model' => trans_choice('model.user', 2)])
            @endif
        </div>

        @include('partials.pagination', ['items' => $users])
    </div>
@endsection;
