@extends('layouts.app')
@section('title', __('model.info', ['name' => $user->first_name.' '.$user->last_name, 'model' => trans_choice('model.User', 1)]))

@section('content')
    <div class="row">
        <div class="xs12">
            <a href="{{ route('users.index') }}" class="button button--contained button--contained--icon-left">
                <i class="material-icons icon">arrow_back</i>
                {{ __('model.back', ['page' => trans_choice('model.user', 2)]) }}
            </a>
        </div>

        <div class="xs12 sm6 md3">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        <i class="material-icons icon">info</i>
                        {{ __('model.General') }}
                    </div>
                </div>

                <div class="card__text">
                    <ul class="list">
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.Name') }}</span>
                                <span class="list__content__text">{{ $user->first_name }} {{ $user->last_name }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.Email') }}</span>
                                <span class="list__content__text">
                                    <a href="mailto:{{ $user->email }}">
                                        {{ $user->email }}
                                    </a>
                                </span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Role', 2) }}</span>
                                <span class="list__content__text">
                                    @foreach($user->roles as $role)
                                        @can('show roles')
                                            <a href="{{ route('roles.show', $role->id) }}">
                                                {{ $role->name }}
                                            </a>
                                        @endcan

                                        @cannot('show roles')
                                            {{ $role->name }}
                                        @endcannot
                                        {{ $loop->last ? '' : ',' }}
                                    @endforeach
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>

                @can('edit users')
                    <div class="card__actions">
                        <a href="{{ route('users.edit', $user->id) }}" class="button button--text">
                            {{ __('model.edit', ['model' => trans_choice('model.User', 1)]) }}
                        </a>
                    </div>
                @endcan
            </div>
        </div>

        <div class="xs12 md8 lg5">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        @can('view assignments')
                            <a href="{{ route('users.index') }}">
                                <i class="material-icons icon">assignment</i>
                                {{ trans_choice('model.Assignment', 2) }}
                            </a>
                        @endcan

                        @cannot('view assignments')
                            <i class="material-icons icon">assignment</i>
                            {{ trans_choice('model.Assignment', 2) }}
                        @endcannot
                    </div>
                </div>

                <div class="card__text">
                    @if($user->assignments->count() > 0)
                        <table class="data-table data-table--hover">
                            <thead>
                            <tr>
                                <th>{{ __('model.Name') }}</th>
                                <th>{{ trans_choice('model.Customer', 1) }}</th>
                                <th>{{ __('model.Deadline') }}</th>
                                <th>{{ trans_choice('model.Time registration', 1) }}</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($user->assignments->sortBy('end_date') as $assignment)
                                <tr>
                                    <td>
                                        @can('show assignments')
                                            <a href="{{ route('assignments.show', $assignment->id) }}">
                                                {{ $assignment->name }}
                                            </a>
                                        @endcan

                                        @cannot('show users')
                                            {{ $assignment->name }}
                                        @endcannot
                                    </td>
                                    <td>
                                        @can('show customers')
                                            <a href="{{ route('customers.show', $assignment->customer->id) }}">
                                                {{ $assignment->customer->name }}
                                            </a>
                                        @endcan

                                        @cannot('show customers')
                                            {{ $assignment->customer->name }}
                                        @endcannot
                                    </td>
                                    <td class="{{$assignment->end_date->isPast() ? 'text--red-900 red-50' : 'text--green-900 green-50' }}">
                                        {{ Carbon\Carbon::now()->diffForHumans($assignment->end_date) }}</td>
                                    <td>
                                        @php
                                            $times = $assignment->time_registrations->where('user_id', '=', $user->id)->pluck('time')->toArray();
                                             $total_time = sum_time($times);
                                        @endphp

                                        {{ date('H:i:s', strtotime($total_time)) }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        @include('partials.not-found-banner', ['model' => trans_choice('model.assignment', 2)])
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
