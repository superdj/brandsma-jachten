@extends('layouts.app')
@section('title', trans_choice('model.Role', 2))

@section('content')
    <div class="row">
        @include('partials.create-button')

        {{--<div class="xs12">
            <form action="{{ route('roles.index') }}">
                <div class="row padding--0">
                    <div class="xs6 md3">
                        <div class="text-field text-field--filled">
                            <input type="search" name="q" id="search" class="text-field__input" value="{{ $q }}">
                            <label for="search" class="text-field__label">{{ __('model.Search') }}</label>
                        </div>
                    </div>

                    <div class="xs6 md2">
                        <div class="text-field text-field--filled text-field--select">
                            <div class="text-field__input"></div>
                            <select name="sort" id="sort">
                                @php
                                    $fields = [
                                        'id' => __('model.Id'),
                                        'name' => __('model.Name'),
                                        'created_at' => __('model.Created at'),
                                        'updated_at' => __('model.Updated at'),
                                    ];
                                @endphp
                                @foreach($fields as $field => $field_name)
                                    <option value="{{ $field }}" {{ $field === $sortBy ? 'selected' : '' }}>
                                        {{ $field_name }}
                                    </option>
                                @endforeach
                            </select>
                            <label for="sort" class="text-field__label">{{ __('model.Order by') }}</label>
                        </div>
                    </div>

                    <div class="xs6 md2">
                        <div class="text-field text-field--filled text-field--select">
                            <div class="text-field__input"></div>
                            <select name="order" id="order">
                                @foreach(['asc', 'desc'] as $field)
                                    <option value="{{ $field }}" {{ $field === $orderBy ? 'selected' : '' }}>
                                        {{ __('model.'.ucfirst($field)) }}
                                    </option>
                                @endforeach
                            </select>
                            <label for="order" class="text-field__label">{{ __('model.Sort by') }}</label>
                        </div>
                    </div>

                    <div class="xs6 md2">
                        <div class="text-field text-field--filled text-field--select">
                            <div class="text-field__input"></div>
                            <select name="perPage" id="perPage">
                                @foreach([5, 10, 15, 20, 50, 100] as $field)
                                    <option value="{{ $field }}" {{ $field == $perPage ? 'selected' : '' }}>
                                        {{ $field }}
                                    </option>
                                @endforeach
                            </select>
                            <label for="perPage" class="text-field__label">{{ __('model.Per page') }}</label>
                        </div>
                    </div>

                    <div class="xs6 md2">
                        <button type="submit" class="button button--contained">
                            {{ __('model.Filter') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>

        @include('partials.pagination', ['items' => $roles])--}}

        <div class="xs12">
            @if($roles->count() > 0)
                <table class="data-table data-table--hover data-table--responsive">
                    <thead>
                    <tr>
                        <th>{{ __('model.name', ['model' => trans_choice('model.Role', 1)]) }}</th>
                        <th>{{ trans_choice('model.Permission', 2) }}</th>
                        <th>{{ trans_choice('model.User', 2) }}</th>
                        <th>{{ __('model.Actions') }}</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($roles as $role)
                        <tr>
                            <td>
                                @can('show roles')
                                    <a href="{{ route('roles.show', $role->id) }}">
                                        {{ $role->name }}
                                    </a>
                                @endcan

                                @cannot('show roles')
                                    {{ $role->name }}
                                @endcannot
                            </td>
                            <td>{{ $role->permissions->count() }}</td>
                            <td>{{ $role->users->count() }}</td>
                            <td>
                                @include('partials.actions',
                                        [
                                            'item' => $role,
                                            'actions' => ['edit', 'show', 'delete']
                                        ]
                                    )
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            @else
                @include('partials.not-found-banner', ['model' => trans_choice('model.role', 2)])
            @endif
        </div>

        {{--@include('partials.pagination', ['items' => $roles])--}}
    </div>
@endsection;
