@extends('layouts.app')
@section('title', __('model.info', ['name' => $role->name, 'model' => trans_choice('model.Role', 1)]))

@section('content')
    <div class="row">
        <div class="xs12">
            <a href="{{ route('roles.index') }}" class="button button--contained button--contained--icon-left">
                <i class="material-icons icon">arrow_back</i>
                {{ __('model.back', ['page' => trans_choice('model.role', 2)]) }}
            </a>
        </div>

        <div class="xs12 sm6 md3">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        <i class="material-icons icon">info</i>
                        {{ __('model.General') }}
                    </div>
                </div>

                <div class="card__text">
                    <ul class="list">
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.name', ['model' => trans_choice('model.Role', 1)]) }}</span>
                                <span class="list__content__text">{{ $role->name }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Permission', 2) }}</span>
                                <span class="list__content__text">
                                    @foreach($role->permissions as $permission)
                                        {{
                                            __('model.permission.'.explode(' ', $permission->name)[0], [
                                                'model' => trans_choice('model.'.ucfirst(str_replace('-', ' ', Str::singular(explode(' ', $permission->name)[1]))), 2)
                                            ])
                                        }}{{ $loop->last ? '' : ',' }}
                                    @endforeach
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>

                @can('edit roles')
                    <div class="card__actions">
                        <a href="{{ route('roles.edit', $role->id) }}" class="button button--text">
                            {{ __('model.edit', ['model' => trans_choice('model.Role', 1)]) }}
                        </a>
                    </div>
                @endcan
            </div>
        </div>

        <div class="xs12 sm6 md4">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        @can('view users')
                            <a href="{{ route('users.index') }}">
                                <i class="material-icons icon">person</i>
                                {{ trans_choice('model.User', 2) }}
                            </a>
                        @endcan

                        @cannot('view users')
                            <i class="material-icons icon">person</i>
                            {{ trans_choice('model.User', 2) }}
                        @endcannot
                    </div>
                </div>

                <div class="card__text">
                    @if($role->users->count() > 0)
                        <table class="data-table data-table--hover">
                            <thead>
                            <tr>
                                <th>{{ __('model.Name') }}</th>
                                <th>{{ trans_choice('model.Role', 2) }}</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($role->users as $user)
                                <tr>
                                    <td>
                                        @can('show users')
                                            <a href="{{ route('users.show', $user->id) }}">
                                                {{ $user->first_name }} {{ $user->last_name }}
                                            </a>
                                        @endcan

                                        @cannot('show users')
                                            {{ $user->first_name }} {{ $user->last_name }}
                                        @endcannot
                                    </td>
                                    <td>{{ $user->roles->count() }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        @include('partials.not-found-banner', ['model' => trans_choice('model.user', 2)])
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
