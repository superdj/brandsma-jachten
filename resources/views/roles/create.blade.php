@extends('layouts.app')
@section('title', __('model.add', ['model' => trans_choice('model.Role', 1)]))

@section('content')
    <form method="POST" action="{{ route('roles.store') }}">
        @csrf
        <div class="row">
            <div class="xs12">
                <a href="{{ route('roles.index') }}" class="button button--contained button--contained--icon-left">
                    <i class="material-icons icon">arrow_back</i>
                    {{ __('model.back', ['page' => trans_choice('model.role', 2)]) }}
                </a>
            </div>

            <div class="xs12 sm6 md3">
                <div class="row padding--0">
                    <h5 class="xs12">{{ trans_choice('model.Role', 1) }}</h5>

                    <div class="xs12">
                        <div class="text-field text-field--filled {{ $errors->has('name') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <input id="name" type="text" class="text-field__input" name="name" value="{{ old('name') }}" required autofocus>
                            <label for="name" class="text-field__label">{{ __('model.name', ['model' => trans_choice('model.Role', 1)]) }}*</label>

                            @if ($errors->has('name'))
                                <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('name') }}
                            </span>
                            @endif
                        </div>
                    </div>

                    @foreach($permissions as $permission)
                        <div class="xs12">
                            <label>
                                <input
                                    type="checkbox"
                                    class="checkbox"
                                    value="{{ $permission->id }}"
                                    {{ old('permissions') && in_array( $permission->id, old('permissions')) ? 'selected' : '' }}
                                    name="permissions[]">
                                {{
                                    __('model.permission.'.explode(' ', $permission->name)[0], [
                                        'model' => trans_choice('model.'.ucfirst(str_replace('-', ' ', Str::singular(explode(' ', $permission->name)[1]))), 2)
                                    ])
                                }}
                            </label>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="xs12">
                <button type="submit" class="button button--contained button--contained--icon-left">
                    <i class="material-icons icon">save</i>
                    {{ __('model.add', ['model' => trans_choice('model.Role', 1)]) }}
                </button>

                <button type="reset" class="button button--text button--text--icon-left">
                    <i class="material-icons icon">settings_backup_restore</i>
                    {{ __('model.reset') }}
                </button>
            </div>
        </div>
    </form>
@endsection
