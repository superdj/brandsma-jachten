@extends('layouts.app')
@section('title', __('model.info', ['name' => $order->name, 'model' => trans_choice('model.Order', 1)]))

@section('content')
    <div class="row">
        <div class="xs12">
            <a href="{{ route('orders.index') }}" class="button button--contained button--contained--icon-left">
                <i class="material-icons icon">arrow_back</i>
                {{ __('model.back', ['page' => trans_choice('model.order', 2)]) }}
            </a>
        </div>

        <div class="xs12 sm6 md3">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        <i class="material-icons icon">info</i>
                        {{ __('model.General') }}
                    </div>
                </div>

                <div class="card__text">
                    <ul class="list">
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.name', ['model' => trans_choice('model.Order', 1)]) }}</span>
                                <span class="list__content__text">{{ $order->name }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.Order number') }}</span>
                                <span class="list__content__text">{{ $order->number }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Status', 1) }}</span>
                                <span class="list__content__text">{{ $order->status->name }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.Created by') }}</span>
                                <span class="list__content__text">
                                    @can('show users')
                                        <a href="{{ route('users.show', $order->creator->id) }}">
                                            {{ $order->creator->first_name }}
                                        </a>
                                    @endcan

                                    @cannot('show users')
                                        {{ $order->creator->first_name }}
                                    @endcannot
                                </span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.Updated by') }}</span>
                                <span class="list__content__text">
                                    @if($order->updater)
                                        @can('show users')
                                            <a href="{{ route('users.show', $order->updater->id) }}">
                                            {{ $order->updater->first_name }} - {{ $order->updated_at }}
                                        </a>
                                        @endcan

                                        @cannot('show users')
                                            {{ $order->updater->first_name }} - {{ $order->updated_at }}
                                        @endcannot
                                    @endif
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>

                @can('edit orders')
                    @if(!$order->status->lock)
                        <div class="card__actions">
                            <a href="{{ route('orders.edit', $order->id) }}" class="button button--text">
                                {{ __('model.edit', ['model' => trans_choice('model.Order', 1)]) }}
                            </a>
                        </div>
                    @endif
                @endif
            </div>
        </div>

        <div class="xs12 md9 lg7">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        @can('view parts')
                            <a href="{{ route('parts.index') }}">
                                <i class="material-icons icon">build</i>
                                {{ trans_choice('model.Part', 2) }}
                            </a>
                        @endcan

                        @cannot('view parts')
                            <i class="material-icons icon">build</i>
                            {{ trans_choice('model.Part', 2) }}
                        @endcannot
                    </div>
                </div>

                <div class="card__text">
                    <table class="data-table data-table--hover">
                        <thead>
                        <tr>
                            <th>{{ trans_choice('model.Part', 1) }}</th>
                            <th>{{ __('model.Parts number') }}</th>
                            <th class="data-table--numeric">{{ __('model.Price') }}</th>
                            <th class="data-table--numeric">{{ __('model.Amount') }}</th>
                            <th class="data-table--numeric">{{ __('model.Worth') }}</th>
                        </tr>
                        </thead>

                        <tbody>
                        @php
                            $totalPrice = 0;
                        @endphp

                        @foreach($order->parts as $part)
                            <tr>
                                <td>
                                    @can('show parts')
                                        <a href="{{ route('parts.show', $part->id) }}">
                                            {{ $part->name }}
                                        </a>
                                    @endcan

                                    @cannot('show parts')
                                        {{ $part->name }}
                                    @endcan
                                </td>
                                <td>{{ $part->number }}</td>
                                <td class="data-table--numeric">&euro;{{ number_format($part->price, 2, ',', '.') }}</td>
                                <td class="data-table--numeric">
                                    {{ $part->pivot->amount }}
                                    {{ $part->unit->abbreviation }}
                                </td>
                                <td class="data-table--numeric">&euro;{{ number_format($part->pivot->amount * $part->price, 2, ',', '.') }}</td>
                            </tr>

                            @php
                                $totalPrice += $part->pivot->amount * $part->price;
                            @endphp

                        @endforeach
                        <tr>
                            <td><strong>{{ __('model.Total') }}</strong></td>
                            <td></td>
                            <td></td>
                            <td class="data-table--numeric">{{ $order->parts()->sum('amount') }}</td>
                            <td class="data-table--numeric">&euro;{{ number_format($totalPrice, 2, ',', '.') }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
