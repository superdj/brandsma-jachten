@extends('layouts.app')
@section('title', __('model.edit_title', ['name' => $order->name, 'model' => trans_choice('model.order', 1)]))

@section('content')

    <form method="POST" action="{{ route('orders.update', $order->id) }}">
        @method('put')
        @csrf

        <div class="row">
            <div class="xs12">
                <a href="{{ route('orders.index') }}" class="button button--contained button--contained--icon-left">
                    <i class="material-icons icon">arrow_back</i>
                    {{ __('model.back', ['page' => trans_choice('model.order', 2)]) }}
                </a>
            </div>

            <div class="xs12 sm6 md3">
                <div class="row padding--0">
                    <h5 class="xs12">{{ trans_choice('model.Order', 1) }}</h5>

                    <div class="xs12">
                        <div class="text-field text-field--filled {{ $errors->has('name') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <input id="name" type="text" class="text-field__input" name="name" value="{{ old('name') ?? $order->name }}" required autofocus>
                            <label for="name" class="text-field__label">{{ __('model.name', ['model' => trans_choice('model.Order', 1)]) }}*</label>

                            @if ($errors->has('name'))
                                <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('name') }}
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="xs12">
                        <div class="text-field text-field--filled {{ $errors->has('number') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <input id="number" type="text" class="text-field__input" name="number" value="{{ old('number') ?? $order->number }}">
                            <label for="number" class="text-field__label">{{ __('model.Order number') }}</label>

                            @if ($errors->has('number'))
                                <span class="text-field__helper-text" role="alert">
                                    {{ $errors->first('number') }}
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="xs12">
                        <div class="text-field text-field--filled text-field--select {{ $errors->has('status_id') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <div class="text-field__input"></div>

                            <select id="status_id" name="status_id" required>
                                @foreach($statuses as $status)
                                    <option value="{{ $status->id }}"
                                        {{ old('status_id') === $status->id || $order->status_id === $status->id ? 'selected' : '' }}
                                    >
                                        {{ $status->name }}
                                    </option>
                                @endforeach
                            </select>

                            <label for="status_id" class="text-field__label">{{ trans_choice('model.Status', 1) }}*</label>

                            @if ($errors->has('status_id'))
                                <span class="text-field__helper-text" role="alert">
                                    {{ $errors->first('status_id') }}
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="xs12 sm6 md3">
                <div class="row padding--0">
                    <h5 class="xs12">{{ trans_choice('model.Part', 2) }}</h5>

                    <datalist id="part_shelf">
                        @foreach($parts as $part)
                            @if($part->suppliers->count() > 0)
                                @php
                                    $number = '';
                                    foreach($part->suppliers as $supplier)
                                    {
                                        if($supplier->pivot->preferred)
                                        {
                                            $number = $supplier->number;
                                        }
                                    }
                                @endphp

                                @if(!empty($number))
                                    <option value="{{ $part->id }}">
                                        {{ $part->name }} -
                                        {{ $number }}
                                    </option>
                                @else
                                    <option value="{{ $part->id }}">
                                        {{ $part->name }}
                                    </option>
                                @endif
                            @else
                                <option value="{{ $part->id }}">
                                    {{ $part->name }}
                                </option>
                            @endif
                        @endforeach
                    </datalist>

                    <div class="xs12" id="parts">
                        @foreach($order->parts as $part)
                            <div class="row padding--0 part">
                                <div class="xs12 md8">
                                    <div class="text-field text-field--filled">
                                        <input id="part-{{ $loop->index }}" type="text" list="part_shelf" class="text-field__input" name="parts[]" required value="{{ $part->id }}">
                                        <label for="part-{{ $loop->index }}" class="text-field__label">{{ trans_choice('model.Part', 1) }}*</label>
                                    </div>
                                </div>

                                <div class="xs12 md4">
                                    <div class="text-field text-field--filled">
                                        <input id="amount-{{ $loop->index }}" type="number" min="0.00" step="0.01" class="text-field__input" name="amounts[]" required value="{{ $part->pivot->amount }}">
                                        <label for="amount-{{ $loop->index }}" class="text-field__label">{{ __('model.Amount') }}*</label>
                                    </div>
                                </div>

                                <hr class="divider xs12">
                            </div>
                        @endforeach
                    </div>

                    <div class="xs12">
                        <button class="button button--outlined" type="button" id="add-part">
                            {{ __('model.add', ['model' => trans_choice('model.Part', 1)]) }}
                        </button>
                    </div>
                </div>
            </div>

            <div class="xs12">
                <button type="submit" class="button button--contained button--contained--icon-left">
                    <i class="material-icons icon">save</i>
                    {{ __('model.edit', ['model' => trans_choice('model.Order', 1)]) }}
                </button>

                <button type="reset" class="button button--text button--text--icon-left">
                    <i class="material-icons icon">settings_backup_restore</i>
                    {{ __('model.reset') }}
                </button>
            </div>
        </div>
    </form>
@endsection

@section('js')
    <script>
        const addPart = document.getElementById('add-part');
        const partsContainer = document.getElementById('parts');
        let deleteParts = document.getElementsByClassName('delete-part');
        let parts = {{ $order->parts->count() }};

        addPart.addEventListener('click', () => {
            let html = `
                <div class="xs12 md8">
                    <div class="text-field text-field--filled">
                        <input id="part-${parts}" type="text" list="part_shelf" class="text-field__input" name="parts[]" required>
                        <label for="part-${parts}" class="text-field__label">{{ trans_choice('model.Part', 1) }}*</label>
                    </div>
                </div>

                <div class="xs12 md4">
                    <div class="text-field text-field--filled">
                        <input id="amount-${parts}" type="number" min="0.00" step="0.01" class="text-field__input" name="amounts[]" required>
                        <label for="amount-${parts}" class="text-field__label">{{ __('model.Amount') }}*</label>
                    </div>
                </div>

                <div class="xs12">
                    <button class="button button--outlined delete-part" type="button">
                        {{ __('model.delete', ['model' => trans_choice('model.Part', 1)]) }}
                </button>
            </div>

            <hr class="divider xs12">`;

            partsContainer.appendChild(document.createElement('id'));
            let lastChild = partsContainer.lastChild;
            lastChild.className = 'row padding--0 part';
            lastChild.innerHTML = html;
            parts++;

            for( let deletePart of deleteParts )
            {
                deletePart.addEventListener('click', () => {
                    let part = deletePart.closest('.part');
                    part.remove();
                });
            }
        });

        for( let deletePart of deleteParts )
        {
            deletePart.addEventListener('click', () => {
                let part = deletePart.closest('.part');
                part.remove();
            });
        }
    </script>
@endsection
