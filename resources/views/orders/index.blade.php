@extends('layouts.app')
@section('title', trans_choice('model.Order', 2))

@section('content')
    <div class="row">
        @include('partials.create-button')

        @include('partials.filter',
            [
                'items' => $orders,
                'fields' => [
                    'id' => __('model.Id'),
                    'name' => __('model.Name'),
                    'status_id' => trans_choice('Status', 1),
                    'user_created' => __('model.Creator'),
                    'user_updated' => __('model.Updater'),
                    'created_at' => __('model.Created at'),
                    'updated_at' => __('model.Updated at'),
                ]
            ]
       )

        @include('partials.pagination', ['items' => $orders])

        <div class="xs12">
            @if($orders->count() > 0)
                <table class="data-table data-table--hover data-table--responsive">
                    <thead>
                    <tr>
                        <th>{{ __('model.name', ['model' => trans_choice('model.Order', 1)]) }}</th>
                        <th>{{ trans_choice('model.Part', 2) }}</th>
                        <th>{{ __('model.Total') }}</th>
                        <th>{{ trans_choice('model.Status', 1) }}</th>
                        <th class="md-down-hide">{{ __('model.Created at') }}</th>
                        <th>{{ __('model.Updated at') }}</th>
                        <th>{{ __('model.Actions') }}</th>
                    </tr>
                    </thead>

                    <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>
                                    @can('show orders')
                                        <a href="{{ route('orders.show', $order->id) }}">
                                            {{ $order->name }}
                                        </a>
                                    @endcan

                                    @cannot('show orders')
                                        {{ $order->name }}
                                    @endcan
                                </td>
                                <td>{{ $order->parts->count() }}</td>
                                <td>{{ $order->parts()->sum('amount') }}</td>
                                <td>{{ $order->status->name }}</td>
                                <td class="md-down-hide">{{  $order->created_at->format('d-m-y H:i:s') }}</td>
                                <td class="md-down-hide">{{ $order->updated_at ? $order->updated_at->format('d-m-y H:i:s') : '' }}</td>
                                <td>
                                    @include('partials.actions',
                                        [
                                            'item' => $order,
                                            'actions' => ['edit', 'show', 'delete']
                                        ]
                                    )
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @else
                @include('partials.not-found-banner', ['model' => trans_choice('model.order', 2)])
            @endif
        </div>

        @include('partials.pagination', ['items' => $orders])

    </div>
@endsection;
