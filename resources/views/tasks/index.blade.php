@extends('layouts.app')
@section('title', trans_choice('model.Task', 2))

@section('content')
    <div class="row">
        @include('partials.create-button')

        @include('partials.filter',
             [
                 'items' => $tasks,
                 'fields' => [
                    'id' => __('model.Id'),
                    'name' => __('model.Name'),
                    'user_created' => __('model.Creator'),
                    'user_updated' => __('model.Updater'),
                    'created_at' => __('model.Created at'),
                    'updated_at' => __('model.Updated at'),
                 ]
             ]
        )

        @include('partials.pagination', ['items' => $tasks])

        <div class="xs12">
            @if($tasks->count() > 0)
                <table class="data-table data-table--hover data-table--responsive">
                    <thead>
                    <tr>
                        <th>{{ __('model.name', ['model' => trans_choice('model.Task', 1)]) }}</th>
                        <th class="data-table--numeric">{{ trans_choice('model.Assignment', 2) }}</th>
                        <th class="md-down-hide">{{ __('model.Created by') }}</th>
                        <th class="md-down-hide">{{ __('model.Updated by') }}</th>
                        <th>{{ __('model.Actions') }}</th>
                    </tr>
                    </thead>

                    <tbody>
                        @foreach($tasks as $task)
                            <tr>
                                <td>{{ $task->name }}</td>
                                <td class="data-table--numeric">{{ $task->assignments->count() }}</td>
                                <td class="md-down-hide">{{ $task->creator->first_name }}</td>
                                <td class="md-down-hide">{{ $task->updater ? $task->updater->first_name : '' }}</td>
                                <td>
                                    @include('partials.actions',
                                        [
                                            'item' => $task,
                                            'actions' => ['edit', 'delete']
                                        ]
                                    )
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @else
                @include('partials.not-found-banner', ['model' => trans_choice('model.task', 2)])
            @endif
        </div>

        @include('partials.pagination', ['items' => $tasks])
    </div>
@endsection;
