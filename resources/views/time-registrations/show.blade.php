@extends('layouts.app')
@section('title', __('model.info', ['name' => $time_registration->assignment->customer->name.' - '.$time_registration->assignment->name, 'model' => trans_choice('model.Time registration', 1)]))

@section('content')
    <div class="row">
        <div class="xs12">
            <a href="{{ route('time-registrations.index') }}" class="button button--contained button--contained--icon-left">
                <i class="material-icons icon">arrow_back</i>
                {{ __('model.back', ['page' => trans_choice('model.Time registration', 2)]) }}
            </a>
        </div>

        <div class="xs12 sm6 md3">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        <i class="material-icons icon">info</i>
                        {{ __('model.General') }}
                    </div>
                </div>

                <div class="card__text">
                    <ul class="list">
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.Time') }}</span>
                                <span class="list__content__text">{{ $time_registration->time }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Mechanic', 1) }}</span>
                                <span class="list__content__text">
                                    @can('show users')
                                        <a href="{{ route('users.show', $time_registration->user->id) }}">
                                            {{ $time_registration->user->first_name }}
                                        </a>
                                    @endcan

                                    @cannot('show users')
                                        {{ $time_registration->user->first_name }}
                                    @endcannot
                                </span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Assignment', 1) }}</span>
                                <span class="list__content__text">
                                    @can('show assignments')
                                        <a href="{{ route('assignments.show', $time_registration->assignment->id) }}">
                                            {{ $time_registration->assignment->name }}
                                        </a>
                                    @endcan

                                    @cannot('show assignments')
                                        {{ $time_registration->assignment->name }}
                                    @endcannot
                                </span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Task', 2) }}</span>
                                <span class="list__content__text">
                                    @foreach($time_registration->tasks as $task)
                                        {{ $task->name }}{{ $loop->last ? '' : ',' }}
                                    @endforeach
                                </span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.Created by') }}</span>
                                <span class="list__content__text">
                                    {{ $time_registration->creator->first_name }} -
                                    {{ $time_registration->created_at->format('d-m-Y H:i:s') }}
                                </span>
                            </div>
                        </li>
                        @if($time_registration->updated)
                            <li class="list--two-line">
                                <div class="list__content">
                                    <span class="list__content__title">{{ __('model.Updated by') }}</span>
                                    <span class="list__content__text">
                                        {{ $time_registration->updater->first_name }} -
                                        {{ $time_registration->updated_at->format('d-m-Y H:i:s') }}
                                    </span>
                                </div>
                            </li>
                        @endif
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Created by', 1) }}</span>
                                <span class="list__content__text">
                                    @can('show users')
                                        <a href="{{ route('users.show', $time_registration->creator->id) }}">
                                            {{ $time_registration->creator->first_name }}
                                        </a>
                                    @endcan

                                    @cannot('show users')
                                        {{ $time_registration->creator->first_name }}
                                    @endcannot
                                </span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Updated by', 1) }}</span>
                                <span class="list__content__text">
                                    @if($time_registration->updater)
                                        @can('show users')
                                            <a href="{{ route('users.show', $time_registration->updater->id) }}">
                                            {{ $time_registration->updater->first_name }}  -
                                            {{ $time_registration->updated_at->format('d-m-Y H:i:s') }}
                                        </a>
                                        @endcan

                                        @cannot('show users')
                                            {{ $time_registration->updater->first_name }} -
                                            {{ $time_registration->updated_at->format('d-m-Y H:i:s') }}
                                        @endcannot
                                    @endif
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="xs12 sm6 md4">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        @can('view parts')
                            <a href="{{ route('tasks.index') }}">
                                <i class="material-icons icon">assignment</i>
                                {{ trans_choice('model.Task', 2) }}
                            </a>
                        @endcan

                        @cannot('view parts')
                            <i class="material-icons icon">assignment</i>
                            {{ trans_choice('model.Task', 2) }}
                        @endcannot
                    </div>
                </div>

                <div class="card__text">
                    @if($time_registration->assignment->tasks->count() > 0)
                        <table class="data-table data-table--hover">
                            <thead>
                            <tr>
                                <th>{{ trans_choice('model.Task', 1) }}</th>
                                <th>{{ trans_choice('model.Status', 1) }}</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($time_registration->assignment->tasks as $task)
                                <tr>
                                    <td>{{ $task->name }}</td>
                                    <td>
                                        @if(in_array($task->id, $time_registration->tasks->pluck('id')->toArray()))
                                            <i class="material-icons icon text--green">check_box</i>
                                        @else
                                            <i class="material-icons icon text--red">check_box_outline_blank</i>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        @include('partials.not-found-banner', ['model' => trans_choice('model.task', 2)])
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
