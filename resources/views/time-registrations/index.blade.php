@extends('layouts.app')
@section('title', trans_choice('model.Time registration', 2))

@section('content')
    <div class="row">
        @include('partials.filter',
             [
                 'items' => $time_registrations,
                 'fields' => [
                    'id' => __('model.Id'),
                    'assignment_id' => trans_choice('model.Assignment', 1),
                    'user_id' => trans_choice('model.Mechanic', 1),
                    'user_created' => __('model.Creator'),
                    'user_updated' => __('model.Updater'),
                    'created_at' => __('model.Created at'),
                    'updated_at' => __('model.Updated at'),
                 ]
             ]
        )

        @include('partials.pagination', ['items' => $time_registrations])

        <div class="xs12">
            @if($time_registrations->count() > 0)
                <table class="data-table data-table--hover data-table--responsive">
                    <thead>
                    <tr>
                        <th>{{ __('model.Time') }}</th>
                        <th>{{ trans_choice('model.Assignment', 1) }}</th>
                        <th>{{ trans_choice('model.Customer', 1) }}</th>
                        <th class="data-table--numeric">{{ trans_choice('model.Task', 2) }}</th>
                        <th>{{ trans_choice('model.Mechanic', 1) }}</th>
                        <th>{{ __('model.Actions') }}</th>
                    </tr>
                    </thead>

                    <tbody>
                        @foreach($time_registrations as $time_registration)
                            <tr>
                                <td>{{ $time_registration->time }}</td>
                                <td>
                                    @can('show assignments')
                                        <a href="{{ route('assignments.show', $time_registration->assignment->id) }}">
                                            {{ $time_registration->assignment->name }}
                                        </a>
                                    @endcan

                                    @cannot('show assignments')
                                        {{ $time_registration->assignment->name }}
                                    @endcannot
                                </td>
                                <td>
                                    @can('show customers')
                                        <a href="{{ route('customers.show', $time_registration->assignment->customer->id) }}">
                                            {{ $time_registration->assignment->customer->name }}
                                        </a>
                                    @endcan

                                    @cannot('show customers')
                                        {{ $time_registration->assignment->customer->name }}
                                    @endcannot
                                </td>
                                <td class="data-table--numeric">{{ $time_registration->tasks->count() }}</td>
                                <td>
                                    @can('show users')
                                        <a href="{{ route('users.show', $time_registration->user->id) }}">
                                            {{ $time_registration->user->first_name }}
                                        </a>
                                    @endcan

                                    @cannot('show users')
                                        {{ $time_registration->user->first_name }}
                                    @endcannot
                                </td>
                                <td>
                                    @include('partials.actions',
                                        [
                                            'item' => $time_registration,
                                            'actions' =>
                                            [
                                                $time_registration->created_at->endOfDay()->diffInMinutes(null, false) < 0 ? 'edit' : null,
                                                'show',
                                                $time_registration->created_at->endOfDay()->diffInMinutes(null, false) < 0 ? 'delete' : null
                                            ]
                                        ]
                                    )
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @else
                @include('partials.not-found-banner', ['model' => trans_choice('model.time registration', 2)])
            @endif
        </div>

        @include('partials.pagination', ['items' => $time_registrations])

    </div>
@endsection;
