@extends('layouts.app')
@section('title', __('model.add', ['model' => trans_choice('model.Time registration', 1).': "'.$assignment->name.'"']))

@section('content')

<form method="POST" action="{{ route('time-registrations.store') }}">
    @csrf

    <input type="hidden" name="assignment" value="{{ $assignment->id }}">

    <div class="row">
        <div class="xs12">
            <a href="{{ route('time-registrations.index') }}" class="button button--contained button--contained--icon-left">
                <i class="material-icons icon">arrow_back</i>
                {{ __('model.back', ['page' => trans_choice('model.time registration', 2)]) }}
            </a>
        </div>

        <div class="xs12 sm6 md3">
             <div class="row padding--0">
                 <h5 class="xs12">{{ trans_choice('model.Time registration', 1) }}</h5>

                <div class="xs12">
                    <div class="text-field text-field--filled {{ $errors->has('time') ? 'text-field--helper-text text-field--invalid' : '' }}">
                        <input id="time" type="time" class="text-field__input" name="time" value="{{ old('time') }}" required autofocus>
                        <label for="time" class="text-field__label">{{ __('model.Time') }}*</label>

                        @if ($errors->has('time'))
                            <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('time') }}
                            </span>
                        @endif
                    </div>
                </div>

                 <div class="xs12">
                     @php
                        $tasks_done = [];
                        foreach( $assignment->time_registrations as $registration )
                        {
                            foreach( $registration->tasks as $task )
                            {
                                $tasks_done[] = $task->id;
                            }
                        }
                     @endphp
                     @foreach($assignment->tasks as $task)
                         <div class="row padding--0">
                            <div class="xs12">
                                 <label>
                                     <input
                                         type="checkbox"
                                         class="checkbox"
                                         name="tasks[]"
                                         value="{{ $task->id }}"
                                         {{ in_array( $task->id, $tasks_done) ? 'checked disabled' : '' }}
                                     >
                                     {{ $task->name }}
                                 </label>
                            </div>
                         </div>
                     @endforeach
                 </div>

                 <div class="xs12">
                     <div class="text-field text-field--filled text-field--select {{ $errors->has('user_id') ? 'text-field--helper-text text-field--invalid' : '' }}">
                         <div class="text-field__input"></div>
                         <select name="user_id" id="user_id">
                             <option value="{{ auth()->user()->id }}">
                                 {{ auth()->user()->first_name }}
                             </option>

                             @foreach($assignment->users as $user)
                                 <option value="{{ $user->id }}">
                                     {{ $user->first_name }}
                                 </option>
                             @endforeach
                         </select>
                         <label for="user_id" class="text-field__label">{{ trans_choice('model.User', 1) }}*</label>

                         @if ($errors->has('user_id'))
                             <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('user_id') }}
                            </span>
                         @endif
                     </div>
                 </div>
             </div>
        </div>

        <div class="xs12">
            <button type="submit" class="button button--contained button--contained--icon-left">
                <i class="material-icons icon">save</i>
                {{ __('model.add', ['model' => trans_choice('model.Time registration', 1)]) }}
            </button>

            <button type="reset" class="button button--text button--text--icon-left">
                <i class="material-icons icon">settings_backup_restore</i>
                {{ __('model.reset') }}
            </button>
        </div>
    </div>
</form>
@endsection
