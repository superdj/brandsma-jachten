@extends('layouts.app')
@section('title', trans_choice('model.Rack', 2))

@section('content')
    <div class="row">
        @include('partials.create-button')

        @include('partials.filter',
            [
                'items' => $racks,
                'fields' => [
                    'id' => __('model.Id'),
                    'name' => __('model.Name'),
                    'warehouse_id' => trans_choice('model.Warehouse', 1),
                    'user_created' => __('model.Creator'),
                    'user_updated' => __('model.Updater'),
                    'created_at' => __('model.Created at'),
                    'updated_at' => __('model.Updated at'),
                ]
            ]
        )

        @include('partials.pagination', ['items' => $racks])

        <div class="xs12">
            @if($racks->count() > 0)
                <table class="data-table data-table--hover data-table--responsive">
                    <thead>
                    <tr>
                        <th>{{ __('model.name', ['model' => trans_choice('model.Rack', 1)]) }}</th>
                        <th>{{ trans_choice('model.Warehouse', 1) }}</th>
                        <th class="data-table--numeric">{{ trans_choice('model.Shelf', 2) }}</th>
                        <th class="md-down-hide">{{ __('model.Created by') }}</th>
                        <th class="md-down-hide">{{ __('model.Updated by') }}</th>
                        <th>{{ __('model.Actions') }}</th>
                    </tr>
                    </thead>

                    <tbody>
                        @foreach($racks as $rack)
                            <tr>
                                <td>{{ $rack->name }}</td>
                                <td>{{ $rack->warehouse->name }}</td>
                                <td class="data-table--numeric">{{ $rack->shelves->count() }}</td>
                                <td class="md-down-hide">{{ $rack->creator->first_name }}</td>
                                <td class="md-down-hide">{{ $rack->updater ? $rack->updater->first_name : '' }}</td>
                                <td>
                                    @include('partials.actions',
                                        [
                                            'item' => $rack,
                                            'actions' => ['edit', 'delete']
                                        ]
                                    )
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @else
                @include('partials.not-found-banner', ['model' => trans_choice('model.rack', 2)])
            @endif
        </div>

        @include('partials.pagination', ['items' => $racks])

    </div>
@endsection;
