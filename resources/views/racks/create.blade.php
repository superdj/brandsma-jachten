@extends('layouts.app')
@section('title', __('model.add', ['model' => trans_choice('model.Rack', 1)]))

@section('content')

<form method="POST" action="{{ route('racks.store') }}">
    @csrf
    <div class="row">
        <div class="xs12">
            <a href="{{ route('racks.index') }}" class="button button--contained button--contained--icon-left">
                <i class="material-icons icon">arrow_back</i>
                {{ __('model.back', ['page' => trans_choice('model.rack', 2)]) }}
            </a>
        </div>

        <div class="xs12 sm6 md3">
             <div class="row padding--0">
                 <h5 class="xs12">{{ trans_choice('model.Rack', 1) }}</h5>

                <div class="xs12">
                    <div class="text-field text-field--filled {{ $errors->has('name') ? 'text-field--helper-text text-field--invalid' : '' }}">
                        <input id="name" type="text" class="text-field__input" name="name" value="{{ old('name') }}" required autofocus>
                        <label for="name" class="text-field__label">{{ __('model.name', ['model' => trans_choice('model.Rack', 1)]) }}*</label>

                        @if ($errors->has('name'))
                            <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('name') }}
                            </span>
                        @endif
                    </div>
                </div>

                 <div class="xs12">
                     <div class="text-field text-field--filled text-field--select {{ $errors->has('warehouse_id') ? 'text-field--helper-text text-field--invalid' : '' }}">
                         <div class="text-field__input"></div>
                         <select id="warehouse_id" name="warehouse_id" required>
                             @foreach($warehouses as $warehouse)
                                 <option value="{{ $warehouse->id }}" {{ $warehouse->id === old('warehouse_id') ? 'selected' : '' }}>
                                     {{ $warehouse->name }}
                                 </option>
                             @endforeach
                         </select>
                         <label for="warehouse_id" class="text-field__label">{{ trans_choice('model.Warehouse', 1) }}*</label>

                         @if ($errors->has('warehouse_id'))
                             <span class="text-field__helper-text" role="alert">
                                {{ $errors->first('warehouse_id') }}
                            </span>
                         @endif
                     </div>
                 </div>
             </div>
        </div>

        <div class="xs12">
            <button type="submit" class="button button--contained button--contained--icon-left">
                <i class="material-icons icon">save</i>
                {{ __('model.add', ['model' => trans_choice('model.Rack', 1)]) }}
            </button>

            <button type="reset" class="button button--text button--text--icon-left">
                <i class="material-icons icon">settings_backup_restore</i>
                {{ __('model.reset') }}
            </button>
        </div>
    </div>
</form>
@endsection
