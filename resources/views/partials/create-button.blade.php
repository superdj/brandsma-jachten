@php
    $route = Route::currentRouteName();
    $controller = explode( '.', $route )[0];
@endphp

@can( 'create '.$controller )
    <div class="xs12">
        <a href="{{ route( $controller.'.create') }}" class="button button--contained button--contained--icon-left">
            <i class="material-icons icon">add</i>
            {{
                __(
                    'model.add',
                    [
                        'model' => trans_choice( 'model.'.ucfirst( Str::singular( $controller ) ), 1 )
                    ]
                )
            }}
        </a>
    </div>
@endcan
