<header class="app-bar app-bar--fixed">
    <i class="material-icons icon" data-trigger="drawer">menu</i>

    <h1 class="app-bar__title">@yield('title')</h1>

    <div class="app-bar__actions">
        <div class="tooltip">
            <a href="{{ route('dashboard.manual') }}" target="_blank">
                <i class="material-icons icon">help</i>
            </a>
            <span class="tooltip__content tooltip--bottom">{{ __('model.Manual') }}</span>
        </div>

        @can('create orders')
            <div class="tooltip">
                <i class="material-icons icon" data-trigger="cart">shopping_cart</i>
                <span class="tooltip__content tooltip--bottom">{{ __('model.Cart') }}</span>
            </div>
        @endcan

        <ul class="menu" id="cart">
            @if(!empty(session('cart')))
                @foreach(session('cart')->items as $item)
                    <li class="menu__item">
                        <span class="menu__content">
                            {{ $item['item']->name }} <em>x{{ $item['amount'] }}</em>
                        </span>
                    </li>
                @endforeach

                <li class="menu__item">
                    <a href="{{ route('cart.index') }}" class="menu__content">
                        {{ __('model.Cart') }}
                    </a>
                </li>
            @endif

            <li class="menu__item">
                <a href="{{ route('orders.create') }}" class="menu__content">
                    {{ __('model.To order') }}
                </a>
            </li>
        </ul>

        <div class="tooltip">
            <i class="material-icons icon" data-trigger="profile-menu">person</i>
            <span class="tooltip__content tooltip--bottom">{{ __('Profile') }}</span>
        </div>

        <div class="menu" id="profile-menu">
            <a href="{{ route('users.edit', auth()->user()->id) }}" class="menu__item">
                <span class="menu__content">{{ __('model.Edit profile') }}</span>
            </a>
            <form action="{{ route('logout') }}" method="post">
                @csrf
                <button class="menu__item">
                    <span class="menu__content">{{ __('Logout') }}</span>
                </button>
            </form>
        </div>
    </div>
</header>
