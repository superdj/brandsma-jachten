<div class="banner banner--single-line">
    <div class="banner__content">
        {{ __('model.not_found', ['model' => $model]) }}
    </div>
</div>
