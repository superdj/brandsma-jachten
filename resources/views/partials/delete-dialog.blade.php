@can('delete '.$controller)
    <form method="post" action="{{ route($controller.'.destroy', $item->id) }}">
        @csrf
        @method('delete')

        <div class="dialog dialog--confirm" id="dialog-{{ $item->id }}">
            <div class="dialog__header">
                {{ __('model.delete', ['model' => trans_choice('model.'.ucfirst(str_replace('-', ' ', $singular)), 1)]) }}
            </div>

            <div class="dialog__content">
                {{ __('model.delete_question', [
                    'model' => trans_choice('model.'.str_replace('-', ' ', $singular), 1),
                    'name' => $item->name
                ]) }}?
            </div>

            <div class="dialog__actions">
                <button
                    type="button"
                    class="button button--text"
                    data-trigger="dialog-{{ $item->id }}"
                >
                    {{ __('model.Cancel') }}
                </button>

                <button type="submit" class="button button--text">{{ __('model.Delete') }}</button>
            </div>
        </div>
    </form>
@endcan
