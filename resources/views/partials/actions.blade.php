@php
    $route = Route::currentRouteName();
    $controller = explode( '.', $route )[0];
    $singular = Str::singular($controller);
@endphp

<div class="md-and-up-hide">
    <button class="fab fab--mini" data-trigger="menu-{{ $item->id }}">
        <i class="material-icons icon">more_vert</i>
    </button>

    <div class="menu" id="menu-{{ $item->id }}">
        @if(in_array('edit', $actions))
            @can('edit '.$controller)
                <a
                    href="{{ route($controller.'.edit', $item->id) }}"
                    class="menu__item {{ $item->status && $item->status->lock ? 'menu__item--disabled' : '' }}"
                >
                    <i class="material-icons icon">edit</i>
                    <span class="menu__content">{{ __('model.Edit') }}</span>
                </a>
            @endcan
        @endif

        @if(in_array('show', $actions))
            @can('show '.$controller)
                <a href="{{ route($controller.'.show', $item->id) }}" class="menu__item">
                    <i class="material-icons icon">visibility</i>
                    <span class="menu__content">{{ __('model.Info') }}</span>
                </a>
            @endcan
        @endif

        @if(in_array('time-registration', $actions))
            @can('create time-registrations')
                <a
                    href="{{ route('time-registrations.create', $item->id) }}"
                    class="menu__item {{ $item->status && !$item->status->lock ? 'menu__item--disabled' : '' }}"
                >
                    <i class="material-icons icon">timer</i>
                    <span class="menu__content">{{ trans_choice('model.Time registration', 1)  }}</span>
                </a>
            @endcan

            @can('create used-parts')
                <a
                    href="{{ route('used-parts.create', $item->id) }}"
                    class="menu__item {{ $item->status && !$item->status->lock ? 'menu__item--disabled' : '' }}"
                >
                    <i class="material-icons icon">build</i>
                    <span class="menu__content">{{ __('model.Storing-out') }}</span>
                </a>
            @endcan
        @endif

        @if(in_array('delete', $actions))
            @can('delete '.$controller)
                <button
                    type="button"
                    data-trigger="dialog-{{ $item->id }}"
                >
                    <span class="menu__item">
                        <i class="material-icons icon">delete</i>
                        <span class="menu__content">{{ __('model.Delete') }}</span>
                    </span>
                </button>
            @endcan
        @endif
    </div>
</div>

<div class="md-down-hide">
    @if(in_array('edit', $actions))
        @can('edit '.$controller)
            <a
                href="{{ route($controller.'.edit', $item->id) }}"
                class="button button--outlined button--outlined--icon-left {{ $item->status && $item->status->lock ? 'button--outlined--disabled' : '' }}"
            >
                <i class="material-icons icon">edit</i>
                {{ __('model.Edit') }}
            </a>
        @endcan
    @endif

    @if(in_array('show', $actions))
        @can('show '.$controller)
            <a href="{{ route($controller.'.show', $item->id) }}" class="button button--outlined button--contained--icon-left">
                <i class="material-icons icon">visibility</i>
                {{ __('model.Info') }}
            </a>
        @endcan
    @endif

    @if(in_array('time-registration', $actions))
        @can('create time-registrations')
            <a
                href="{{ route('time-registrations.create', $item->id) }}"
                class="button button--outlined button--outlined--icon-left {{ $item->status && $item->status->lock ? 'button--outlined--disabled' : '' }}"
            >
                <i class="material-icons icon">timer</i>
                {{ trans_choice('model.Time registration', 1)  }}
            </a>
        @endcan

        @can('create used-parts')
            <a
                href="{{ route('used-parts.create', $item->id) }}"
                class="button button--outlined button--outlined--icon-left {{ $item->status && $item->status->lock ? 'button--outlined--disabled' : '' }}"
            >
                <i class="material-icons icon">build</i>
                {{ __('model.Storing-out') }}
            </a>
        @endcan
    @endif

    @if(in_array('delete', $actions))
        @can('delete '.$controller)
            <button
                type="button"
                class="button button--outlined button--outlined--icon-left"
                data-trigger="dialog-{{ $item->id }}"
            >
                <i class="material-icons icon">delete</i>
                {{ __('model.Delete') }}
            </button>
        @endcan
    @endif
</div>

@include('partials.delete-dialog',
    [
        'item' => $item,
        'controller' => $controller,
        'singular' => $singular
    ]
)
