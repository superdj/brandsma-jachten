@if($items->count() > 0)
    <div class="xs12 xs-up__text--center">
        {{ $items->appends(['q' => $q, 'sort' => $sortBy, 'order' => $orderBy, 'perpage' => $perPage])->links() }}
    </div>
@endif
