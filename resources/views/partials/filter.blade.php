@if($items->count() > 0)
    <div class="xs12">
        <form action="{{ route( Route::currentRouteName() ) }}">
            <div class="row padding--0">
                <div class="xs6 md2">
                    <div class="text-field text-field--filled">
                        <input type="search" name="q" id="search" class="text-field__input" value="{{ $q }}">
                        <label for="search" class="text-field__label">{{ __('model.Search') }}</label>
                        <i class="material-icons icon">search</i>
                    </div>
                </div>

                <div class="xs6 md2">
                    <div class="text-field text-field--filled text-field--select">
                        <div class="text-field__input"></div>
                        <select name="sort" id="sort">
                            @foreach($fields as $field => $field_name)
                                <option value="{{ $field }}" {{ $field === $sortBy ? 'selected' : '' }}>
                                    {{ $field_name }}
                                </option>
                            @endforeach
                        </select>
                        <label for="sort" class="text-field__label">{{ __('model.Order by') }}</label>
                    </div>
                </div>

                <div class="xs6 md2">
                    <div class="text-field text-field--filled text-field--select">
                        <div class="text-field__input"></div>
                        <select name="order" id="order">
                            @foreach(['asc', 'desc'] as $field)
                                <option value="{{ $field }}" {{ $field === $orderBy ? 'selected' : '' }}>
                                    {{ __('model.'.ucfirst($field)) }}
                                </option>
                            @endforeach
                        </select>
                        <label for="order" class="text-field__label">{{ __('model.Sort by') }}</label>
                    </div>
                </div>

                <div class="xs6 md2">
                    <div class="text-field text-field--filled text-field--select">
                        <div class="text-field__input"></div>
                        <select name="perPage" id="perPage">
                            @foreach([5, 10, 15, 20, 50, 100] as $field)
                                <option value="{{ $field }}" {{ $field == $perPage ? 'selected' : '' }}>
                                    {{ $field }}
                                </option>
                            @endforeach
                        </select>
                        <label for="perPage" class="text-field__label">{{ __('model.Per page') }}</label>
                    </div>
                </div>

                <div class="xs6 md2">
                    <button type="submit" class="button button--contained">
                        {{ trans_choice('model.Filter', 2) }}
                    </button>
                </div>
            </div>
        </form>
    </div>
@endif
