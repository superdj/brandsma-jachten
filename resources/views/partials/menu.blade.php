<nav class="drawer drawer--full-height drawer--modal" id="drawer">
    <ul>
        <li class="drawer__header drawer__header--small">
            <div class="drawer__header__profile-image">
                <img src="{{ asset('storage/logo-brandsma.svg') }}" alt="Brandsma Logo"/>
            </div>
            <div class="drawer__header__profile-name">
                {{ Auth::user()->first_name }}
                {{ Auth::user()->last_name }}
            </div>
        </li>
        <li>
            <a href="{{ route('dashboard.index') }}" class="drawer__item {{ Request::is('dashboard') ? 'drawer__item--active' : '' }}">
                <span class="drawer__item__content">{{ __('Home') }}</span>
            </a>
        </li>

        @can('view parts')
            <li>
                <a href="{{ route('parts.index') }}" class="drawer__item {{ Request::is('dashboard/parts*') ? 'drawer__item--active' : '' }}">
                    <span class="drawer__item__content">{{ trans_choice('model.Part', 2) }}</span>
                </a>
            </li>
        @endcan

        @canany(['view assignments', 'view time-registrations', 'view used-parts'])
            <li>
                <div class="drawer__item drawer__item__parent">
                    <span class="drawer__item__content">{{ trans_choice('model.Assignment', 2) }}</span>
                    <i class="material-icons icon arrow">expand_more</i>
                </div>

                <ul class="drawer__item__children">
                    @can('view assignments')
                        <li>
                            <a href="{{ route('assignments.index') }}" class="drawer__item {{ Request::is('dashboard/assignments*') ? 'drawer__item--active' : '' }}">
                                <span class="drawer__item__content">{{ trans_choice('model.Assignment', 2) }}</span>
                            </a>
                        </li>
                    @endcan

                    @can('view time-registrations')
                        <li>
                            <a href="{{ route('time-registrations.index') }}" class="drawer__item {{ Request::is('dashboard/time-registrations*') ? 'drawer__item--active' : '' }}">
                                <span class="drawer__item__content">{{ trans_choice('model.Time registration', 2) }}</span>
                            </a>
                        </li>
                    @endcan

                    @can('view used-parts')
                        <li>
                            <a href="{{ route('used-parts.index') }}" class="drawer__item {{ Request::is('dashboard/used-parts*') ? 'drawer__item--active' : '' }}">
                                <span class="drawer__item__content">{{ trans_choice('model.Used part', 2) }}</span>
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcanany

        @can('view orders')
            <li>
                <a href="{{ route('orders.index') }}" class="drawer__item {{ Request::is('dashboard/orders*') ? 'drawer__item--active' : '' }}">
                    <span class="drawer__item__content">{{ trans_choice('model.Order', 2) }}</span>
                </a>
            </li>
        @endcan

        <li class="divider"></li>
        @canany(['view users', 'view roles'])
            <li>
                <div class="drawer__item drawer__item__parent">
                    <span class="drawer__item__content">{{ trans_choice('model.User', 2) }}</span>
                    <i class="material-icons icon arrow">expand_more</i>
                </div>

                <ul class="drawer__item__children">
                    @can('view users')
                        <li>
                            <a href="{{ route('users.index') }}" class="drawer__item {{ Request::is('dashboard/users*') ? 'drawer__item--active' : '' }}">
                                <span class="drawer__item__content">{{ trans_choice('model.User', 2) }}</span>
                            </a>
                        </li>
                    @endcan

                    @can('view roles')
                        <li>
                            <a href="{{ route('roles.index') }}" class="drawer__item {{ Request::is('dashboard/roles*') ? 'drawer__item--active' : '' }}">
                                <span class="drawer__item__content">{{ trans_choice('model.Role', 2) }}</span>
                            </a>
                        </li>
                    @endcan
                 </ul>
             </li>
        @endcanany

        @can('view customers')
            <li>
                <a href="{{ route('customers.index') }}" class="drawer__item {{ Request::is('dashboard/customers*') ? 'drawer__item--active' : '' }}">
                    <span class="drawer__item__content">{{ trans_choice('model.Customer', 2) }}</span>
                </a>
            </li>
        @endcan

        @can('view suppliers')
            <li>
                <a href="{{ route('suppliers.index') }}" class="drawer__item {{ Request::is('dashboard/suppliers*') ? 'drawer__item--active' : '' }}">
                    <span class="drawer__item__content">{{ trans_choice('model.Supplier', 2) }}</span>
                </a>
            </li>
        @endcan

        @canany(['view warehouses', 'view racks', 'view shelves'])
            <li>
                <div class="drawer__item drawer__item__parent">
                    <span class="drawer__item__content">{{ trans_choice('model.Warehouse', 2) }}</span>
                    <i class="material-icons icon arrow">expand_more</i>
                </div>

                <ul class="drawer__item__children">
                    @can('view warehouses')
                        <li>
                            <a href="{{ route('warehouses.index') }}" class="drawer__item {{ Request::is('dashboard/warehouses*') ? 'drawer__item--active' : '' }}">
                                <span class="drawer__item__content">{{ trans_choice('model.Warehouse', 2) }}</span>
                            </a>
                        </li>
                    @endcan

                    @can('view racks')
                        <li>
                            <a href="{{ route('racks.index') }}" class="drawer__item {{ Request::is('dashboard/racks*') ? 'drawer__item--active' : '' }}">
                                <span class="drawer__item__content">{{ trans_choice('model.Rack', 2) }}</span>
                            </a>
                        </li>
                    @endcan

                    @can('view shelves')
                        <li>
                            <a href="{{ route('shelves.index') }}" class="drawer__item {{ Request::is('dashboard/shelves*') ? 'drawer__item--active' : '' }}">
                                <span class="drawer__item__content">{{ trans_choice('model.Shelf', 2) }}</span>
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcanany

        @can('view statuses')
            <li>
                <a href="{{ route('statuses.index') }}" class="drawer__item {{ Request::is('dashboard/statuses*') ? 'drawer__item--active' : '' }}">
                    <span class="drawer__item__content">{{ trans_choice('model.Status', 2) }}</span>
                </a>
            </li>
        @endcan

        @can('view tasks')
            <li>
                <a href="{{ route('tasks.index') }}" class="drawer__item {{ Request::is('dashboard/tasks*') ? 'drawer__item--active' : '' }}">
                    <span class="drawer__item__content">{{ trans_choice('model.Task', 2) }}</span>
                </a>
            </li>
        @endcan

        @can('view units')
            <li>
                <a href="{{ route('units.index') }}" class="drawer__item {{ Request::is('dashboard/units*') ? 'drawer__item--active' : '' }}">
                    <span class="drawer__item__content">{{ trans_choice('model.Unit', 2) }}</span>
                </a>
            </li>
        @endcan

        @can('view categories')
            <li>
                <a href="{{ route('categories.index') }}" class="drawer__item {{ Request::is('dashboard/categories*') ? 'drawer__item--active' : '' }}">
                    <span class="drawer__item__content">{{ trans_choice('model.Category', 2) }}</span>
                </a>
            </li>
        @endcan
    </ul>
</nav>
