@extends('layouts.app')
@section('title', __('model.add', ['model' => trans_choice('model.Status', 1)]))

@section('content')
    <form method="POST" action="{{ route('statuses.store') }}">
        @csrf
        <div class="row">
            <div class="xs12">
                <a href="{{ route('statuses.index') }}" class="button button--contained button--contained--icon-left">
                    <i class="material-icons icon">arrow_back</i>
                    {{ __('model.back', ['page' => trans_choice('model.status', 2)]) }}
                </a>
            </div>

            <div class="xs12 sm6 md3">
                 <div class="row padding--0">
                     <h5 class="xs12">{{ trans_choice('model.Status', 1) }}</h5>

                    <div class="xs12">
                        <div class="text-field text-field--filled {{ $errors->has('name') ? 'text-field--helper-text text-field--invalid' : '' }}">
                            <input id="name" type="text" class="text-field__input" name="name" value="{{ old('name') }}" required autofocus>
                            <label for="name" class="text-field__label">{{ __('model.name', ['model' => trans_choice('model.Status', 1)]) }}*</label>

                            @if ($errors->has('name'))
                                <span class="text-field__helper-text" role="alert">
                                    {{ $errors->first('name') }}
                                </span>
                            @endif
                        </div>
                    </div>

                     <div class="xs12">
                         <div class="text-field text-field--filled text-field--select {{ $errors->has('model') ? 'text-field--helper-text text-field--invalid' : '' }}">
                             <div class="text-field__input"></div>
                             <select id="model" name="model">
                                 <option value="App\Order" {{ old('model') === 'App\Order' ? 'selected' : '' }}>{{ trans_choice('model.Order', 1) }}</option>
                                 <option value="App\Assignment" {{ old('model') === 'App\Assignment' ? 'selected' : '' }}>{{ trans_choice('model.Assignment', 1) }}</option>
                             </select>
                             <label for="model" class="text-field__label">{{ trans_choice('model.Filter', 1) }}*</label>

                             @if ($errors->has('model'))
                                 <span class="text-field__helper-text" role="alert">
                                    {{ $errors->first('model') }}
                                </span>
                             @endif
                         </div>
                     </div>

                     <div class="xs12">
                         <label>
                             <input type="hidden" name="lock" value="0">
                             <input type="checkbox" name="lock" class="switch" value="1" {{ old('lock') == 1 ? 'checked' : '' }}>
                             {{ __('model.Lock') }}
                         </label>
                     </div>
                 </div>
            </div>

            <div class="xs12">
                <button type="submit" class="button button--contained button--contained--icon-left">
                    <i class="material-icons icon">save</i>
                    {{ __('model.add', ['model' => trans_choice('model.Status', 1)]) }}
                </button>

                <button type="reset" class="button button--text button--text--icon-left">
                    <i class="material-icons icon">settings_backup_restore</i>
                    {{ __('model.reset') }}
                </button>
            </div>
        </div>
    </form>
@endsection
