@extends('layouts.app')
@section('title', trans_choice('model.Status', 2))

@section('content')
    <div class="row">
        @include('partials.create-button')

        @include('partials.filter',
           [
               'items' => $statuses,
               'fields' => [
                    'id' => __('model.Id'),
                    'name' => __('model.Name'),
                    'model' => trans_choice('model.Filter', 1),
                    'lock' => __('model.Lock'),
                    'user_created' => __('model.Creator'),
                    'user_updated' => __('model.Updater'),
                    'created_at' => __('model.Created at'),
                    'updated_at' => __('model.Updated at'),
               ]
           ]
        )

        @include('partials.pagination', ['items' => $statuses])

        <div class="xs12">
            @if($statuses->count() > 0)
                <table class="data-table data-table--hover data-table--responsive">
                    <thead>
                    <tr>
                        <th>{{ __('model.name', ['model' => trans_choice('model.Status', 1)]) }}</th>
                        <th>{{ trans_choice('model.Filter', 1) }}</th>
                        <th>{{ __('model.Lock') }}</th>
                        <th class="md-down-hide">{{ __('model.Created by') }}</th>
                        <th class="md-down-hide">{{ __('model.Updated by') }}</th>
                        <th>{{ __('model.Actions') }}</th>
                    </tr>
                    </thead>

                    <tbody>
                        @foreach($statuses as $status)
                            <tr>
                                <td>{{ $status->name }}</td>
                                <td>{{ trans_choice('model.'.str_replace('App', '', stripslashes($status->model)), 1) }}</td>
                                <td>
                                    @if($status->lock)
                                        <i class="material-icons icon text--green">lock</i>
                                    @else
                                        <i class="material-icons icon text--red">lock_open</i>
                                    @endif
                                </td>
                                <td class="md-down-hide">{{ $status->creator->first_name }}</td>
                                <td class="md-down-hide">{{ $status->updater ? $status->updater->first_name : '' }}</td>
                                <td>
                                    @include('partials.actions',
                                        [
                                            'item' => $status,
                                            'actions' => ['edit', 'delete']
                                        ]
                                    )
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @else
                @include('partials.not-found-banner', ['model' => trans_choice('model.status', 2)])
            @endif
        </div>

        @include('partials.pagination', ['items' => $statuses])
    </div>
@endsection;
