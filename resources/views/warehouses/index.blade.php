@extends('layouts.app')
@section('title', trans_choice('model.Warehouse', 2))

@section('content')
    <div class="row">
        @include('partials.create-button')

        @include('partials.filter',
            [
                'items' => $warehouses,
                'fields' => [
                    'id' => __('model.Id'),
                    'name' => __('model.Name'),
                    'user_created' => __('model.Creator'),
                    'user_updated' => __('model.Updater'),
                    'created_at' => __('model.Created at'),
                    'updated_at' => __('model.Updated at'),
                ]
            ]
        )

        @include('partials.pagination', ['items' => $warehouses])

        <div class="xs12">
            @if($warehouses->count() > 0)
                <table class="data-table data-table--hover data-table--responsive">
                    <thead>
                    <tr>
                        <th>{{ __('model.name', ['model' => trans_choice('model.Warehouse', 1)]) }}</th>
                        <th class="data-table--numeric">{{ trans_choice('model.Rack', 2) }}</th>
                        <th class="data-table--numeric">{{ trans_choice('model.Shelf', 2) }}</th>
                        <th class="md-down-hide">{{ __('model.Created by') }}</th>
                        <th class="md-down-hide">{{ __('model.Updated by') }}</th>
                        <th>{{ __('model.Actions') }}</th>
                    </tr>
                    </thead>

                    <tbody>
                        @foreach($warehouses as $warehouse)
                            <tr>
                                <td>{{ $warehouse->name }}</td>
                                <td class="data-table--numeric">{{ $warehouse->racks ? $warehouse->racks->count() : '' }}</td>
                                <td class="data-table--numeric">{{ $warehouse->shelves ? $warehouse->shelves->count() : '' }}</td>
                                <td class="md-down-hide">{{ $warehouse->creator->first_name }}</td>
                                <td class="md-down-hide">{{ $warehouse->updater ? $warehouse->updater->first_name : '' }}</td>
                                <td>
                                    @include('partials.actions',
                                        [
                                            'item' => $warehouse,
                                            'actions' => ['edit', 'delete']
                                        ]
                                    )
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @else
                @include('partials.not-found-banner', ['model' => trans_choice('model.warehouse', 2)])
            @endif
        </div>

        @include('partials.pagination', ['items' => $warehouses])
    </div>
@endsection;
