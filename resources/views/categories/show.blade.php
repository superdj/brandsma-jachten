@extends('layouts.app')
@section('title', __('model.info', ['model' => trans_choice('model.Category', 1), 'name' => $category->name]))

@section('content')
    <div class="row">
        <div class="xs12">
            <a href="{{ route('categories.index') }}" class="button button--contained button--contained--icon-left">
                <i class="material-icons icon">arrow_back</i>
                {{ __('model.back', ['page' => trans_choice('model.category', 2)]) }}
            </a>
        </div>

        <div class="xs12 sm4 md3">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        <i class="material-icons icon">info</i>
                        {{ __('model.General') }}
                    </div>
                </div>

                <div class="card__text">
                    <ul class="list">
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ __('model.name', ['model' => trans_choice('model.Category', 1)]) }}</span>
                                <span class="list__content__text">{{ $category->name }}</span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Created by', 1) }}</span>
                                <span class="list__content__text">
                                    @can('show users')
                                        <a href="{{ route('users.show', $category->creator->id) }}">
                                            {{ $category->creator->first_name }}
                                        </a>
                                    @endcan

                                    @cannot('show users')
                                        {{ $category->creator->first_name }}
                                    @endcannot
                                </span>
                            </div>
                        </li>
                        <li class="list--two-line">
                            <div class="list__content">
                                <span class="list__content__title">{{ trans_choice('model.Updated by', 1) }}</span>
                                <span class="list__content__text">
                                    @if($category->updater)
                                        @can('show users')
                                            <a href="{{ route('users.show', $category->updater->id) }}">
                                            {{ $category->updater->first_name }}
                                        </a>
                                        @endcan

                                        @cannot('show users')
                                            {{ $category->updater->first_name }}
                                        @endcannot
                                    @endif
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>

                @can('edit categories')
                    <div class="card__actions">
                        <a href="{{ route('categories.edit', $category->id) }}" class="button button--text">
                            {{ __('model.edit', ['model' => trans_choice('model.Category', 1)]) }}
                        </a>
                    </div>
                @endcan
            </div>
        </div>

        <div class="xs12 sm8 md5 lg4">
            <div class="card">
                <div class="card__header">
                    <div class="card__title">
                        @can('view parts')
                            <a href="{{ route('parts.index') }}">
                                <i class="material-icons icon">build</i>
                                {{ trans_choice('model.Part', 2) }}
                            </a>
                        @endcan

                        @cannot('view parts')
                            <i class="material-icons icon">build</i>
                            {{ trans_choice('model.Part', 2) }}
                        @endcannot
                    </div>
                </div>

                <div class="card__text">
                    @if( $category->parts->count() > 0 )
                        <table class="data-table data-table--hover">
                            <thead>
                            <tr>
                                <th>{{ trans_choice('model.Part', 1) }}</th>
                                <th>{{ __('model.Price') }}</th>
                            </tr>
                            </thead>

                            <tbody>
                            @php($price = 0)
                            @foreach($category->parts as $part)
                                <tr>
                                    <td>
                                        @can('show parts')
                                            <a href="{{ route('parts.show', $part->id) }}">
                                                {{ $part->name }}
                                            </a>
                                        @endcan

                                        @cannot('show parts')
                                            {{ $part->name }}
                                        @endcannot
                                    </td>
                                    <td>
                                        &euro;{{ number_format($part->price, 2, ',', '.') }}
                                        @php($price += $part->price)
                                    </td>
                                </tr>
                            @endforeach

                            <tr>
                                <td><strong>{{ __('model.Total') }}</strong></td>
                                <td>&euro;{{ number_format($price, 2, ',', '.') }}</td>
                            </tr>
                            </tbody>
                        </table>
                    @else
                        @include('partials.not-found-banner', ['model' => trans_choice('model.part', 2)])
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
