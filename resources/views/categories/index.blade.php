@extends('layouts.app')
@section('title', trans_choice('model.Category', 2))

@section('content')
    <div class="row">
        @include('partials.create-button')

        @include('partials.filter',
             [
                 'items' => $categories,
                 'fields' => [
                    'id' => __('model.Id'),
                    'name' => __('model.Name'),
                    'user_created' => __('model.Creator'),
                    'user_updated' => __('model.Updater'),
                    'created_at' => __('model.Created at'),
                    'updated_at' => __('model.Updated at'),
                 ]
             ]
        )

        @include('partials.pagination', ['items' => $categories])

        <div class="xs12">
            @if($categories->count() > 0)
                <table class="data-table data-table--hover data-table--responsive">
                    <thead>
                    <tr>
                        <th>{{ __('model.name', ['model' => trans_choice('model.Category', 1)]) }}</th>
                        <th class="data-table--numeric">{{ trans_choice('model.Part', 2) }}</th>
                        <th>{{ __('model.Actions') }}</th>
                    </tr>
                    </thead>

                    <tbody>

                        @foreach($categories as $category)
                            <tr>
                                <td>
                                    @can('show categories')
                                        <a href="{{ route('categories.show', $category->id) }}">
                                            {{ $category->name }}
                                        </a>
                                    @endcan

                                    @cannot('show categories')
                                        {{ $category->name }}
                                    @endcannot
                                </td>
                                <td class="data-table--numeric">{{ $category->parts->count() }}</td>
                                <td>
                                    @include('partials.actions',
                                        [
                                            'item' => $category,
                                            'actions' => ['edit', 'show', 'delete']
                                        ]
                                    )
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @else
                @include('partials.not-found-banner', ['model' => trans_choice('model.category', 2)])
            @endif
        </div>

        @include('partials.pagination', ['items' => $categories])
    </div>
@endsection;
